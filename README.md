###  功能列表

- 新建系统配置项
- 国际化
- 新建升级类/创建升级类相关实体
- 管理War包下的extensions.list文件
- 产品API
- 其他(扫描指定目录下的扩展包)

### 快速使用

1. 在仓库的[工具包标签列表](https://gitee.com/junsai/SimpleDevelopForSmartbi/tags)，找到指定版本，下载jar包等。
2. 启动方式。
   1. java -jar SimpleDevelopForSmartbi.jar 启动。
   2. 双击start.cmd脚本文件。
   3. 访问地址 http://localhost:18972/simpledevelop/vue/index.html#/buildConfig。

### 文档参考

[操作说明文档.docx](https://gitee.com/junsai/SimpleDevelopForSmartbi/attach_files/810342/download/操作说明文档.docx)

