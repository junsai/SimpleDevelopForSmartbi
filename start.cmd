@echo off
FOR /F "eol=; tokens=2,2 delims==" %%i IN ('findstr /i "port" setting.properties') DO set port=%%i
if "%port%" == "" (set port=18972)
explorer "http://localhost:%port%/simpledevelop/vue/index.html#/buildConfig"
START "SimpleDevelopForSmartbi" javaw -jar SimpleDevelopForSmartbi.jar --server.port=%port%
exit;