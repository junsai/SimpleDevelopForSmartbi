package com.ljs.module.entity.template.upgrade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by lijunsai on 2021/08/19
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ConversionValueType {
    private String valueType;
    private String type;
    private String classPackage;
    private String[] specialHandle;

    public ConversionValueType (String valueType, String type, String classPackage){
        this.valueType = valueType;
        this.type = type;
        this.classPackage = classPackage;
    }
}
