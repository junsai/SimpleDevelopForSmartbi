package com.ljs.module.entity.template.systemconfig;

import com.alibaba.fastjson.annotation.JSONField;
import com.ljs.module.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ConfigurationPatch实体
 * Created by lijunsai on 2021/07/12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConfigItem extends BaseEntity {
    @JSONField(ordinal = 1)
    private String tabName;
    @JSONField(ordinal = 2)
    private String groupName;
    @JSONField(ordinal = 3)
    private String className;
    @JSONField(ordinal = 4)
    private String itemNumber;

    public String toStringFILE() {
        return "\r\n## 系统配置项 \r\n" +
                "{\r\n" +
                "    tabName:" + tabName + ",\r\n" +
                "    groupName:" + groupName + ",\r\n" +
                "    className:" + className + ",\r\n" +
                "    itemNumber:" + itemNumber + "\r\n" +
                "}\r\n";
    }

    public String toStringHTML() {
        return "<br>## 系统配置项 <br>" +
                "{<br>" +
                "    tabName:" + tabName + ",<br>" +
                "    groupName:" + groupName + ",<br>" +
                "    className:" + className + ",<br>" +
                "    itemNumber:" + itemNumber + "<br>" +
                "}<br>";
    }
}
