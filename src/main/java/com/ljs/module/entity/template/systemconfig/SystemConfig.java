package com.ljs.module.entity.template.systemconfig;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ljs.module.annotation.TemplateKey;
import com.ljs.module.common.TypeEnum;
import com.ljs.module.entity.BaseEntity;
import com.ljs.module.entity.ConfigProperties;
import com.ljs.module.entity.language.Language;
import com.ljs.module.entity.language.Translate;
import com.ljs.module.util.ConfigurationPatchUtil;
import com.ljs.module.util.ConvertrUtil;
import com.ljs.module.util.FileUtil;
import com.ljs.module.util.LanguageFileUtil;
import com.ljs.module.util.TemplateUtil;
import com.ljs.module.util.Util;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Map;

@TemplateKey
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SystemConfig extends BaseEntity {
    // 文件名
    @TemplateKey
    private String fileName;
    // 配置项名称
    @TemplateKey
    private String itemName;
    // dbkey
    @TemplateKey
    private String dbkey;
    // 元素
    @TemplateKey
    private String defaultValue;
    // 描述
    @TemplateKey
    private String  description;

    @TemplateKey
    private boolean haveInit;
    // 构建类型
    private String type;
    // 自定义tabName
    private String tabNameCus;
    // 自定义tabName
    private String groupNameCus;
    // 选用系统tabName
    private String tabName;
    // 选用系统groupName
    private String groupName;
    // 本地路径
    private String path;
    // 选项
    private String[] configs;

    // bool类型实体类
    @TemplateKey
    private ConfigProperties configProperties;
    // 翻译实体类
    private Translate translate = new Translate();
    // ConfigurationPatch实体类
    private ConfigItem configItem = new ConfigItem();

    @TemplateKey
    private String[] keys;
    @TemplateKey
    private String[] values;

    public void init(){
        fileName = fileName.trim().replaceAll("\\.js", "");
        // 读取构建选项
        configProperties = ConfigProperties.of(configs);
        translate.setConfigProperties(configProperties);
        configItem.setConfigProperties(configProperties);

        // 构建configItem
        if (configProperties.isCusConfigItem()) {
            tabName = "${" + translate.humpString(tabNameCus, "").getEnToUppercase() + "}";
            groupName = "${" + translate.humpString(groupNameCus, "").getEnToUppercase() + "}";
        } else {
            tabName = ConvertrUtil.getConfig(tabName);
            groupName = ConvertrUtil.getConfig(groupName);
        }
        configItem = new ConfigItem(tabName, groupName, "ext.config." + fileName,"9999");

        // itemName 国际化
        Language language = translate.humpString(itemName, fileName);
        itemName = Util.appendLanguage(language.getEnToUppercase());
        dbkey = language.getNoFileName();
        // 描述国际化
        if (!StringUtils.isBlank(description)) {
            description = Util.appendLanguage(translate.humpString(description, fileName).getEnToUppercase());
        }
        haveInit = !noInit();

        if (TypeEnum.valueOf(type).equals(TypeEnum.RADIO) || TypeEnum.valueOf(type).equals(TypeEnum.SELECT)) {
            values = defaultValue.split(",");
            keys = new String[values.length];
            for (int i = 0; i < values.length; i++) {
                language = translate.humpString(values[i], fileName);
                values[i] = Util.appendLanguage(language.getEnToUppercase());
                keys[i] = language.getNoFileName();
            }
        }
    }

    public String build() throws IOException {
        init();

        StringBuilder respMsg = new StringBuilder();
        // 处理 系统配置项主体
        String template = TypeEnum.valueOf(type).getTemplate();
        Map<String, Object> templateKeyORM = TemplateUtil.getTemplateKeyORM(this);
        String render = TemplateUtil.render(template, templateKeyORM);

        if (configProperties.isLocation()) {
            if (!StringUtils.isBlank(path)) {
                // 插入系统配置项文件
                respMsg.append(FileUtil.insertConfigJsFile(path, new StringBuilder(render), fileName, configProperties));

                if (configProperties.isInsertLanguage()) {
                    // 更新国际化文件
                    respMsg.append(LanguageFileUtil.appendToProperties(path, translate.getConvertrMap(), configProperties));
                }

                if (configProperties.isInsertConfigurationPatch()) {
                    // 更新Configuration.js
                    configItem.setConfigProperties(configProperties);
                    respMsg.append(ConfigurationPatchUtil.insertConfiguration(configItem, path + FileUtil.EXT_PATH + "ConfigurationPatch.js"));
                }
            }
        } else {
            respMsg.append(render)
                    .append(translate.mapAppendFile(new StringBuilder(), "").toString().replaceAll("\r\n", "<br>"))
                    .append(configItem.toStringHTML());
        }
        return respMsg.toString();
    }

    public TypeEnum[] noInit = new TypeEnum[]{TypeEnum.NO_DEFAULT_VALUE_INPUT, TypeEnum.JUST_BUTTON, TypeEnum.TEXT_AREA};
    public boolean noInit(){
        for (TypeEnum typeEnum : noInit) {
            if (type.equals(typeEnum.name())) {
                return true;
            }
        }
        return false;
    }
}
