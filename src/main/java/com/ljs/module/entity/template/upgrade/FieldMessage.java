package com.ljs.module.entity.template.upgrade;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Created by lijunsai on 2021/08/19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FieldMessage {
    private String methodGet;
    private String methodSet;
    private String fieldName;
    private String valueType;
    private String classType;
    private String classPackage;
    private String[] annotation;
    private String isNull;
}
