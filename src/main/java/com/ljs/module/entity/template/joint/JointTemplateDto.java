package com.ljs.module.entity.template.joint;


import com.ljs.module.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JointTemplateDto extends BaseEntity {
    private String links = "";
//    private String exports;
//    private String functions;
    private String templateCode = "";
    private String params = "";
    private String jsCode = "";
    private String path;
}
