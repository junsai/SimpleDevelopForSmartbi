package com.ljs.module.entity.common;

import lombok.Data;

import java.util.List;

@Data
public class CommonTab {
    private String defaultTreeDir;
    private String defaultTreeItem;
    private String defaultTabLabel;
    private List<TreeNode> nodes;

    public CommonTab(String defaultTreeDir, String defaultTreeItem, String defaultTabLabel) {
        this.defaultTreeDir = defaultTreeDir;
        this.defaultTreeItem = defaultTreeItem;
        this.defaultTabLabel = defaultTabLabel;
    }
}
