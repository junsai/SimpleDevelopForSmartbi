package com.ljs.module.entity.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.HashSet;

/**
 * Created by lijunsai on 2021/09/18
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ToString(callSuper = true)
public class ApiEntityDto extends ApiEntity {
    private Double score;
    @JsonIgnore
    private HashSet<String> light = new HashSet<>();
    private String[] keyWords;
}
