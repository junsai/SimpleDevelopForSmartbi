package com.ljs.module.entity.smartbi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SearchExt {
    private String ftpPath;
    private String name;
    private List<String> paths;
    private String select;
}
