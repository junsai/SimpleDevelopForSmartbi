package com.ljs.module.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ljs.module.annotation.IgnoreField;
import com.ljs.module.annotation.TemplateExtendKey;
import com.ljs.module.annotation.TemplateKey;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseEntity {
    @TemplateKey
    @JsonIgnore
    @IgnoreField
    protected ConfigProperties configProperties;

    @TemplateExtendKey
    @IgnoreField
    protected String extendJson;
}
