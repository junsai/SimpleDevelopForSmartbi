package com.ljs.module.controller;

import com.ljs.module.common.DataBean;
import com.ljs.module.common.PathEnum;
import com.ljs.module.entity.common.CommonTab;
import com.ljs.module.entity.common.TreeNode;
import com.ljs.module.util.FileUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/common/tab")
public class CommonTabController {

    static Map<String, CommonTab> map = new HashMap<>();

    static {
        map.put("jointTemplate", new CommonTab("联合模板", "联合模板简介", "联合模板简介"));
        map.put("customTemplate", new CommonTab("自定义模板", "自定义模板简介", "自定义模板简介"));
    }


    /**
     * 构建升级类
     * @return 操作结果
     */
    @ResponseBody
    @GetMapping("/get")
    public DataBean<CommonTab> getInitCommonTabForType(String type){
        try {
            CommonTab commonTab = map.get(type);
            String path = "";
            String endWith = "";
            switch (type) {
                case "jointTemplate" :
                    path = PathEnum.JOINT_TEMPLATE.getPath();
                    endWith = ".joint"; break;
                case "customTemplate" :
                    path = PathEnum.CUSTOM_TEMPLATE.getPath();
                    endWith = ".js"; break;
            }
            List<TreeNode> treeNode = FileUtil.getTreeNode(path, "", endWith, true);
            commonTab.setNodes(treeNode);
            return DataBean.success(commonTab);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return DataBean.fail().setDesc("");
    }

}