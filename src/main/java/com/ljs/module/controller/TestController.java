package com.ljs.module.controller;

import com.ljs.module.entity.template.custom.Custom;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/test/tab")
public class TestController {

    /**
     * 构建升级类
     * @return 操作结果
     */
    @ResponseBody
    @PostMapping("/token")
    public String token(
            @RequestHeader("Authorization")String Authorization,
            @RequestParam("grant_type") String type,
            @RequestParam("code") String code){
        System.out.println(Authorization);
        System.out.println(type);
        System.out.println(code);
        return "{access_token : \"123123123\"}";
    }

    @ResponseBody
    @GetMapping("/info")
    public String info(
            @RequestHeader("Authorization")String Authorization){
        System.out.println(Authorization);
        return "{userName : \"zhangsan\"}";
    }

    @ResponseBody
    @PostMapping("/test")
    public String test(@RequestBody Custom custom){
        return "{userName : \"zhangsan\"}";
    }

    private static List<Map<String, Object>> users = new ArrayList<>();
    private static List<Map<String, Object>> depts = new ArrayList<>();

    static {
        depts.add(getDept(1, -1, "单位番号1", "中央军委", ""));
        depts.add(getDept(2, 1, "单位番号2", "中央军委2", ""));
        depts.add(getDept(3, 1, "单位番号3", "中央军委3", ""));
        depts.add(getDept(4, 3, "单位番号3-4", "中央军委3-4", ""));
        depts.add(getDept(5, 3, "单位番号3-5", "中央军委3-5", ""));

        users.add(getUser(326, "admin", "超级管理员", 1, "123", "系统管理员，拥有所有权限", -1));
        users.add(getUser(327, "admin1", "超级管理员", 1, "123", "系统管理员，拥有所有权限", 2));
        users.add(getUser(328, "admin2", "超级管理员", 0, "123", "系统管理员，拥有所有权限", 3));
        users.add(getUser(329, "admin3", "超级管理员", 1, "123", "系统管理员，拥有所有权限", 4));
        users.add(getUser(336, "admin4", "超级管理员", 1, "123", "系统管理员，拥有所有权限", 5));
    }

    public static Map<String,Object> getDept(Integer orgId, Integer parentId, String orgCode, String orgName, String orgDesc) {
        Map<String, Object> map = new HashMap<>();
        map.put("orgId", orgId);
        map.put("parentId", parentId);
        map.put("orgCode", orgCode);
        map.put("orgName", orgName);
        map.put("orgDesc", orgDesc);
        return map;
    }

    public static Map<String,Object> getUser(Integer userId, String username, String usertruename, Integer isEnabled, String password, String remark, Integer managerOrgId) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("username", username);
        map.put("usertruename", usertruename);
        map.put("isEnabled", isEnabled);
        map.put("password", password);
        map.put("remark", remark);
        map.put("manageOrgId", managerOrgId);
        return map;
    }

    @ResponseBody
    @GetMapping("/user/info")
    public Map<String, Object> getUserInfo(String clientId, Integer size, Integer current) {
        Map<String, Object> map = new HashMap<>();
        map.put("status", 200);
        map.put("timestamp", 200);

        Map<String, Object> mapChildren = new HashMap<>();
        mapChildren.put("current", current);
        mapChildren.put("size", size);
        mapChildren.put("userListTotal", users.size());
        mapChildren.put("deptListTotal", depts.size());

        List<Map<String, Object>> usersTmp = new ArrayList<>();
        List<Map<String, Object>> deptsTmp = new ArrayList<>();
        for (Integer i = 0; i < size; i++) {
            int index = (current - 1) * size + i;
            if (users.size() > index) {
                usersTmp.add(users.get(index));
            }
            if (depts.size() > index) {
                deptsTmp.add(depts.get(index));
            }
        }
        mapChildren.put("userList", usersTmp);
        mapChildren.put("deptList", deptsTmp);
        map.put("result", mapChildren);
        return map;
    }
}