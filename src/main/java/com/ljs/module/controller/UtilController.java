package com.ljs.module.controller;

import com.ljs.module.util.Util;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;

/**
 * 工具类控制器  资源文件打开某个目录/复制文字到粘贴板等
 * Created by lijunsai on 2021/10/08
 */
@Controller
@RequestMapping("/util")
public class UtilController {

    /**
     * 打开文件所在位置
     * @param path
     */
    @GetMapping("openFile")
    public void openFile(String path) {
        try {
            if (path.contains(".")) {
                path = path.substring(0, path.lastIndexOf(File.separator));
            }
            Runtime.getRuntime().exec("cmd /c start explorer " + path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 复制到粘贴板里
     * @param content
     */
    @GetMapping("copyContent")
    public void copyContent(String content) {
        Util.setClipboardString(content);
    }

    /**
     * 获取当前系统的路径方式
     */
    @ResponseBody
    @GetMapping("file/separator")
    public String getFileSeparator() {
        return File.separator;
    }
}
