package com.ljs.module.controller;

import com.ljs.module.entity.ConfigProperties;
import com.ljs.module.entity.language.Translate;
import com.ljs.module.util.LanguageFileUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 国际化控制器
 * Created by lijunsai on 2021/10/08
 */
@Controller
@RequestMapping("/language")
public class LanguageController {
    /**
     * 国际化接口
     * @param filed 需要被国际化的字段
     * @param configs 勾选项
     * @param path 扩展包路径
     * @return 操作结果
     * @throws IOException
     */
    @ResponseBody
    @PostMapping("/translate")
    public List<String> translate(String filed, String[] configs, String path) throws IOException {
        Translate translate = new Translate();
        translate.setConfigProperties(ConfigProperties.of(configs));
        String[] split = filed.split(",");
        for (String s : split) {
            translate.humpString(s, "");
        }
        String ret = "";
        if (translate.getConfigProperties().isLocation()) {
            ret = LanguageFileUtil.appendToProperties(path, translate.getConvertrMap(), translate.getConfigProperties());
        }
        StringBuilder[] allPros = translate.getAllPros();
//        ret += ConvertrUtil.mapAppendFile(new StringBuilder(), "").toString().replaceAll("\r\n", "<br>");
        return Arrays.stream(allPros).map(x -> x.toString()).collect(Collectors.toList());
    }
}
