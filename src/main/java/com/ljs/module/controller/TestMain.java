package com.ljs.module.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TestMain {
    private final static String STRING_TEMPLATE  =
            "<#function foreach list newLine>" +
                    "<#return newLine>" +
                    "</#function>" +
                    "${foreach(1, 2)}";

//    public static void main(String[] args) throws UnsupportedEncodingException {
//        String str = "替换 #correlate{" +
//                "test0\n" +
//                "} test #correlate{\" +\n" +
//                "                \"test1\\n\" +\n" +
//                "                \"}";
//        String regEx = "#correlate\\{[\\s\\S]*?\\}";
//        Pattern pattern = Pattern.compile(regEx);
//        Matcher matcher = pattern.matcher(str);
//        while (matcher.find()) {
//            System.out.println(matcher.group());
//        }
//        System.out.println(str.replaceAll(, "123"));
//        TemplateEngine engine = TemplateUtil.createEngine();
//        Template template = engine.getTemplate(STRING_TEMPLATE);
//        Map<String, Object> bindingMap = new HashMap<>(8);
//        Map<String, FieldMessage> map = new HashMap<>();
//        FieldMessage fieldMessage = new FieldMessage();
//        fieldMessage.setFieldName("testasd");
//        map.put("test", fieldMessage);
//        bindingMap.put("fieldMsgs", map);
//        String renderStr = template.render(bindingMap);
//        System.out.println(renderStr);
//        //默认配置就够用了
//        TemplateEngine engine = TemplateUtil.createEngine(new TemplateConfig(FileUtil.templatePath , TemplateConfig.ResourceMode.FILE));
//        //资源，根据实现不同，此资源可以是模板本身，也可以是模板的相对路径
//        Template template = engine.getTemplate("systemconfig/input.txt");
//        //给STRING_TEMPLATE绑定数据
//        Map<String, Object> bindingMap = new HashMap<>(8);
//        bindingMap.put("description", "smartbi.ext.minglue.oauth");
//        bindingMap.put("fileName", "fileName");
//        bindingMap.put("itemName", "itemName");
//        bindingMap.put("bdkey", "bdkey");
////        bindingMap.put("defaultValue", "bdkey");
//        bindingMap.put("longValue", true);
//        //最终渲染出来的样子
//        String renderStr = template.render(bindingMap);
//        System.out.println(renderStr);
//        Upgrade upgrade = new Upgrade();
//
//        upgrade.setExtendJson("{}");
//
//        System.out.println(TemplateFactoryUtil.getTemplateKeyORM(upgrade));
//    }

    public static void exeCmd(String commandStr) {
        BufferedReader br = null;
        try {
            Process p = Runtime.getRuntime().exec(commandStr);
            br = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            System.out.println(sb.toString());
            int exitValue = p.waitFor();
            System.out.println("exitValue: " + exitValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally
        {
            if (br != null)
            {
                try {
                    br.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws Exception{
        String filePath = "D:\\program data\\git data\\Projects\\YHZQ\\YhzqProcessOther\\src\\web\\vision\\js\\yhzq\\video";

//        File file = new File(filePath, "Insight.mp4");
        File file = new File(filePath, "CombinedQuery.mp4");
        long length = file.length();
        System.out.println(length);
//        System.out.println(Math.ceil(Double.parseDouble("" + 8) / Double.parseDouble("" + 3)));
//        ScriptParseService scriptParseService = new ScriptParseService();
//        Map<String, Object> map = new HashMap<>();
//        map.put("test", "test");
//        DataBean action = scriptParseService.action("D:\\program data\\intelij IDEA workpace\\SimpleDevelopForSmartbi\\template\\jointTemplete\\test.joint", map);
//        System.out.println(action.getDesc());
//        String test =  "test (123,123,123)";
//        String regx = "([\\s\\S]*?)\\(([\\s\\S]*?)\\)";
//        Pattern pattern = Pattern.compile(regx);
//        Matcher matcher = pattern.matcher(test);
//        if (matcher.find()) {
//            System.out.println(matcher.group(1));
//            System.out.println(matcher.group(2));
//        }
//        File file = new File("D:\\program data\\intelij IDEA workpace\\SimpleDevelopForSmartbi\\template\\jointTemplete", "test.joint");
//        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
//        String linkRegx = "Link ([\\s\\S]*?);";
//        Pattern linkPattern = Pattern.compile(linkRegx);
//        String exportRegx = "Export [\\s\\S]*?;";
//        Pattern exportPattern = Pattern.compile(exportRegx);
//        String functionRegx = "Function [\\s\\S]*?;";
//        Pattern functionPattern = Pattern.compile(functionRegx);
//        String tmp = "";
//
//        while ((tmp = br.readLine()) != null) {
//            tmp = tmp.trim();
//            String[] split = tmp.split("");
//            tmp = "";
//            // 去除注释
//            for (String s : split) {
//                if (s.equals("#")) {
//                    break;
//                }
//                tmp += s;
//            }
//            tmp = tmp.trim();
//            if (StringUtils.isBlank(tmp)) {
//                System.err.println("无代码");
//                continue;
//            }
//            System.out.println(tmp);
//            Matcher matcher = linkPattern.matcher(tmp);
//            if (matcher.find()) {
//                System.out.println("是link");
//                continue;
//            }
//            if (exportPattern.matcher(tmp).find()) {
//                System.out.println("是export");
//                continue;
//            }
//            if (functionPattern.matcher(tmp).find()) {
//                System.out.println("是function");
//                continue;
//            }
//        }
//        String[] links = new String[]{"&link  module;", "&link test;"};
//        String[] export = new String[]{"&export module test;", "&export test true;"};
//        String[] function = new String[]{"&function module(123,123,123);", "&function module(123,123,123);"};

//        for (String link : links) {
//            link = link.trim();
//            String[] split = link.split("");
//            link = "";
//            // 去除注释
//            for (String s : split) {
//                if (s.equals("#")) {
//                    break;
//                }
//                link += s;
//            }
//            link = link.trim();
//            if (StringUtils.isBlank(link)) {
//                System.err.println("无代码");
//                continue;
//            }
//            if (linkPattern.matcher(link).find()) {
//                System.out.println("是link");
//                continue;
//            }
//            if (exportPattern.matcher(link).find()) {
//                System.out.println("是export");
//                continue;
//            }
//            if (functionPattern.matcher(link).find()) {
//                System.out.println("是function");
//                continue;
//            }
////            System.out.println(linkPattern.matcher(link).find());
////            String[] s = link.split(" ");
////            System.out.println(Arrays.toString(s));
//        }


//        ScriptEngineManager sem=new ScriptEngineManager();
//        ScriptEngine engine=sem.getEngineByName("javascript");
//        //定义js函数
//        engine.eval("function main(oldKeysMap,newKeysMap){\n" +
//                "    newKeysMap[\"test\"] = \"123\";" +
//                "    return newKeysMap;\n" +
//                "}");
//        Invocable jsInvoke =(Invocable)engine; //Invocable是ScriptEngine的一个接口，调用函数需要强转
//        Map<String, Object> map = new HashMap<>();
//        map.put("test", "test");
//        map.put("test1", "test");
//        Map<String, Object> main = (Map<String, Object>)jsInvoke.invokeFunction("main", new Object[]{map});
//        System.out.println(main);

//        try {
//            Runtime runtime = Runtime.getRuntime();
//            // 打开任务管理器，exec方法调用后返回 Process 进程对象
//            Process process = runtime.exec("cmd.exe java -version");
//            // 等待进程对象执行完成，并返回“退出值”，0 为正常，其他为异常
//            int exitValue = process.waitFor();
//            System.out.println("exitValue: " + exitValue);
//            // 销毁process对象
//            process.destroy();
//        } catch (IOException | InterruptedException e) {
//            e.printStackTrace();
//        }
//        String commandStr = "cmd.exe /c ipconfig";
////        String commandStr = "ipconfig";
//        exeCmd(commandStr);
//        execCommandAndGetOutput();
    }

    public static void execCommandAndGetOutput() {
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec("cmd.exe /c ipconfig");
            // 输出结果，必须写在 waitFor 之前
            String outStr = getStreamStr(process.getInputStream());
            // 错误结果，必须写在 waitFor 之前
            String errStr = getStreamStr(process.getErrorStream());
            int exitValue = process.waitFor(); // 退出值 0 为正常，其他为异常
            System.out.println("exitValue: " + exitValue);
            System.out.println("outStr: " + outStr);
            System.out.println("errStr: " + errStr);
            process.destroy();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String getStreamStr(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "GBK"));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
            sb.append("\n");
        }
        br.close();
        return sb.toString();
    }
}
