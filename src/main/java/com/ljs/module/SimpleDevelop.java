package com.ljs.module;

import com.ljs.module.util.InitConfigFileUtil;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by lijunsai on 2021/07/09
 */
@SpringBootApplication
public class SimpleDevelop {
    public static void main(String[] args) {
//        SpringApplication.run(SimpleDevelop.class, args);
        SpringApplicationBuilder builder = new SpringApplicationBuilder(SimpleDevelop.class);

        builder.headless(false);

        ConfigurableApplicationContext context = builder.run(args);
        InitConfigFileUtil.init();
    }
}
