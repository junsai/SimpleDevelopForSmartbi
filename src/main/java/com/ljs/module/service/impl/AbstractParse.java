package com.ljs.module.service.impl;

import com.ljs.module.service.IScriptParse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public abstract class AbstractParse<T> implements IScriptParse<T> {

    protected Pattern pattern = Pattern.compile(getRegx());
    protected Matcher matcher = null;
    protected List<String> scripts = new ArrayList<>();

    @Override
    public boolean compile(String data) {
        matcher = pattern.matcher(data);
        boolean find = matcher.find();
        if (find) {
            scripts.add(data);
        }
        return find;
    }

    public abstract String getRegx();
}
