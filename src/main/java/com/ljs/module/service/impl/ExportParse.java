package com.ljs.module.service.impl;

import com.ljs.module.common.DataBean;
import com.ljs.module.util.Util;

import java.util.HashMap;
import java.util.Map;

public class ExportParse extends AbstractParse<Map<String, Object>> {

    Map<String, Object> map = new HashMap<>();

    @Override
    public String getRegx() {
        return "Export ([\\s\\S]*?);";
    }

    @Override
    public DataBean parse(String data) {
        String export = getMatcher().group(1);
        String[] split = export.split("=");
        if (split.length != 2) {
            return DataBean.fail(data + " 格式不规范需要填写为 key = value (= 为关键字不可出现两次及以上)");
        }
        if (Util.isNullOrEmpty(split[0], split[1])) {
            return DataBean.fail(data + " 格式不规范需要填写为 key = value 其中 key value不能为空");
        }
        split[0] = split[0].trim();
        split[1] = split[1].trim();
        map.put(split[0], Util.isBoolOrString(split[1]));
        return DataBean.success();
    }

    @Override
    public DataBean action(Map<String, Object> map) {
        return null;
    }

    @Override
    public Map<String, Object> getData() {
        return map;
    }
}