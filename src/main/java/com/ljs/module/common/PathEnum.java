package com.ljs.module.common;

import com.ljs.module.util.FileUtil;
import lombok.Getter;

import java.io.File;

@Getter
public enum PathEnum {
    API("API存储路径", FileUtil.basePath + File.separator + "api"),
    FTP("ftp缓存存储路径",FileUtil.basePath + File.separator + "ftpCache"),
    CONFIG("基础信息缓存路径",FileUtil.basePath + File.separator + "config"),
    TEMPLATE("模板缓存路径",FileUtil.basePath + File.separator + "template"),
    CUSTOM_TEMPLATE("自定义模板缓存路径",PathEnum.TEMPLATE + File.separator + "custom"),
    JOINT_TEMPLATE("联合模板缓存路径",PathEnum.TEMPLATE + File.separator + "jointTemplate");

    PathEnum(String desc, String path) {
        this.desc = desc;
        this.path = path;
        FileUtil.directoryNotExistThenMake(path);
    }

    public String toString() {
        return this.getPath();
    }
    private String desc;
    private String path;
}
