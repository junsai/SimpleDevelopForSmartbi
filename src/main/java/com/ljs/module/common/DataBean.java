package com.ljs.module.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 响应体实体
 * Created by lijunsai on 2020/12/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DataBean<T> implements Serializable {

    protected Integer reasonCode;
    protected String desc;
    protected T data;

    protected DataBean<T> setStatus(ResponseStatus status) {
        return this.setReasonCode(status.getCode()).setDesc(status.getDes());
    }

    public static DataBean fail(String desc) {
        return new DataBean().setStatus(ResponseStatus.ACTION_FAIL).setDesc(desc);
    }
    public static DataBean fail() {
        return new DataBean().setStatus(ResponseStatus.ACTION_FAIL);
    }

    public static DataBean success() {
        return new DataBean().setStatus(ResponseStatus.ACTION_SUCCESS);
    }

    public static <T> DataBean<T> fail(T data) {
        return new DataBean<T>().setData(data).setStatus(ResponseStatus.ACTION_FAIL);
    }

    public static <T> DataBean<T> success(T data) {
        return new DataBean<T>().setData(data).setStatus(ResponseStatus.ACTION_SUCCESS);
    }
}
