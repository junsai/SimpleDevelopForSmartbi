package com.ljs.module.util;

import com.ljs.module.SimpleDevelop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class InitConfigFileUtil {
    private static Map<String, String[]> files = new HashMap<>();
    static {
        files.put("/config", new String[]{"valueType.txt"});
        files.put("/template/custom/module", new String[]{"module.js","module.js.param","application.js","application.js.param"});
        files.put("/template/jointTemplate", new String[]{"template.joint"});
        files.put("/template/jointTemplate/module", new String[]{"module.joint"});
        files.put("/template/systemconfig", new String[]{"defaultExtend.json","button.js", "input.js", "radio.js", "select.js", "textarea.js"});
        files.put("/template/upgrade", new String[]{"defaultExtend.json","entity.js", "entitydao.js", "idsentity.js", "postupgrade.js", "upgrade.js"});
        files.put("/template", new String[]{"common.ftl"});
    }

    public static void init() {
        try {
            for (String path : files.keySet()) {
                String[] fileNames = files.get(path);
                if (fileNames != null && fileNames.length > 0) {
                    for (String fileName : fileNames) {
                        init(path, fileName);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void init(String path, String fileName){
        String basePath = FileUtil.basePath + path;
        File file = new File( basePath + File.separator + fileName);
        if (file.exists()) {
            return;
        }
        FileUtil.directoryNotExistThenMake(basePath);
        FileUtil.fileNotExistThenMake(basePath + File.separator + fileName);

//        String filePath = SimpleDevelop.class.getResource("resources" + File.separator + "initfile" + path + File.separator + fileName).getFile();
//        InputStream resourceAsStream = SimpleDevelop.class.getResourceAsStream(File.separator + "initfile" + path + File.separator + fileName);
//        ClassPathResource classPathResource = new ClassPathResource("initfile" + path + File.separator + fileName);
        InputStream inputStream = null;
        FileOutputStream fos = null;
        try {
//            File resource = new File(filePath);
            inputStream = SimpleDevelop.class.getResourceAsStream("/initfile" + path + "/" + fileName);
//            file = classPathResource.getFile();
            byte[] content = new byte[2048];
            fos = new FileOutputStream(file);
            int len = 0;
            while ((len = inputStream.read(content)) != -1) {
                fos.write(content,0, len);
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
