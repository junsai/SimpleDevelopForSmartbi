// 系统配置项构建
const buildConfig = Vue.component('build-config', {
    template: readComponentHTML("buildConfig"),
    data() {
        return {
            ruleForm: {
                fileName: '',
                itemName: '这是系统选项名',
                defaultValue: '',
                description: '',
                configs: [],
                type: '',
                tabNameCus: '',
                groupNameCus: '',
                tabName: '',
                groupName: '',
                path: '',
                extendJson: '{}'
            },
            resetBtn: '<input class="button-buttonbar-noimage " value="恢复初始值" type="button" style="width:100%;" >',
            defaultValue: '<input type="text" style="100%" value="这是默认值">',
            defaultBtnValue: '<input class="button-buttonbar-noimage " value="恢复初始值" type="button" style="width:100%;">',
            isLocation: true,
            isCusConfigItem: false,
            rules: {
                defaultValue: [],
                fileName: [
                    {required: true, message: '请输入系统选项文件名', trigger: 'blur'},
                ],
                itemName: [
                    {required: true, message: '请输入系统选项名称', trigger: 'blur'},
                ],
                tabName: [
                    {required: true, message: '请选择tabName', trigger: 'change'}
                ],
                groupName: [
                    {required: true, message: '请选择groupName', trigger: 'change'}
                ],
                tabNameCus: [
                    {required: true, message: '请输入自定义tabName', trigger: 'blur'}
                ],
                groupNameCus: [
                    {required: true, message: '请输入自定义groupName', trigger: 'blur'}
                ],
                path: [
                    {required: true, message: '请输入项目路径', trigger: 'blur'}
                ]
            },
            selectMenu: [],
            groupNames: {
                "": [],
                "公共设置": ["公共设置", "邮件设置", "文件导出设置", "上传文件设置", "图形设置", "服务器地址"],
                "用户管理": ["用户管理"],
                "数据集": ["数据集", "文件缓存设置"],
                "自助仪表盘": ["自助仪表盘"],
                "灵活分析|即席查询": ["灵活分析|即席查询", "字体|背景颜色设置"],
                "多维分析": ["多维分析", "字体|背景颜色设置"],
                "缓存": ["业务数据缓冲池", "数据集定义对象池", "参数缓存", "电子表格"],
                "电子表格": ["电子表格", "清单表", "水印", "文件回写位置设置"],
                "透视分析": ["透视分析", "字体|背景颜色设置"],
                // "引擎设置" : ["引擎设置"],
                // "移动端" : ["移动端设置", "离线设置"],
            }
        };
    },
    mounted() {
        this.ruleForm.path = this.$store.state.projectPath;
        this.ruleForm.configs = ["prefix", "cn", "location", "config", "propertis"];
        this.change("INPUT");
        this.getExtendJson.apply(this, ["systemconfig", "defaultExtend.json"]);
    },
    methods: {
        // 提交
        submitForm: function () {
            // alert(JSON.stringify(this.ruleForm));
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    var that = this;
                    if (this.isLocation) {
                        that.openLoading();
                        axios
                            .post(
                                '/simpledevelop//system/config/build',
                                JSON.stringify(this.ruleForm),
                                {headers: {'Content-Type': 'application/json'}})
                            .then(function (response) {
                                that.$alert(response.data, '操作结果', {
                                    dangerouslyUseHTMLString: true
                                })
                                that.closeLoading();
                            })
                            .catch(error => this.$message.error(JSON.stringify(error)));
                    } else {
                        document.getElementById("configForm").submit();
                    }

                } else {
                    this.$message.error('请完善必填信息');
                    return false;
                }
            });
        },
        // 重置
        resetForm: function () {
            this.$refs['ruleForm'].resetFields();
            this.defaultValue = '';
            this.ruleForm.itemName = '';
            this.resetBtn = '';
        },
        // 改变构建类型 给出一些初始值
        change: function (type) {
            this.ruleForm.type = type;
            switch (type) {
                case "RADIO" :
                    this.ruleForm.defaultValue = 'YES|是,NO|否';
                    this.resetBtn = this.defaultBtnValue;
                    this.rules.defaultValue = [{required: true, message: '请输入单选按钮的值', trigger: 'blur'}];
                    break;
                case "INPUT":
                    this.ruleForm.defaultValue = '输入框默认值';
                    this.ruleForm.description = '默认值( 输入框默认值 )';
                    this.resetBtn = this.defaultBtnValue;
                    this.rules.defaultValue = [];
                    break;
                case "SELECT":
                    this.ruleForm.defaultValue = 'GBK,UTF-8,TEST|测试';
                    this.ruleForm.description = '默认值( GBK )';
                    this.resetBtn = this.defaultBtnValue;
                    this.rules.defaultValue = [{required: true, message: '请输入下拉框的值', trigger: 'blur'}];
                    break;
                case "TEXT_AREA":
                    this.ruleForm.defaultValue = '';
                    this.ruleForm.description = '这是文本域描述';
                    this.resetBtn = '';
                    this.rules.defaultValue = [];
                    break;
                case "JUST_BUTTON":
                    this.ruleForm.defaultValue = '这是按钮值';
                    this.ruleForm.description = '这是按钮描述';
                    this.resetBtn = '';
                    this.rules.defaultValue = [{required: true, message: '请输入按钮的值', trigger: 'blur'}];
                    break;
                case "NO_DEFAULT_VALUE_INPUT":
                    this.ruleForm.defaultValue = '';
                    this.ruleForm.description = '这是输入框描述';
                    this.resetBtn = '';
                    this.rules.defaultValue = [];
                    break;
            }
            this.changeHTML();
        },
        // 改变中间第二个v-html绑定的参数 输入框 文本域 单选框和多选框等
        changeHTML: function () {
            switch (this.ruleForm.type) {
                case "RADIO":
                    this.defaultValue = this.radioValueToHTML();
                    break;
                case "INPUT":
                    this.defaultValue = this.inputValueToHTML();
                    break;
                case "SELECT":
                    this.defaultValue = this.selectValueToHTML();
                    break;
                case "TEXT_AREA":
                    this.defaultValue = this.textAreaValueToHTML();
                    break;
                case "JUST_BUTTON":
                    this.defaultValue = this.buttonValueToHTML();
                    break;
                case "NO_DEFAULT_VALUE_INPUT":
                    this.defaultValue = this.noDefaultInputValueToHTML();
                    break;
            }
        },
        // 通过ruleForm.defaultValue转换成HTML
        radioValueToHTML: function () {
            var values = this.ruleForm.defaultValue.split(",");
            if (!this.ruleForm.defaultValue) {
                this.ruleForm.description = '默认值(  )';
                return '';
            }
            var str = ''
            for (i in values) {
                var key = values[i];
                var value = values[i];
                if (values[i].indexOf("|") != -1) {
                    value = values[i].split("|")[1];
                }
                if (i == 0) {
                    this.ruleForm.description = '默认值( ' + value + ' )';
                }
                str += '<input type="radio" name="radio" value="' + value + '"' + ((i == 0) ? "checked" : "") + '><label>' + value + '</label>';
            }
            return str;
        },
        inputValueToHTML: function () {
            this.ruleForm.description = '默认值( ' + this.ruleForm.defaultValue + ' )';
            return '<input type="text" style="80%" value="' + this.ruleForm.defaultValue + '">';
        },
        selectValueToHTML: function () {
            var values = this.ruleForm.defaultValue.split(",");
            if (!this.ruleForm.defaultValue) {
                this.ruleForm.description = '默认值(  )';
                return '';
            }
            var str = '<select style="width: 80%">';
            for (i in values) {
                var key = values[i];
                var value = values[i];
                if (values[i].indexOf("|") != -1) {
                    value = values[i].split("|")[1];
                }
                if (i == 0) {
                    this.ruleForm.description = '默认值( ' + value + ' )';
                }
                str += '<option value="' + value + '" ' + ((i == 0) ? "checked" : "") + '>' + value + '</option>';
            }
            str += '</select>'
            return str;
        },
        textAreaValueToHTML: function () {
            return '<textarea  style="width:80%;" />' + this.ruleForm.defaultValue;
        },
        buttonValueToHTML: function () {
            return '<input value="' + this.ruleForm.defaultValue + '" type="button" style="width:80%;"/>'
        },
        noDefaultInputValueToHTML: function () {
            return '<input type="text" style="80%" value="' + this.ruleForm.defaultValue + '">';
        },
        // groupName下拉框的二级联动
        changeTabName: function (e) {
            this.ruleForm.groupName = "";
            this.selectMenu = this.groupNames[e];
        },
        // 点击自定义configItem 展示隐藏下拉框
        changeCusConfigItem: function (e) {
            this.isCusConfigItem = e;
        },
        // 点击本地模式 展示和隐藏checkbox和项目路径的input输入框
        changeisLocation: function (e) {
            this.isLocation = e;
        }
    },
    computed: {
        listenExtPath() {
            return this.$store.state.projectPath;
        },
        listenChangeHTML() {
            return this.ruleForm.defaultValue;
        }
    },
    watch: {
        listenExtPath: function (newValue, oldValue) {
            this.ruleForm.path = newValue;
        },
        listenChangeHTML: function (newValue, oldValue) {
            this.changeHTML();
        }
    }
})