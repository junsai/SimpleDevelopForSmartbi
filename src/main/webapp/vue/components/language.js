// 国际化
var language = Vue.component('build-language', {
    template: readComponentHTML("language"),
    data() {
        return {
            ruleForm: {
                filed: '',
                configs: [],
                path: ''
            },
            language: {
              en: '',
              zh: '',
              tw: ''
            },
            isLocation: true,
            rules: {
                filed: [
                    {required: true, message: '请输入系统选项文件名', trigger: 'blur'},
                ],
                path: [
                    {required: true, message: '请输入项目路径', trigger: 'blur'}
                ]
            }
        };
    },
    mounted() {
        this.setCopyElem(".btn");
        this.ruleForm.path = this.$store.state.projectPath;
        this.ruleForm.configs = ["cn", "location"];
    },
    methods: {
        // 开始国际化
        submitForm: function () {
            var that = this;
            // alert(JSON.stringify(this.ruleForm));
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    that.openLoading();
                    var postData = new URLSearchParams();
                    postData.append("filed", this.ruleForm.filed);
                    postData.append("configs", this.ruleForm.configs);
                    postData.append("path", this.ruleForm.path);
                    axios
                        .post(
                            '/simpledevelop/language/translate',
                            postData
                        ).then(function (response) {
                        that.$message({
                            message: '更新成功',
                            type: 'success'
                        });
                        that.language.en = response.data[0];
                        that.language.zh = response.data[1];
                        that.language.tw = response.data[2];
                        that.closeLoading();
                    })
                        .catch(error => this.$message.error(JSON.stringify(error)));
                } else {
                    this.$message.error('请完善必填信息');
                    return false;
                }
            });
        },
        // 重置
        resetForm: function () {
            this.$refs['ruleForm'].resetFields();
            this.language.en = '';
            this.language.zh = '';
            this.language.tw = '';
        },
        // 点击本地模式 展示和隐藏checkbox和项目路径的input输入框
        changeisLocation: function (e) {
            this.isLocation = e;
        }
    },
    computed: {
        listenExtPath() {
            return this.$store.state.projectPath;
        }
    },
    watch: {
        listenExtPath: function (newValue, oldValue) {
            this.ruleForm.path = newValue;
        }
    }
})