// extension的Tab页
var customTemplate = Vue.component('custom-Template', {
    template: readComponentHTML("customTemplate"),
    props: {
        template: {
            type: String
        },
        path: {
            type: String
        }
    },
    data() {
        return {
            ruleForm: {
                fileName: '',
                extendJson: '',
                configs: [],
                path: '',
                template: '',
            },
            tableData: [{
                field: '',
                value: '',
                isBool: false,
                isNew: false
            }],
            isSubmit: false,
            resp: '',
            isLocation: false,
            separator: '',
            rules: {
                path: [
                    {required: true, message: '请输入路径', trigger: 'blur'}
                ],
                extendJson: [
                    {required: true, message: '请输入文件名', trigger: 'blur'}
                ],
                fileName: [
                    {required: true, message: '请输入文件名', trigger: 'blur'}
                ]
            }
        }
    },
    mounted() {
        this.separator = this.$root.$data.separator;
        this.setCopyElem(".btn");
        this.ruleForm.template = this.path;
        this.getExtendJson.apply(this, ["custom" + this.separator + this.path + ".param", ""]);
    },
    methods: {
        submitForm() {
            var that = this;
            this.$refs['ruleForm'].validate((valid) => {
                if (valid) {
                    // that.openLoading(); 这里打开加载框会导致子节点不见了 todo 后期再修复
                    this.isSubmit = true;
                    var tmp = {};
                    for (let i = 0; i < this.tableData.length; i++) {
                        tmp[this.tableData[i].field] = this.tableData[i].isBool ? Boolean(this.tableData[i].value) : this.tableData[i].value;
                    }
                    this.ruleForm.extendJson = JSON.stringify(tmp);
                    axios
                        .post(
                            '/simpledevelop/template/custom/build',
                            JSON.stringify(this.ruleForm),
                            {headers: {'Content-Type': 'application/json'}})
                        .then(function (response) {
                            if (response.data.reasonCode === 2000) {
                                that.resp = response.data.data;
                            } else {
                                that.resp = response.data.desc;
                            }
                            that.isSubmit = false;
                            // that.closeLoading();
                        })
                        .catch(error => this.$message.error(JSON.stringify(error)));
                }
            })
        },
        resetForm() {
            this.ruleForm.extendJson = "";
            this.getExtendJson.apply(this, ["custom" + this.separator + this.path + ".param", ""]);
            this.ruleForm.fileName = '';
            this.ruleForm.path = '';
        },
        changeisLocation(e) {
            this.isLocation = e;
        },
        addNewRow(index) {
            var lastIndex = null;
            if (index.target) {
                lastIndex = this.tableData.length - 1;
            } else if ((index + 1) == this.tableData.length) {
                lastIndex = index;
            }
            if (lastIndex != null) {
                if (this.tableData[lastIndex].field) {
                    this.tableData.push({
                        field: '',
                        value: '',
                        isBool: false,
                        isNew: true
                    })
                } else {
                    this.$message({
                        message: '上一行字段名不为空才可以新增一行',
                        type: 'warning'
                    })
                }
            }
        },
        fieldKeyUp(index, event) {
            switch (event.keyCode) {
                case 13:
                    this.addNewRow(index);
                    break;
                case 46:
                    this.delRow(index);
                    break;
            }
        },
        delRow(index) {
            if (this.tableData.length == 1) {
                this.$message({
                    message: '请至少保留一行数据',
                    type: 'warning'
                })
                return;
            }
            if (!this.tableData[index].isNew) {
                this.$message({
                    message: '默认参数不可删除',
                    type: 'warning'
                })
                return;
            }
            this.tableData.splice(index, 1);
        },
        initTable(newValue) {
            if (this.isSubmit) {
                return;
            }
            this.tableData = [];
            if (!newValue) {
                this.tableData = [{
                    field: '',
                    value: '',
                    isBool: false,
                }];
            }
            let split = newValue.split("|");
            for (let i = 0; i < split.length; i++) {
                this.tableData.push({
                    field: split[i],
                    value: '',
                    isBool: false,
                })
            }
        },
        addTooltip(h, {column, $index}) {
            return [
                column.label,
                h(
                    'el-tooltip',
                    {
                        props: {
                            content: '在输入框回车、双击[新建]Delete[删除]', // 鼠标悬停时要展示的文字提示
                            placement: 'top' // 悬停内容展示的位置
                        }
                    },
                    [h('span', {class: {'el-icon-question': true}})] // 图标
                )
            ]
        },
    },
    computed: {
        listenExtendJson() {
            return this.ruleForm.extendJson;
        }
    },
    watch: {
        listenExtendJson: function (newValue, oldValue) {
            this.initTable(newValue);
        }
    }
})