var introduceComponent = Vue.component('introduce-component', {
    template: "",
    props: {
        type: {
            type: String
        }
    },
    render(h) {
        switch (this.type) {
            case "jointTemplate" :
                return this.introduceJointTemplate(h);
            case "customTemplate" :
                return this.introduceCustomTemplate(h);
            default :
                return h("div", {
                    domProps: {
                        innerHTML: "无简介"
                    },
                },[]);
        }
    },
    methods: {
        introduceJointTemplate(h) {
            var data = [
                h("a",{
                    attrs: {
                        href: "https://www.cnblogs.com/linjiqin/p/3388298.html",
                        target: '_blank'
                    },
                    domProps: {
                        innerHTML: "freemarker语法"
                    }
                },[]),
                this.title3(h, "存放路径"),
                "在jar包路径/template/jointTemplate 下为联合模板存放位置",
                this.title3(h, "使用说明"),
                "联合模板意思是指可以指定多个自定义模板，通过代码来决定模板如何操作",
                "通过Link引入指定模板",
                "加载所有引入的模板的参数，合并到模板参数区域",
                "扩展区域 变量操作区 可以在这里进行变量计算等",
                "扩展区域 代码区  通过内置关键词和内置函数 实现创建路径和指定模板创建到指定路径中",
                this.title3(h, "变量操作区"),
                "变量操作区 语法为javascript语法，可以处理一些变量或内置一些变量，其中函数名不可修改 必须为main",
                "oldKeysMap 存放模板参数解析出来的参数 newKeysMap 为空集合，新建变量可存放在该map中，最后return出去",
                this.title3(h, "代码区"),
                "代码区 可使用freemarker语法 例如使用 ${fileName} 引用模板参数或变量操作区返回的参数",
                "联合模板中 通过Link 关键词引用自定义模板 例如 Link module\\\\module;  需要包含;结束符，意思为引入 /template/custom/module/module.js的模板 初始化时会去加载这些模板的变量列表",
                "联合模板中 通过Export 关键词来暴露参数  例如 Export test=test; 分号结尾 使用key=value的形式",
                "联合模板中 通过Function 关键词来调用内置参数  例如 Function createFileAppend(String,String,String,boolean),更多内置函数如下",
                this.title3(h, "内置函数"),
                "Function createDir(String path) 创建一个指定目录，可以为多级",
                "Function createFile(String path, String name) 创建一个指定空文件",
                "Function createFile(String path, String name, String module) 将指定module的自定义模板解析后创建一个文件",
                "Function createFileAppend(String path, String name, String module, Boolean isAppend) 将指定module的自定义模板解析后创建一个文件,可以指定是否为拼接模式",
            ]
            return h("div", {},[this.content2JSX(h, data)]);
        },
        introduceCustomTemplate(h) {
            var data = [
                h("a",{
                    attrs: {
                        href: "https://www.cnblogs.com/linjiqin/p/3388298.html",
                        target: '_blank'
                    },
                    domProps: {
                        innerHTML: "freemarker语法"
                    }
                },[]),
                this.title3(h, "存放路径"),
                "在jar包路径/template/custom 下为自定义模板存放位置",
                this.title3(h, "使用说明"),
                "在.js的文件中编写模板文件 模板语法为freemarker",
                "在.js.param的文件中编写模板中的变量 使用 |隔开每个变量"
            ]
            return h("div", {},[this.content2JSX(h, data)]);
        },
        title3(h, desc) {
            return h("h3", {
                domProps: {
                    innerHTML: desc
                }
            })
        },
        content2JSX(h, desc) {
            var datas = [];
            for (let i in desc) {
                if (typeof(desc[i])=='string') {
                    datas.push(h("span", {
                        domProps: {
                            innerHTML: desc[i]
                        }
                    }));
                    datas.push(h("br",{},[]));
                } else {
                    datas.push(desc[i]);
                }
            }
            return datas;
        }
    }
})