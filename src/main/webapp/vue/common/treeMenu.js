// 树组件
// nodes => [
// 	"key": "",
// 	"name": "",
// 	"isFile" : false,
// 	"path": ""
// 	children:[]
// ]
var treeMenu = Vue.component('tree-Menu', {
    template: readCommonHTML("treeMenu"),
    props: {
        nodes: {
            type: Array
        }
    },
    data() {
        return {
            menuVisible: false
        }
    },
    mounted() {
    },
    methods: {
        openJointTemplate(path, name) {
            this.$emit("openJointTemplate", path, name);
        },
        // 右击事件
        rightClick(event,object,value,element){
            if(value.level > 1){
                this.menuVisible = true;
                let menu = document.querySelector("#menu");
                /* 菜单定位基于鼠标点击位置 */
                this.styleMenu(menu,event);
            }
            console.log("右键被点击的event:",event);
            console.log("右键被点击的object:",object);
            console.log("右键被点击的value:",value.level);
            console.log("右键被点击的element:",element);
        },
        foo() {
            // 取消鼠标监听事件 菜单栏
            this.menuVisible = false;
            document.removeEventListener('click', this.foo); // 要及时关掉监听，不关掉的是一个坑，不信你试试，虽然前台显示的时候没有啥毛病，加一个alert你就知道了
        },
        styleMenu(menu,event) {
            if (event.clientX > 1800) {
                menu.style.left = event.pageX  - 100 + 'px';
            } else {
                menu.style.left = event.pageX  + 1 + 'px';
            }
            document.addEventListener('click', this.foo); // 给整个document新增监听鼠标事件，点击任何位置执行foo方法
            if (event.clientY > 700) {
                menu.style.top = event.pageY  - 30 + 'px';
            } else {
                menu.style.top = event.pageY  - 10 + 'px';
            }
        },
    }
})