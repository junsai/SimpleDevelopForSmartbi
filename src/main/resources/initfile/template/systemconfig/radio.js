<#-- 创建单选 -->
var AbstractSystemConfigItem = jsloader.resolve("freequery.config.configitem.AbstractSystemConfigItem");
var domutils = jsloader.resolve("freequery.lang.domutils");
var util = jsloader.resolve("freequery.common.util");
var modalWindow = jsloader.resolve("freequery.common.modalWindow");
var dialogFactory = jsloader.resolve("freequery.dialog.dialogFactory");

var ${fileName} = function() {
    this.itemName = "${itemName}"; // 配置项名称
    this.dbkey = "${dbkey}";
};
lang.extend(${fileName}, AbstractSystemConfigItem);

${fileName}.prototype.init = function() {
    this.tr = document.createElement("tr");
    this.tr.height = "30";

    this.td1 = document.createElement("td");
    this.td1.align = "left";
    this.td1.width = "200px";
    this.td1.innerHTML = this.itemName + "${'$'}{Colon}";
    this.tr.appendChild(this.td1);

    this.td2 = document.createElement("td");
    this.td2.innerHTML = 
    <#list keys as key>
        "<input type='radio' name='${fileName}' class='_${fileName}_${key}' value='${key}' checked/><label>${values[key_index]}</label>" <#if key_has_next>+<#else>;</#if>
    </#list>
    this.tr.appendChild(this.td2);

    <#list keys as key>
    this.radio_${key} = domutils.findElementByClassName([this.tr], "_${fileName}_${key}");
    </#list>

    this.td3 = document.createElement("td");
    this.td3.innerHTML = "${description!}";
    this.tr.appendChild(this.td3);

    this.td4 = document.createElement("td");
    this.td4.innerHTML = "<input class='button-buttonbar-noimage _${fileName}Btn' value='${'$'}{Restoreoriginalvalues}' type='button' style='width:100%;'/>";
    this.tr.appendChild(this.td4);

    // ”恢复初始值“按钮
    this.initBtn = domutils.findElementByClassName([this.tr], "_${fileName}Btn");
    this.addListener(this.initBtn, "click", this.doInit, this);

    this.doInit();

    return this.tr;
}

// 恢复初始值按钮
${fileName}.prototype.doInit = function() {
    <#list keys as key>
    this.radio_${key}.checked = <#if key_index == 0>true<#else>false</#if>;
    </#list>
}

// 检查配置信息是否合法 
${fileName}.prototype.validate = function() {
    return true;
}

<#assign langValueOrValue= longValue ? string('longValue', 'value')/>
// 保存配置并返回是否保存成功，对于从系统配置表里的获取数据的配置项来说，返回一个对象
${fileName}.prototype.save = function() {
    if (!this.validate()) {
        return false;
    }
    var obj = {};
    <#list keys as key>
    <#if key_index != 0>} else </#if>if (this.radio_${key}.checked) {
        obj.${langValueOrValue} = this.radio_${key}.value;
    </#list>
    }
    obj.key = this.dbkey;
    return obj;
}

// 对于从系统配置表里的获取数据的配置项来说，需要在初始化后根据配置信息来显示
${fileName}.prototype.handleConfig = function(systemConfig) {
    for (var i in systemConfig) {
        var config = systemConfig[i];
        if (config && config.key == this.dbkey) {
            <#list keys as key>
            <#if key_index != 0>} else </#if>if (systemConfig[i].${langValueOrValue} == this.radio_${key}.value) {
                <#list keys as key2>
                this.radio_${key2}.setAttribute("__checked", <#if key2_index == key_index>true<#else>false</#if>);
                </#list>
            </#list>
            }
            break;
        }
    }
}
