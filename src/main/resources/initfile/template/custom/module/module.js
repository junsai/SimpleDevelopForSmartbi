package ${package};

import org.apache.log4j.Logger;
import smartbi.framework.IModule;

public class ${moduleName} implements IModule {

    private static Logger LOG = Logger.getLogger(${moduleName}.class);
	
	private ${moduleName}() {}

    private static ${moduleName} instance;

    /**
     * 获取${moduleName}单例
     *
     * @return 单例
     */
    public static ${moduleName} getInstance() {
        if (instance == null) {
            instance = new ${moduleName}();
        }
        return instance;
    }
	
    @Override
    public void activate() {
    }
}