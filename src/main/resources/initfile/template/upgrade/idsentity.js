<#-- 创建升级类实体类多主键时的主键实体 -->
package ${package}.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;
<#list importPackages as import>
import ${import};
</#list>
<#assign newLine= "\r\n				"/>
@Embeddable
public class ${entityName}IdsEntity implements Serializable {

	<#list idsFieldMsgs as fieldMsg>
    private ${fieldMsg.classType} ${fieldMsg.fieldName};
	</#list>
	
	<#list idsFieldMsgs as fieldMsg>
	<#list fieldMsg.annotation as annotation>
	${annotation}
	</#list>
	public ${fieldMsg.classType} ${fieldMsg.methodGet}() {
		return this.${fieldMsg.fieldName};
	}
	
	public void ${fieldMsg.methodSet}(${fieldMsg.classType} ${fieldMsg.fieldName}) {
		this.${fieldMsg.fieldName} = ${fieldMsg.fieldName};
	}
	</#list>
    private Integer test;
    private Double asdasd;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ${entityName}IdsEntity that = (${entityName}IdsEntity) o;
        return <#list idsFieldMsgs as fieldMsg>Objects.equals(${fieldMsg.fieldName}, that.${fieldMsg.fieldName})<#if fieldMsg_has_next>${newLine}& <#else></#if></#list>;
    }

    @Override
    public int hashCode() {
        return Objects.hash(<#list idsFieldMsgs as fieldMsg>${fieldMsg.fieldName}<#if (fieldMsg_index + 1) % 4 == 0>${newLine}</#if><#if fieldMsg_has_next>,</#if></#list>);
    }
}
