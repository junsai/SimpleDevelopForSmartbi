package ${package}.postupgrade;

import smartbi.composite.postupgrade.BasePostTask;

public class PostTask_${nowVersion} extends BasePostTask {

    @Override
    public String getNewVersion() {
        return "${nextVersion}";
    }
	
	<#if !less85>
    @Override
    public String getDate() {
        return "${createDate}";
    }
	</#if>
}