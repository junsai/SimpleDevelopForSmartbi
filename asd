package ;

import org.apache.log4j.Logger;
import smartbi.framework.IModule;

public class  implements IModule {

    private static Logger LOG = Logger.getLogger(.class);
	
	private () {}

    private static  instance;

    /**
     * 获取单例
     *
     * @return 单例
     */
    public static  getInstance() {
        if (instance == null) {
            instance = new ();
        }
        return instance;
    }
	
    @Override
    public void activate() {
    }
}