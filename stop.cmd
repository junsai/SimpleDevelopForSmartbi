@echo off
FOR /F "eol=; tokens=2,2 delims==" %%i IN ('findstr /i "port" setting.properties') DO set port=%%i
if "%port%" == "" (set port=18972)
for /f "tokens=5" %%a in ('netstat /ano ^| findstr %port%') do taskkill /F /pid %%a
exit;