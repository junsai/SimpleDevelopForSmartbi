[ {
  "type" : 0,
  "keyword" : "execute",
  "father" : "",
  "desc" : "execute",
  "name" : "TransformCommand.prototype.execute=function(action,node)",
  "version" : "0",
  "path" : "freequery.jump.TransformCommand",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "create",
  "father" : "",
  "desc" : "create",
  "name" : "TransformCommand.prototype.create=function(id,type,parentId,parentType,createType)",
  "version" : "0",
  "path" : "freequery.jump.TransformCommand",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createTransform",
  "father" : "",
  "desc" : "createTransform",
  "name" : "TransformCommand.prototype.createTransform=function(info)",
  "version" : "0",
  "path" : "freequery.jump.TransformCommand",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "openTransform",
  "father" : "",
  "desc" : "openTransform",
  "name" : "TransformCommand.prototype.openTransform=function(id)",
  "version" : "0",
  "path" : "freequery.jump.TransformCommand",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "TransformCommand.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.jump.TransformCommand",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "close",
  "father" : "",
  "desc" : "close",
  "name" : "TransformCommand.prototype.close=function()",
  "version" : "0",
  "path" : "freequery.jump.TransformCommand",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnClose",
  "father" : "",
  "desc" : "doOnClose",
  "name" : "TransformCommand.prototype.doOnClose=function()",
  "version" : "0",
  "path" : "freequery.jump.TransformCommand",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnNeedRefresh",
  "father" : "",
  "desc" : "doOnNeedRefresh",
  "name" : "TransformCommand.prototype.doOnNeedRefresh=function(view,id)",
  "version" : "0",
  "path" : "freequery.jump.TransformCommand",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "confirmClose",
  "father" : "",
  "desc" : "confirmClose",
  "name" : "TransformCommand.prototype.confirmClose=function(func)",
  "version" : "0",
  "path" : "freequery.jump.TransformCommand",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]