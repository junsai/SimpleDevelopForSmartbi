[ {
  "type" : 0,
  "keyword" : "setFocus",
  "father" : "",
  "desc" : "setFocus",
  "name" : "RelationPaneIntern.prototype.setFocus=function(obj)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCmd",
  "father" : "",
  "desc" : "doCmd",
  "name" : "RelationPaneIntern.prototype.doCmd=function(obj)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCmdDelete",
  "father" : "",
  "desc" : "doCmdDelete",
  "name" : "RelationPaneIntern.prototype.doCmdDelete=function(obj)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCmdModify",
  "father" : "",
  "desc" : "doCmdModify",
  "name" : "RelationPaneIntern.prototype.doCmdModify=function(obj)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "RelationPaneIntern.prototype.init=function()",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onKeydown",
  "father" : "",
  "desc" : "onKeydown",
  "name" : "RelationPaneIntern.prototype.onKeydown=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getTableById",
  "father" : "",
  "desc" : "getTableById",
  "name" : "RelationPaneIntern.prototype.getTableById=function(id)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addTable",
  "father" : "",
  "desc" : "addTable",
  "name" : "RelationPaneIntern.prototype.addTable=function(x,y,tableModel,isOffset)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "deleteTable",
  "father" : "",
  "desc" : "deleteTable",
  "name" : "RelationPaneIntern.prototype.deleteTable=function(table)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addLink",
  "father" : "",
  "desc" : "addLink",
  "name" : "RelationPaneIntern.prototype.addLink=function(linkModel)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "deleteLink",
  "father" : "",
  "desc" : "deleteLink",
  "name" : "RelationPaneIntern.prototype.deleteLink=function(link)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clearAllLinks",
  "father" : "",
  "desc" : "clearAllLinks",
  "name" : "RelationPaneIntern.prototype.clearAllLinks=function()",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "RelationPaneIntern.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getTemplateHolder",
  "father" : "",
  "desc" : "getTemplateHolder",
  "name" : "RelationPaneIntern.prototype.getTemplateHolder=function()",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMousedown",
  "father" : "",
  "desc" : "onMousedown",
  "name" : "RelationPaneIntern.prototype.onMousedown=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMouseup",
  "father" : "",
  "desc" : "onMouseup",
  "name" : "RelationPaneIntern.prototype.onMouseup=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onDocumentMousedown",
  "father" : "",
  "desc" : "onDocumentMousedown",
  "name" : "RelationPaneIntern.prototype.onDocumentMousedown=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onDocumentMouseup",
  "father" : "",
  "desc" : "onDocumentMouseup",
  "name" : "RelationPaneIntern.prototype.onDocumentMouseup=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMousemove",
  "father" : "",
  "desc" : "onMousemove",
  "name" : "RelationPaneIntern.prototype.onMousemove=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMouseover",
  "father" : "",
  "desc" : "onMouseover",
  "name" : "RelationPaneIntern.prototype.onMouseover=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMouseout",
  "father" : "",
  "desc" : "onMouseout",
  "name" : "RelationPaneIntern.prototype.onMouseout=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onContextmenu",
  "father" : "",
  "desc" : "onContextmenu",
  "name" : "RelationPaneIntern.prototype.onContextmenu=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onDragover",
  "father" : "",
  "desc" : "onDragover",
  "name" : "RelationPaneIntern.prototype.onDragover=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onDragdrop",
  "father" : "",
  "desc" : "onDragdrop",
  "name" : "RelationPaneIntern.prototype.onDragdrop=function(ev)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setEnabled",
  "father" : "",
  "desc" : "setEnabled",
  "name" : "RelationPaneIntern.prototype.setEnabled=function(enabled)",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getEnabled",
  "father" : "",
  "desc" : "getEnabled",
  "name" : "RelationPaneIntern.prototype.getEnabled=function()",
  "version" : "0",
  "path" : "freequery.relationpane.RelationPaneIntern",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]