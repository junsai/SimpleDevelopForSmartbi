[ {
  "type" : 0,
  "keyword" : "setFocus",
  "father" : "",
  "desc" : "setFocus",
  "name" : "Raphael_BaseLink.prototype.setFocus=function(setFocus)",
  "version" : "0",
  "path" : "freequery.relationpane.Raphael_BaseLink",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hide",
  "father" : "",
  "desc" : "hide",
  "name" : "Raphael_BaseLink.prototype.hide=function()",
  "version" : "0",
  "path" : "freequery.relationpane.Raphael_BaseLink",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "show",
  "father" : "",
  "desc" : "show",
  "name" : "Raphael_BaseLink.prototype.show=function()",
  "version" : "0",
  "path" : "freequery.relationpane.Raphael_BaseLink",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updatePosition",
  "father" : "",
  "desc" : "updatePosition",
  "name" : "Raphael_BaseLink.prototype.updatePosition=function(fromLeft,fromRight,toLeft,toRight)",
  "version" : "0",
  "path" : "freequery.relationpane.Raphael_BaseLink",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setPosition",
  "father" : "",
  "desc" : "setPosition",
  "name" : "Raphael_BaseLink.prototype.setPosition=function(pointA,pointB,direction)",
  "version" : "0",
  "path" : "freequery.relationpane.Raphael_BaseLink",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refresh",
  "father" : "",
  "desc" : "refresh",
  "name" : "Raphael_BaseLink.prototype.refresh=function()",
  "version" : "0",
  "path" : "freequery.relationpane.Raphael_BaseLink",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "Raphael_BaseLink.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.relationpane.Raphael_BaseLink",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "buildArrowPath",
  "father" : "",
  "desc" : "buildArrowPath",
  "name" : "Raphael_BaseLink.prototype.buildArrowPath=function(x,y,len,direction)",
  "version" : "0",
  "path" : "freequery.relationpane.Raphael_BaseLink",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "drawArrow",
  "father" : "",
  "desc" : "drawArrow",
  "name" : "Raphael_BaseLink.prototype.drawArrow=function(path)",
  "version" : "0",
  "path" : "freequery.relationpane.Raphael_BaseLink",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]