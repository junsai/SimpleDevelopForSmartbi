[ {
  "type" : 0,
  "keyword" : "render",
  "father" : "",
  "desc" : "render",
  "name" : "SystemManagerTree.prototype.render=function()",
  "version" : "0",
  "path" : "freequery.systemmanager.SystemManagerTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "canRederOnTree",
  "father" : "",
  "desc" : "canRederOnTree",
  "name" : "SystemManagerTree.prototype.canRederOnTree=function(type)",
  "version" : "0",
  "path" : "freequery.systemmanager.SystemManagerTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createTreeNode",
  "father" : "",
  "desc" : "createTreeNode",
  "name" : "SystemManagerTree.prototype.createTreeNode=function(text,level,mayHasChild,parent,id)",
  "version" : "0",
  "path" : "freequery.systemmanager.SystemManagerTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getMenuHandlers",
  "father" : "",
  "desc" : "getMenuHandlers",
  "name" : "SystemManagerTree.prototype.getMenuHandlers=function()",
  "version" : "0",
  "path" : "freequery.systemmanager.SystemManagerTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getCommandFactory",
  "father" : "",
  "desc" : "getCommandFactory",
  "name" : "SystemManagerTree.prototype.getCommandFactory=function()",
  "version" : "0",
  "path" : "freequery.systemmanager.SystemManagerTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeDataSourceSubTree",
  "father" : "",
  "desc" : "changeDataSourceSubTree",
  "name" : "SystemManagerTree.prototype.changeDataSourceSubTree=function()",
  "version" : "0",
  "path" : "freequery.systemmanager.SystemManagerTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "SystemManagerTree.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.systemmanager.SystemManagerTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "reSetEveryNode",
  "father" : "",
  "desc" : "reSetEveryNode",
  "name" : "SystemManagerTree.prototype.reSetEveryNode=function(node)",
  "version" : "0",
  "path" : "freequery.systemmanager.SystemManagerTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]