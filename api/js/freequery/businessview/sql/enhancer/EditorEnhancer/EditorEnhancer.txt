[ {
  "type" : 0,
  "keyword" : "assertChanges",
  "father" : "",
  "desc" : "assertChanges",
  "name" : "EditorEnhancer.prototype.assertChanges=function()",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "canEnhance",
  "father" : "",
  "desc" : "canEnhance",
  "name" : "EditorEnhancer.prototype.canEnhance=function(sqlEditor)",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "beforeEnhance",
  "father" : "",
  "desc" : "beforeEnhance",
  "name" : "EditorEnhancer.prototype.beforeEnhance=function(sqlEditor)",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "enhance",
  "father" : "",
  "desc" : "enhance",
  "name" : "EditorEnhancer.prototype.enhance=function(sqlEditor)",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "afterEnhance",
  "father" : "",
  "desc" : "afterEnhance",
  "name" : "EditorEnhancer.prototype.afterEnhance=function(sqlEditor)",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_doEnhance",
  "father" : "",
  "desc" : "_doEnhance",
  "name" : "EditorEnhancer.prototype._doEnhance=function(obj,name,holder)",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_doInnerEnhance",
  "father" : "",
  "desc" : "_doInnerEnhance",
  "name" : "EditorEnhancer.prototype._doInnerEnhance=function(obj,name)",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_getByPath",
  "father" : "",
  "desc" : "_getByPath",
  "name" : "EditorEnhancer.prototype._getByPath=function(obj,paths)",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_replaceDocumentListener",
  "father" : "",
  "desc" : "_replaceDocumentListener",
  "name" : "EditorEnhancer.prototype._replaceDocumentListener=function(editor,eventName,srcFunc,targetFunc)",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "log",
  "father" : "",
  "desc" : "log",
  "name" : "EditorEnhancer.prototype.log=function()",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "warn",
  "father" : "",
  "desc" : "warn",
  "name" : "EditorEnhancer.prototype.warn=function()",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getOldFunctionName",
  "father" : "",
  "desc" : "getOldFunctionName",
  "name" : "EditorEnhancer.prototype.getOldFunctionName=function(functionName)",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "EditorEnhancer.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.businessview.sql.enhancer.EditorEnhancer",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]