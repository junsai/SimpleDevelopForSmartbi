[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "BusinessViewIntern.prototype.init=function()",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initButtonsHoverEvent",
  "father" : "",
  "desc" : "initButtonsHoverEvent",
  "name" : "BusinessViewIntern.prototype.initButtonsHoverEvent=function()",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doBVContentChanged",
  "father" : "",
  "desc" : "doBVContentChanged",
  "name" : "BusinessViewIntern.prototype.doBVContentChanged=function()",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initProcParams",
  "father" : "",
  "desc" : "initProcParams",
  "name" : "BusinessViewIntern.prototype.initProcParams=function(params,selectExpr)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initJavaParams",
  "father" : "",
  "desc" : "initJavaParams",
  "name" : "BusinessViewIntern.prototype.initJavaParams=function(params,selectExpr)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setReadOnly",
  "father" : "",
  "desc" : "setReadOnly",
  "name" : "BusinessViewIntern.prototype.setReadOnly=function(readOnly)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createWorkSpace",
  "father" : "",
  "desc" : "createWorkSpace",
  "name" : "BusinessViewIntern.prototype.createWorkSpace=function(bizViewClientId,isProcQuery,dsId,bizThemeId)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doQueryRelationValueChange",
  "father" : "",
  "desc" : "doQueryRelationValueChange",
  "name" : "BusinessViewIntern.prototype.doQueryRelationValueChange=function(obj,oldId,newId)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doValueChange",
  "father" : "",
  "desc" : "doValueChange",
  "name" : "BusinessViewIntern.prototype.doValueChange=function(sender,propertyName,propertyValue,propertyPanelUser)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "BusinessViewIntern.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMousedown",
  "father" : "",
  "desc" : "onMousedown",
  "name" : "BusinessViewIntern.prototype.onMousedown=function(ev)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMousemove",
  "father" : "",
  "desc" : "onMousemove",
  "name" : "BusinessViewIntern.prototype.onMousemove=function(ev)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMouseup",
  "father" : "",
  "desc" : "onMouseup",
  "name" : "BusinessViewIntern.prototype.onMouseup=function(ev)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onDocumentMouseup",
  "father" : "",
  "desc" : "onDocumentMouseup",
  "name" : "BusinessViewIntern.prototype.onDocumentMouseup=function(ev)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setEnabled",
  "father" : "",
  "desc" : "setEnabled",
  "name" : "BusinessViewIntern.prototype.setEnabled=function(enabled)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getEnabled",
  "father" : "",
  "desc" : "getEnabled",
  "name" : "BusinessViewIntern.prototype.getEnabled=function()",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setButtonStatus",
  "father" : "",
  "desc" : "setButtonStatus",
  "name" : "BusinessViewIntern.prototype.setButtonStatus=function()",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSpaceButtonStatus",
  "father" : "",
  "desc" : "setSpaceButtonStatus",
  "name" : "BusinessViewIntern.prototype.setSpaceButtonStatus=function()",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setToolButtonDisplay",
  "father" : "",
  "desc" : "setToolButtonDisplay",
  "name" : "BusinessViewIntern.prototype.setToolButtonDisplay=function(button,display)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isDisplayForToolButton",
  "father" : "",
  "desc" : "isDisplayForToolButton",
  "name" : "BusinessViewIntern.prototype.isDisplayForToolButton=function(button)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setAuditButtonStatus",
  "father" : "",
  "desc" : "setAuditButtonStatus",
  "name" : "BusinessViewIntern.prototype.setAuditButtonStatus=function()",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setShortCutDisplay",
  "father" : "",
  "desc" : "setShortCutDisplay",
  "name" : "BusinessViewIntern.prototype.setShortCutDisplay=function(flag)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setTempTableDisabled",
  "father" : "",
  "desc" : "setTempTableDisabled",
  "name" : "BusinessViewIntern.prototype.setTempTableDisabled=function(disabled)",
  "version" : "0",
  "path" : "freequery.businessview.BusinessViewIntern",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]