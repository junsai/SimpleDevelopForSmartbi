[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "DataPreviewTab.prototype.init=function(node)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showWarmReminder",
  "father" : "",
  "desc" : "showWarmReminder",
  "name" : "DataPreviewTab.prototype.showWarmReminder=function()",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initPreviewData",
  "father" : "",
  "desc" : "initPreviewData",
  "name" : "DataPreviewTab.prototype.initPreviewData=function()",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "previewDatasetData",
  "father" : "",
  "desc" : "previewDatasetData",
  "name" : "DataPreviewTab.prototype.previewDatasetData=function(node)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshData",
  "father" : "",
  "desc" : "refreshData",
  "name" : "DataPreviewTab.prototype.refreshData=function()",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "previewProcedureData",
  "father" : "",
  "desc" : "previewProcedureData",
  "name" : "DataPreviewTab.prototype.previewProcedureData=function(node)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initResultFieldTable",
  "father" : "",
  "desc" : "initResultFieldTable",
  "name" : "DataPreviewTab.prototype.initResultFieldTable=function(fields,boxData,\t\tcaptionName, tabId, resultGrid) ",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "previewCombinedQueryData",
  "father" : "",
  "desc" : "previewCombinedQueryData",
  "name" : "DataPreviewTab.prototype.previewCombinedQueryData=function(node,type)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "combinedQueryAfterRender",
  "father" : "",
  "desc" : "combinedQueryAfterRender",
  "name" : "DataPreviewTab.prototype.combinedQueryAfterRender=function()",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getQueryClassName",
  "father" : "",
  "desc" : "getQueryClassName",
  "name" : "DataPreviewTab.prototype.getQueryClassName=function(type)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideBtnExceptRefresh",
  "father" : "",
  "desc" : "hideBtnExceptRefresh",
  "name" : "DataPreviewTab.prototype.hideBtnExceptRefresh=function(query)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "previewInsightData",
  "father" : "",
  "desc" : "previewInsightData",
  "name" : "DataPreviewTab.prototype.previewInsightData=function(node)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "afterGetInsightFieldListCallback",
  "father" : "",
  "desc" : "afterGetInsightFieldListCallback",
  "name" : "DataPreviewTab.prototype.afterGetInsightFieldListCallback=function()",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "previewOlapData",
  "father" : "",
  "desc" : "previewOlapData",
  "name" : "DataPreviewTab.prototype.previewOlapData=function(node)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "openOlapQueryViewRefreshCallback",
  "father" : "",
  "desc" : "openOlapQueryViewRefreshCallback",
  "name" : "DataPreviewTab.prototype.openOlapQueryViewRefreshCallback=function()",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideBtnExceptRefresh2",
  "father" : "",
  "desc" : "hideBtnExceptRefresh2",
  "name" : "DataPreviewTab.prototype.hideBtnExceptRefresh2=function(query)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertTrBetweenCondData",
  "father" : "",
  "desc" : "insertTrBetweenCondData",
  "name" : "DataPreviewTab.prototype.insertTrBetweenCondData=function(container,bofid)",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "DataPreviewTab.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addWaterMask",
  "father" : "",
  "desc" : "addWaterMask",
  "name" : "DataPreviewTab.prototype.addWaterMask=function()",
  "version" : "0",
  "path" : "freequery.quickview.tabs.DataPreviewTab",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]