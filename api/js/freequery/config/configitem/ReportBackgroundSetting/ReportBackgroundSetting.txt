[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "ReportBackgroundSetting.prototype.init=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.ReportBackgroundSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "ReportBackgroundSetting.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.ReportBackgroundSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "ReportBackgroundSetting.prototype.validate=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.ReportBackgroundSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "ReportBackgroundSetting.prototype.save=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.ReportBackgroundSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doInitReportBackgroundSetting",
  "father" : "",
  "desc" : "doInitReportBackgroundSetting",
  "name" : "ReportBackgroundSetting.prototype.doInitReportBackgroundSetting=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.ReportBackgroundSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "ReportBackgroundSetting.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "freequery.config.configitem.ReportBackgroundSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]