[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "WaterMarkImg.prototype.init=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "WaterMarkImg.prototype.validate=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "WaterMarkImg.prototype.save=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "uploadWatermarkNodeImageButton",
  "father" : "",
  "desc" : "uploadWatermarkNodeImageButton",
  "name" : "WaterMarkImg.prototype.uploadWatermarkNodeImageButton=function(e)",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnChooseFileClick",
  "father" : "",
  "desc" : "doOnChooseFileClick",
  "name" : "WaterMarkImg.prototype.doOnChooseFileClick=function(e)",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "deleteImageWatermark",
  "father" : "",
  "desc" : "deleteImageWatermark",
  "name" : "WaterMarkImg.prototype.deleteImageWatermark=function(deleteImg)",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnChooseFileChange",
  "father" : "",
  "desc" : "doOnChooseFileChange",
  "name" : "WaterMarkImg.prototype.doOnChooseFileChange=function(e)",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "replaceWatermarkImg",
  "father" : "",
  "desc" : "replaceWatermarkImg",
  "name" : "WaterMarkImg.prototype.replaceWatermarkImg=function(isDoOkSave)",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "deleteWatermarkImgFile",
  "father" : "",
  "desc" : "deleteWatermarkImgFile",
  "name" : "WaterMarkImg.prototype.deleteWatermarkImgFile=function(newImg)",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doFileResClick",
  "father" : "",
  "desc" : "doFileResClick",
  "name" : "WaterMarkImg.prototype.doFileResClick=function(e)",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateFileResInfo",
  "father" : "",
  "desc" : "updateFileResInfo",
  "name" : "WaterMarkImg.prototype.updateFileResInfo=function(time)",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "WaterMarkImg.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doDefaultSetting",
  "father" : "",
  "desc" : "doDefaultSetting",
  "name" : "WaterMarkImg.prototype.doDefaultSetting=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doPreImgShow",
  "father" : "",
  "desc" : "doPreImgShow",
  "name" : "WaterMarkImg.prototype.doPreImgShow=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.WaterMarkImg",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]