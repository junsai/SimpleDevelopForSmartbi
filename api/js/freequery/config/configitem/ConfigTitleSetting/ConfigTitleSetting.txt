[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "ConfigTitleSetting.prototype.init=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.ConfigTitleSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "ConfigTitleSetting.prototype.validate=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.ConfigTitleSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "ConfigTitleSetting.prototype.save=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.ConfigTitleSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doInitConfigTitleSetting",
  "father" : "",
  "desc" : "doInitConfigTitleSetting",
  "name" : "ConfigTitleSetting.prototype.doInitConfigTitleSetting=function()",
  "version" : "0",
  "path" : "freequery.config.configitem.ConfigTitleSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "ConfigTitleSetting.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "freequery.config.configitem.ConfigTitleSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getInitValue",
  "father" : "",
  "desc" : "getInitValue",
  "name" : "ConfigTitleSetting.prototype.getInitValue=function(defaultValue)",
  "version" : "0",
  "path" : "freequery.config.configitem.ConfigTitleSetting",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]