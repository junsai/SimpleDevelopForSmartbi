[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "RowPermissionView.prototype.init=function()",
  "version" : "0",
  "path" : "freequery.permission.RowPermissionView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showSimpleView",
  "father" : "",
  "desc" : "showSimpleView",
  "name" : "RowPermissionView.prototype.showSimpleView=function()",
  "version" : "0",
  "path" : "freequery.permission.RowPermissionView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showAdvancedSettingView",
  "father" : "",
  "desc" : "showAdvancedSettingView",
  "name" : "RowPermissionView.prototype.showAdvancedSettingView=function()",
  "version" : "0",
  "path" : "freequery.permission.RowPermissionView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getApplicableOption",
  "father" : "",
  "desc" : "getApplicableOption",
  "name" : "RowPermissionView.prototype.getApplicableOption=function()",
  "version" : "0",
  "path" : "freequery.permission.RowPermissionView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "applyRangeSpanClick",
  "father" : "",
  "desc" : "applyRangeSpanClick",
  "name" : "RowPermissionView.prototype.applyRangeSpanClick=function()",
  "version" : "0",
  "path" : "freequery.permission.RowPermissionView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setRowPermission",
  "father" : "",
  "desc" : "setRowPermission",
  "name" : "RowPermissionView.prototype.setRowPermission=function(items)",
  "version" : "0",
  "path" : "freequery.permission.RowPermissionView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setText",
  "father" : "",
  "desc" : "setText",
  "name" : "RowPermissionView.prototype.setText=function()",
  "version" : "0",
  "path" : "freequery.permission.RowPermissionView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectItems",
  "father" : "",
  "desc" : "getSelectItems",
  "name" : "RowPermissionView.prototype.getSelectItems=function(applyRange)",
  "version" : "0",
  "path" : "freequery.permission.RowPermissionView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]