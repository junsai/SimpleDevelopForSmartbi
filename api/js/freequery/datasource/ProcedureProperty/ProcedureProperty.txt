[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "ProcedureProperty.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "show",
  "father" : "",
  "desc" : "show",
  "name" : "ProcedureProperty.prototype.show=function(procId)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initResultFieldTable",
  "father" : "",
  "desc" : "initResultFieldTable",
  "name" : "ProcedureProperty.prototype.initResultFieldTable=function(fields,boxData,captionName,tabId,resultGrid)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getProc",
  "father" : "",
  "desc" : "getProc",
  "name" : "ProcedureProperty.prototype.getProc=function(isAutoResult)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setReadOnly",
  "father" : "",
  "desc" : "setReadOnly",
  "name" : "ProcedureProperty.prototype.setReadOnly=function(flag)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onBtnOK",
  "father" : "",
  "desc" : "onBtnOK",
  "name" : "ProcedureProperty.prototype.onBtnOK=function(e)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onBtnClose",
  "father" : "",
  "desc" : "onBtnClose",
  "name" : "ProcedureProperty.prototype.onBtnClose=function(e)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onBtnAuto",
  "father" : "",
  "desc" : "onBtnAuto",
  "name" : "ProcedureProperty.prototype.onBtnAuto=function(e)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnResultGridShowEditBox",
  "father" : "",
  "desc" : "doOnResultGridShowEditBox",
  "name" : "ProcedureProperty.prototype.doOnResultGridShowEditBox=function(grid,editor,cell)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onBtnAutoResult",
  "father" : "",
  "desc" : "onBtnAutoResult",
  "name" : "ProcedureProperty.prototype.onBtnAutoResult=function(e)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "autoDetectResult",
  "father" : "",
  "desc" : "autoDetectResult",
  "name" : "ProcedureProperty.prototype.autoDetectResult=function(ok,proc,paramsValue)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "dataTypeChanged",
  "father" : "",
  "desc" : "dataTypeChanged",
  "name" : "ProcedureProperty.prototype.dataTypeChanged=function(ev)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addAllBindParamButton",
  "father" : "",
  "desc" : "addAllBindParamButton",
  "name" : "ProcedureProperty.prototype.addAllBindParamButton=function()",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addBindParamButton",
  "father" : "",
  "desc" : "addBindParamButton",
  "name" : "ProcedureProperty.prototype.addBindParamButton=function(cell)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "bindParam",
  "father" : "",
  "desc" : "bindParam",
  "name" : "ProcedureProperty.prototype.bindParam=function(ev)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "cancelBindParam",
  "father" : "",
  "desc" : "cancelBindParam",
  "name" : "ProcedureProperty.prototype.cancelBindParam=function(ev)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAddNewRow",
  "father" : "",
  "desc" : "doAddNewRow",
  "name" : "ProcedureProperty.prototype.doAddNewRow=function(obj,row)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnInputBlur",
  "father" : "",
  "desc" : "doOnInputBlur",
  "name" : "ProcedureProperty.prototype.doOnInputBlur=function(resultTab)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnParamGridShowEditBox",
  "father" : "",
  "desc" : "doOnParamGridShowEditBox",
  "name" : "ProcedureProperty.prototype.doOnParamGridShowEditBox=function(grid,editor,cell)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doParamTypeChanged",
  "father" : "",
  "desc" : "doParamTypeChanged",
  "name" : "ProcedureProperty.prototype.doParamTypeChanged=function(ev)",
  "version" : "0",
  "path" : "freequery.datasource.ProcedureProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]