[ {
  "type" : 0,
  "keyword" : "initColumn",
  "father" : "",
  "desc" : "initColumn",
  "name" : "SimpleListView.prototype.initColumn=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "toDataItems",
  "father" : "",
  "desc" : "toDataItems",
  "name" : "SimpleListView.prototype.toDataItems=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDataItems",
  "father" : "",
  "desc" : "getDataItems",
  "name" : "SimpleListView.prototype.getDataItems=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getAllRows",
  "father" : "",
  "desc" : "getAllRows",
  "name" : "SimpleListView.prototype.getAllRows=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getRowById",
  "father" : "",
  "desc" : "getRowById",
  "name" : "SimpleListView.prototype.getRowById=function(rowId)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertSimpleRow",
  "father" : "",
  "desc" : "insertSimpleRow",
  "name" : "SimpleListView.prototype.insertSimpleRow=function(rowData)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertRow",
  "father" : "",
  "desc" : "insertRow",
  "name" : "SimpleListView.prototype.insertRow=function(rowData,doSort)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateAutoRowWidth",
  "father" : "",
  "desc" : "updateAutoRowWidth",
  "name" : "SimpleListView.prototype.updateAutoRowWidth=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCheckBoxClick",
  "father" : "",
  "desc" : "doCheckBoxClick",
  "name" : "SimpleListView.prototype.doCheckBoxClick=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getColFromRow",
  "father" : "",
  "desc" : "getColFromRow",
  "name" : "SimpleListView.prototype.getColFromRow=function(row,index)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getColsFromRow",
  "father" : "",
  "desc" : "getColsFromRow",
  "name" : "SimpleListView.prototype.getColsFromRow=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRowClick",
  "father" : "",
  "desc" : "doRowClick",
  "name" : "SimpleListView.prototype.doRowClick=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRowDblClick",
  "father" : "",
  "desc" : "doRowDblClick",
  "name" : "SimpleListView.prototype.doRowDblClick=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doMouseEnter",
  "father" : "",
  "desc" : "doMouseEnter",
  "name" : "SimpleListView.prototype.doMouseEnter=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doMouseLeave",
  "father" : "",
  "desc" : "doMouseLeave",
  "name" : "SimpleListView.prototype.doMouseLeave=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedRow",
  "father" : "",
  "desc" : "getSelectedRow",
  "name" : "SimpleListView.prototype.getSelectedRow=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSelectedRow",
  "father" : "",
  "desc" : "setSelectedRow",
  "name" : "SimpleListView.prototype.setSelectedRow=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isSelected",
  "father" : "",
  "desc" : "isSelected",
  "name" : "SimpleListView.prototype.isSelected=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setRowClass",
  "father" : "",
  "desc" : "setRowClass",
  "name" : "SimpleListView.prototype.setRowClass=function(row,cssClass)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSelectedRowsClassName",
  "father" : "",
  "desc" : "setSelectedRowsClassName",
  "name" : "SimpleListView.prototype.setSelectedRowsClassName=function(clzName)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getRowFromChild",
  "father" : "",
  "desc" : "getRowFromChild",
  "name" : "SimpleListView.prototype.getRowFromChild=function(childEl)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getNameFromRow",
  "father" : "",
  "desc" : "getNameFromRow",
  "name" : "SimpleListView.prototype.getNameFromRow=function(rowEl)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getCheckBoxFromCol",
  "father" : "",
  "desc" : "getCheckBoxFromCol",
  "name" : "SimpleListView.prototype.getCheckBoxFromCol=function(col)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getElementFromChild",
  "father" : "",
  "desc" : "getElementFromChild",
  "name" : "SimpleListView.prototype.getElementFromChild=function(childEl,parentClass)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeRow",
  "father" : "",
  "desc" : "removeRow",
  "name" : "SimpleListView.prototype.removeRow=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeSpecialRow",
  "father" : "",
  "desc" : "removeSpecialRow",
  "name" : "SimpleListView.prototype.removeSpecialRow=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeAllRows",
  "father" : "",
  "desc" : "removeAllRows",
  "name" : "SimpleListView.prototype.removeAllRows=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeAllRowsWithPurview",
  "father" : "",
  "desc" : "removeAllRowsWithPurview",
  "name" : "SimpleListView.prototype.removeAllRowsWithPurview=function(purviewType)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getBodyTable",
  "father" : "",
  "desc" : "getBodyTable",
  "name" : "SimpleListView.prototype.getBodyTable=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "appendToParent",
  "father" : "",
  "desc" : "appendToParent",
  "name" : "SimpleListView.prototype.appendToParent=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "SimpleListView.prototype.init=function(parentEl)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doKeyup",
  "father" : "",
  "desc" : "doKeyup",
  "name" : "SimpleListView.prototype.doKeyup=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "SimpleListView.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshRows",
  "father" : "",
  "desc" : "refreshRows",
  "name" : "SimpleListView.prototype.refreshRows=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showSearchBar",
  "father" : "",
  "desc" : "showSearchBar",
  "name" : "SimpleListView.prototype.showSearchBar=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideSearchBar",
  "father" : "",
  "desc" : "hideSearchBar",
  "name" : "SimpleListView.prototype.hideSearchBar=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSearch",
  "father" : "",
  "desc" : "doSearch",
  "name" : "SimpleListView.prototype.doSearch=function(condition)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClearSearch",
  "father" : "",
  "desc" : "doClearSearch",
  "name" : "SimpleListView.prototype.doClearSearch=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "autoSetRowWidth",
  "father" : "",
  "desc" : "autoSetRowWidth",
  "name" : "SimpleListView.prototype.autoSetRowWidth=function(isAuto)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setMultSelect",
  "father" : "",
  "desc" : "setMultSelect",
  "name" : "SimpleListView.prototype.setMultSelect=function(isMult)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getLastSelectedRow",
  "father" : "",
  "desc" : "getLastSelectedRow",
  "name" : "SimpleListView.prototype.getLastSelectedRow=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getRowIndex",
  "father" : "",
  "desc" : "getRowIndex",
  "name" : "SimpleListView.prototype.getRowIndex=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getRowByIndex",
  "father" : "",
  "desc" : "getRowByIndex",
  "name" : "SimpleListView.prototype.getRowByIndex=function(rowIndex)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSelectedRows",
  "father" : "",
  "desc" : "setSelectedRows",
  "name" : "SimpleListView.prototype.setSelectedRows=function(startIndex,endIndex)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "unselectRow",
  "father" : "",
  "desc" : "unselectRow",
  "name" : "SimpleListView.prototype.unselectRow=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showSearchResultTip",
  "father" : "",
  "desc" : "showSearchResultTip",
  "name" : "SimpleListView.prototype.showSearchResultTip=function(tip)",
  "version" : "0",
  "path" : "freequery.control.SimpleListView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]