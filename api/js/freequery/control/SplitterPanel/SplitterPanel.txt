[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "SplitterPanel.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "SplitterPanel.prototype.doClose=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getTemplate",
  "father" : "",
  "desc" : "getTemplate",
  "name" : "SplitterPanel.prototype.getTemplate=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setLeftPaneWidthRange",
  "father" : "",
  "desc" : "setLeftPaneWidthRange",
  "name" : "SplitterPanel.prototype.setLeftPaneWidthRange=function(range)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isNum",
  "father" : "",
  "desc" : "isNum",
  "name" : "SplitterPanel.prototype.isNum=function(num)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getElemTop",
  "father" : "",
  "desc" : "getElemTop",
  "name" : "SplitterPanel.prototype.getElemTop=function(obj)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onHspSplitterClick",
  "father" : "",
  "desc" : "onHspSplitterClick",
  "name" : "SplitterPanel.prototype.onHspSplitterClick=function(e)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setLeftViewVisible",
  "father" : "",
  "desc" : "setLeftViewVisible",
  "name" : "SplitterPanel.prototype.setLeftViewVisible=function(visible)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isLeftViewVisible",
  "father" : "",
  "desc" : "isLeftViewVisible",
  "name" : "SplitterPanel.prototype.isLeftViewVisible=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideLeftView",
  "father" : "",
  "desc" : "hideLeftView",
  "name" : "SplitterPanel.prototype.hideLeftView=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showLeftView",
  "father" : "",
  "desc" : "showLeftView",
  "name" : "SplitterPanel.prototype.showLeftView=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideLocationBar",
  "father" : "",
  "desc" : "hideLocationBar",
  "name" : "SplitterPanel.prototype.hideLocationBar=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showLocationBar",
  "father" : "",
  "desc" : "showLocationBar",
  "name" : "SplitterPanel.prototype.showLocationBar=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setFrameTitleLocal",
  "father" : "",
  "desc" : "setFrameTitleLocal",
  "name" : "SplitterPanel.prototype.setFrameTitleLocal=function(titleLocal)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getFrameTitleLocal",
  "father" : "",
  "desc" : "getFrameTitleLocal",
  "name" : "SplitterPanel.prototype.getFrameTitleLocal=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setFrameTitle",
  "father" : "",
  "desc" : "setFrameTitle",
  "name" : "SplitterPanel.prototype.setFrameTitle=function(title)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getFrameTitle",
  "father" : "",
  "desc" : "getFrameTitle",
  "name" : "SplitterPanel.prototype.getFrameTitle=function()",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setFrameCloseButtonVisibility",
  "father" : "",
  "desc" : "setFrameCloseButtonVisibility",
  "name" : "SplitterPanel.prototype.setFrameCloseButtonVisibility=function(visible)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setFrameMaxButtonVisibility",
  "father" : "",
  "desc" : "setFrameMaxButtonVisibility",
  "name" : "SplitterPanel.prototype.setFrameMaxButtonVisibility=function(visible)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "mouseDownToResize",
  "father" : "",
  "desc" : "mouseDownToResize",
  "name" : "SplitterPanel.prototype.mouseDownToResize=function(e)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "mouseMoveToResize",
  "father" : "",
  "desc" : "mouseMoveToResize",
  "name" : "SplitterPanel.prototype.mouseMoveToResize=function(e)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "mouseUpToResize",
  "father" : "",
  "desc" : "mouseUpToResize",
  "name" : "SplitterPanel.prototype.mouseUpToResize=function(e)",
  "version" : "0",
  "path" : "freequery.control.SplitterPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]