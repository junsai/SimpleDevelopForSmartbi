[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "DropDownBox.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "dropDown",
  "father" : "",
  "desc" : "dropDown",
  "name" : "DropDownBox.prototype.dropDown=function(x,y,width,height)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "itemsCount",
  "father" : "",
  "desc" : "itemsCount",
  "name" : "DropDownBox.prototype.itemsCount=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createItemHTML",
  "father" : "",
  "desc" : "createItemHTML",
  "name" : "DropDownBox.prototype.createItemHTML=function(id,caption,data,imgs,colors)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertItem",
  "father" : "",
  "desc" : "insertItem",
  "name" : "DropDownBox.prototype.insertItem=function(id,caption,data,imgs)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertItems",
  "father" : "",
  "desc" : "insertItems",
  "name" : "DropDownBox.prototype.insertItems=function(data)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeItem",
  "father" : "",
  "desc" : "removeItem",
  "name" : "DropDownBox.prototype.removeItem=function(id)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doMouseOver",
  "father" : "",
  "desc" : "doMouseOver",
  "name" : "DropDownBox.prototype.doMouseOver=function(e)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setHighLightItem",
  "father" : "",
  "desc" : "setHighLightItem",
  "name" : "DropDownBox.prototype.setHighLightItem=function(item,oldIndex)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doItemClick",
  "father" : "",
  "desc" : "doItemClick",
  "name" : "DropDownBox.prototype.doItemClick=function(e)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCheckItemClick",
  "father" : "",
  "desc" : "doCheckItemClick",
  "name" : "DropDownBox.prototype.doCheckItemClick=function(e)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateItemClick",
  "father" : "",
  "desc" : "updateItemClick",
  "name" : "DropDownBox.prototype.updateItemClick=function(checkbox,isClickCheckBox)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resetItemClass",
  "father" : "",
  "desc" : "resetItemClass",
  "name" : "DropDownBox.prototype.resetItemClass=function(s)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setItemCheckedById",
  "father" : "",
  "desc" : "setItemCheckedById",
  "name" : "DropDownBox.prototype.setItemCheckedById=function(id,checked)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemById",
  "father" : "",
  "desc" : "getItemById",
  "name" : "DropDownBox.prototype.getItemById=function(id)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemCheckedById",
  "father" : "",
  "desc" : "getItemCheckedById",
  "name" : "DropDownBox.prototype.getItemCheckedById=function(id)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemNameById",
  "father" : "",
  "desc" : "getItemNameById",
  "name" : "DropDownBox.prototype.getItemNameById=function(id)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemNameByName",
  "father" : "",
  "desc" : "getItemNameByName",
  "name" : "DropDownBox.prototype.getItemNameByName=function(name)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemIdByIndex",
  "father" : "",
  "desc" : "getItemIdByIndex",
  "name" : "DropDownBox.prototype.getItemIdByIndex=function(index)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedId",
  "father" : "",
  "desc" : "getSelectedId",
  "name" : "DropDownBox.prototype.getSelectedId=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedValue",
  "father" : "",
  "desc" : "getSelectedValue",
  "name" : "DropDownBox.prototype.getSelectedValue=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "allChecked",
  "father" : "",
  "desc" : "allChecked",
  "name" : "DropDownBox.prototype.allChecked=function(skipItemAll)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clearChecked",
  "father" : "",
  "desc" : "clearChecked",
  "name" : "DropDownBox.prototype.clearChecked=function(skipItemAll)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setChecked",
  "father" : "",
  "desc" : "setChecked",
  "name" : "DropDownBox.prototype.setChecked=function(values)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "reverseChecked",
  "father" : "",
  "desc" : "reverseChecked",
  "name" : "DropDownBox.prototype.reverseChecked=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateItemAllChecked",
  "father" : "",
  "desc" : "updateItemAllChecked",
  "name" : "DropDownBox.prototype.updateItemAllChecked=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getStandByData",
  "father" : "",
  "desc" : "getStandByData",
  "name" : "DropDownBox.prototype.getStandByData=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clear",
  "father" : "",
  "desc" : "clear",
  "name" : "DropDownBox.prototype.clear=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "cancelDiv",
  "father" : "",
  "desc" : "cancelDiv",
  "name" : "DropDownBox.prototype.cancelDiv=function(e)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setItemReadOnly",
  "father" : "",
  "desc" : "setItemReadOnly",
  "name" : "DropDownBox.prototype.setItemReadOnly=function(id,readOnly)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemClass",
  "father" : "",
  "desc" : "getItemClass",
  "name" : "DropDownBox.prototype.getItemClass=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setItemClass",
  "father" : "",
  "desc" : "setItemClass",
  "name" : "DropDownBox.prototype.setItemClass=function(className)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getMouseOverItemClass",
  "father" : "",
  "desc" : "getMouseOverItemClass",
  "name" : "DropDownBox.prototype.getMouseOverItemClass=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setMousemoveItemClass",
  "father" : "",
  "desc" : "setMousemoveItemClass",
  "name" : "DropDownBox.prototype.setMousemoveItemClass=function(className)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setPanelClass",
  "father" : "",
  "desc" : "setPanelClass",
  "name" : "DropDownBox.prototype.setPanelClass=function(className)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "scrollIntoView",
  "father" : "",
  "desc" : "scrollIntoView",
  "name" : "DropDownBox.prototype.scrollIntoView=function(item)",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clearSelectItem",
  "father" : "",
  "desc" : "clearSelectItem",
  "name" : "DropDownBox.prototype.clearSelectItem=function()",
  "version" : "0",
  "path" : "freequery.control.DropDownBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]