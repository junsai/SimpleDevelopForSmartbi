[ {
  "type" : 0,
  "keyword" : "_parse",
  "father" : "",
  "desc" : "_parse",
  "name" : "_parse:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_wrapFunc",
  "father" : "",
  "desc" : "_wrapFunc",
  "name" : "_wrapFunc:function(pattern)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_statisticsPatternCounter",
  "father" : "",
  "desc" : "_statisticsPatternCounter",
  "name" : "_statisticsPatternCounter:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resolveText",
  "father" : "",
  "desc" : "resolveText",
  "name" : "resolveText:function(text)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isMatchSeqLen",
  "father" : "",
  "desc" : "isMatchSeqLen",
  "name" : "isMatchSeqLen:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getType",
  "father" : "",
  "desc" : "getType",
  "name" : "getType:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "formatVal",
  "father" : "",
  "desc" : "formatVal",
  "name" : "formatVal:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doFormatVal",
  "father" : "",
  "desc" : "doFormatVal",
  "name" : "doFormatVal:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "formatByPattern",
  "father" : "",
  "desc" : "formatByPattern",
  "name" : "formatByPattern:function(text,pattern)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "validate:function(fieldDesc)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doValidate",
  "father" : "",
  "desc" : "doValidate",
  "name" : "doValidate:function(fieldDesc)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "minLen",
  "father" : "",
  "desc" : "minLen",
  "name" : "minLen:function(pc)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "maxLen",
  "father" : "",
  "desc" : "maxLen",
  "name" : "maxLen:function(pc)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "add",
  "father" : "",
  "desc" : "add",
  "name" : "add:function(val)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "minLen",
  "father" : "",
  "desc" : "minLen",
  "name" : "minLen:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "maxLen",
  "father" : "",
  "desc" : "maxLen",
  "name" : "maxLen:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resolve",
  "father" : "",
  "desc" : "resolve",
  "name" : "resolve:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "matches",
  "father" : "",
  "desc" : "matches",
  "name" : "matches:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemValue",
  "father" : "",
  "desc" : "getItemValue",
  "name" : "getItemValue:function(c)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSeqsIndex",
  "father" : "",
  "desc" : "getSeqsIndex",
  "name" : "getSeqsIndex:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSeq",
  "father" : "",
  "desc" : "getSeq",
  "name" : "getSeq:function(i)",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_initVariableSeq",
  "father" : "",
  "desc" : "_initVariableSeq",
  "name" : "_initVariableSeq:function()",
  "version" : "0",
  "path" : "freequery.control.MultiCalendarFormatFactory",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]