[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "FilterPanels.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.FilterPanels",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clear",
  "father" : "",
  "desc" : "clear",
  "name" : "FilterPanels.prototype.clear=function()",
  "version" : "0",
  "path" : "freequery.control.FilterPanels",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resetFlagRefresh",
  "father" : "",
  "desc" : "resetFlagRefresh",
  "name" : "FilterPanels.prototype.resetFlagRefresh=function(flag)",
  "version" : "0",
  "path" : "freequery.control.FilterPanels",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addItem",
  "father" : "",
  "desc" : "addItem",
  "name" : "FilterPanels.prototype.addItem=function(fieldObj)",
  "version" : "0",
  "path" : "freequery.control.FilterPanels",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemByFieldId",
  "father" : "",
  "desc" : "getItemByFieldId",
  "name" : "FilterPanels.prototype.getItemByFieldId=function(fieldId)",
  "version" : "0",
  "path" : "freequery.control.FilterPanels",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]