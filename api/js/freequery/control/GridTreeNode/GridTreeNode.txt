[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "GridTreeNode.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addChild",
  "father" : "",
  "desc" : "addChild",
  "name" : "GridTreeNode.prototype.addChild=function(text,mayHasChild,id)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeChild",
  "father" : "",
  "desc" : "removeChild",
  "name" : "GridTreeNode.prototype.removeChild=function(indexOrNode)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeAllChildren",
  "father" : "",
  "desc" : "removeAllChildren",
  "name" : "GridTreeNode.prototype.removeAllChildren=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDescendantsCount",
  "father" : "",
  "desc" : "getDescendantsCount",
  "name" : "GridTreeNode.prototype.getDescendantsCount=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initExpanderImg",
  "father" : "",
  "desc" : "initExpanderImg",
  "name" : "GridTreeNode.prototype.initExpanderImg=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setText",
  "father" : "",
  "desc" : "setText",
  "name" : "GridTreeNode.prototype.setText=function(text)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getExpanded",
  "father" : "",
  "desc" : "getExpanded",
  "name" : "GridTreeNode.prototype.getExpanded=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setExpanded",
  "father" : "",
  "desc" : "setExpanded",
  "name" : "GridTreeNode.prototype.setExpanded=function(expanded)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshChildRegion",
  "father" : "",
  "desc" : "refreshChildRegion",
  "name" : "GridTreeNode.prototype.refreshChildRegion=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setVisible",
  "father" : "",
  "desc" : "setVisible",
  "name" : "GridTreeNode.prototype.setVisible=function(visible,recursive)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hasChild",
  "father" : "",
  "desc" : "hasChild",
  "name" : "GridTreeNode.prototype.hasChild=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setIcon",
  "father" : "",
  "desc" : "setIcon",
  "name" : "GridTreeNode.prototype.setIcon=function(icon,isFont)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshPrefixInternal",
  "father" : "",
  "desc" : "refreshPrefixInternal",
  "name" : "GridTreeNode.prototype.refreshPrefixInternal=function(level,src)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshPrefix",
  "father" : "",
  "desc" : "refreshPrefix",
  "name" : "GridTreeNode.prototype.refreshPrefix=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnMouseDown",
  "father" : "",
  "desc" : "doOnMouseDown",
  "name" : "GridTreeNode.prototype.doOnMouseDown=function(e,target)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnTextSpanMouseDown",
  "father" : "",
  "desc" : "doOnTextSpanMouseDown",
  "name" : "GridTreeNode.prototype.doOnTextSpanMouseDown=function(e)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initChildren",
  "father" : "",
  "desc" : "initChildren",
  "name" : "GridTreeNode.prototype.initChildren=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnExpanderMouseDown",
  "father" : "",
  "desc" : "doOnExpanderMouseDown",
  "name" : "GridTreeNode.prototype.doOnExpanderMouseDown=function(e)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getChildNodeByNodeId",
  "father" : "",
  "desc" : "getChildNodeByNodeId",
  "name" : "GridTreeNode.prototype.getChildNodeByNodeId=function(id)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnMouseUp",
  "father" : "",
  "desc" : "doOnMouseUp",
  "name" : "GridTreeNode.prototype.doOnMouseUp=function(e)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createTransferData",
  "father" : "",
  "desc" : "createTransferData",
  "name" : "GridTreeNode.prototype.createTransferData=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createTransferDatas",
  "father" : "",
  "desc" : "createTransferDatas",
  "name" : "GridTreeNode.prototype.createTransferDatas=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnMouseMove",
  "father" : "",
  "desc" : "doOnMouseMove",
  "name" : "GridTreeNode.prototype.doOnMouseMove=function(e)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "moveTo",
  "father" : "",
  "desc" : "moveTo",
  "name" : "GridTreeNode.prototype.moveTo=function(parent,before)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_updateLevel",
  "father" : "",
  "desc" : "_updateLevel",
  "name" : "GridTreeNode.prototype._updateLevel=function(level)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_moveRow",
  "father" : "",
  "desc" : "_moveRow",
  "name" : "GridTreeNode.prototype._moveRow=function(domBefore)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_getDomBefore",
  "father" : "",
  "desc" : "_getDomBefore",
  "name" : "GridTreeNode.prototype._getDomBefore=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setCheckBoxType",
  "father" : "",
  "desc" : "setCheckBoxType",
  "name" : "GridTreeNode.prototype.setCheckBoxType=function(checkBoxType)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnCheckBox",
  "father" : "",
  "desc" : "doOnCheckBox",
  "name" : "GridTreeNode.prototype.doOnCheckBox=function(e)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setChecked",
  "father" : "",
  "desc" : "setChecked",
  "name" : "GridTreeNode.prototype.setChecked=function(checked)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getParentNodes",
  "father" : "",
  "desc" : "getParentNodes",
  "name" : "GridTreeNode.prototype.getParentNodes=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getNodePath",
  "father" : "",
  "desc" : "getNodePath",
  "name" : "GridTreeNode.prototype.getNodePath=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnMouseover",
  "father" : "",
  "desc" : "doOnMouseover",
  "name" : "GridTreeNode.prototype.doOnMouseover=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnMouseout",
  "father" : "",
  "desc" : "doOnMouseout",
  "name" : "GridTreeNode.prototype.doOnMouseout=function()",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderColumn",
  "father" : "",
  "desc" : "renderColumn",
  "name" : "GridTreeNode.prototype.renderColumn=function(renderFunc,that)",
  "version" : "0",
  "path" : "freequery.control.GridTreeNode",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]