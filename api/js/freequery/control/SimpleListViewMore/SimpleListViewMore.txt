[ {
  "type" : 0,
  "keyword" : "initColumn",
  "father" : "",
  "desc" : "initColumn",
  "name" : "SimpleListViewMore.prototype.initColumn=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "groupToData",
  "father" : "",
  "desc" : "groupToData",
  "name" : "SimpleListViewMore.prototype.groupToData=function(groupInfo)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fill",
  "father" : "",
  "desc" : "fill",
  "name" : "SimpleListViewMore.prototype.fill=function(bean)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "SimpleListViewMore.prototype.getValue=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "toDataItems",
  "father" : "",
  "desc" : "toDataItems",
  "name" : "SimpleListViewMore.prototype.toDataItems=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDataItems",
  "father" : "",
  "desc" : "getDataItems",
  "name" : "SimpleListViewMore.prototype.getDataItems=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getAllRows",
  "father" : "",
  "desc" : "getAllRows",
  "name" : "SimpleListViewMore.prototype.getAllRows=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getRowById",
  "father" : "",
  "desc" : "getRowById",
  "name" : "SimpleListViewMore.prototype.getRowById=function(rowId)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertSimpleRow",
  "father" : "",
  "desc" : "insertSimpleRow",
  "name" : "SimpleListViewMore.prototype.insertSimpleRow=function(rowData)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "toArray",
  "father" : "",
  "desc" : "toArray",
  "name" : "SimpleListViewMore.prototype.toArray=function(elems)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setListPanelHTML",
  "father" : "",
  "desc" : "setListPanelHTML",
  "name" : "SimpleListViewMore.prototype.setListPanelHTML=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createMoreItemHTML",
  "father" : "",
  "desc" : "createMoreItemHTML",
  "name" : "SimpleListViewMore.prototype.createMoreItemHTML=function(pageType)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doMoreItemClick",
  "father" : "",
  "desc" : "doMoreItemClick",
  "name" : "SimpleListViewMore.prototype.doMoreItemClick=function(obj,limitNum)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addGroups",
  "father" : "",
  "desc" : "addGroups",
  "name" : "SimpleListViewMore.prototype.addGroups=function(data)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addLimitGroups",
  "father" : "",
  "desc" : "addLimitGroups",
  "name" : "SimpleListViewMore.prototype.addLimitGroups=function(data)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addGroup",
  "father" : "",
  "desc" : "addGroup",
  "name" : "SimpleListViewMore.prototype.addGroup=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedIndex",
  "father" : "",
  "desc" : "getSelectedIndex",
  "name" : "SimpleListViewMore.prototype.getSelectedIndex=function(item)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "items",
  "father" : "",
  "desc" : "items",
  "name" : "SimpleListViewMore.prototype.items=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemIndex",
  "father" : "",
  "desc" : "getItemIndex",
  "name" : "SimpleListViewMore.prototype.getItemIndex=function(item)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "deleteItem",
  "father" : "",
  "desc" : "deleteItem",
  "name" : "SimpleListViewMore.prototype.deleteItem=function(item)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isExist",
  "father" : "",
  "desc" : "isExist",
  "name" : "SimpleListViewMore.prototype.isExist=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertRow",
  "father" : "",
  "desc" : "insertRow",
  "name" : "SimpleListViewMore.prototype.insertRow=function(rowData,doSort)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateAutoRowWidth",
  "father" : "",
  "desc" : "updateAutoRowWidth",
  "name" : "SimpleListViewMore.prototype.updateAutoRowWidth=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCheckBoxClick",
  "father" : "",
  "desc" : "doCheckBoxClick",
  "name" : "SimpleListViewMore.prototype.doCheckBoxClick=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getColFromRow",
  "father" : "",
  "desc" : "getColFromRow",
  "name" : "SimpleListViewMore.prototype.getColFromRow=function(row,index)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getColsFromRow",
  "father" : "",
  "desc" : "getColsFromRow",
  "name" : "SimpleListViewMore.prototype.getColsFromRow=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRowClick",
  "father" : "",
  "desc" : "doRowClick",
  "name" : "SimpleListViewMore.prototype.doRowClick=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRowDblClick",
  "father" : "",
  "desc" : "doRowDblClick",
  "name" : "SimpleListViewMore.prototype.doRowDblClick=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doMouseEnter",
  "father" : "",
  "desc" : "doMouseEnter",
  "name" : "SimpleListViewMore.prototype.doMouseEnter=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doMouseLeave",
  "father" : "",
  "desc" : "doMouseLeave",
  "name" : "SimpleListViewMore.prototype.doMouseLeave=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setDefault",
  "father" : "",
  "desc" : "setDefault",
  "name" : "SimpleListViewMore.prototype.setDefault=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedRow",
  "father" : "",
  "desc" : "getSelectedRow",
  "name" : "SimpleListViewMore.prototype.getSelectedRow=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSelectedRow",
  "father" : "",
  "desc" : "setSelectedRow",
  "name" : "SimpleListViewMore.prototype.setSelectedRow=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isSelected",
  "father" : "",
  "desc" : "isSelected",
  "name" : "SimpleListViewMore.prototype.isSelected=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setRowClass",
  "father" : "",
  "desc" : "setRowClass",
  "name" : "SimpleListViewMore.prototype.setRowClass=function(row,cssClass)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSelectedRowsClassName",
  "father" : "",
  "desc" : "setSelectedRowsClassName",
  "name" : "SimpleListViewMore.prototype.setSelectedRowsClassName=function(clzName)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getRowFromChild",
  "father" : "",
  "desc" : "getRowFromChild",
  "name" : "SimpleListViewMore.prototype.getRowFromChild=function(childEl)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getNameFromRow",
  "father" : "",
  "desc" : "getNameFromRow",
  "name" : "SimpleListViewMore.prototype.getNameFromRow=function(rowEl)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getCheckBoxFromCol",
  "father" : "",
  "desc" : "getCheckBoxFromCol",
  "name" : "SimpleListViewMore.prototype.getCheckBoxFromCol=function(col)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getElementFromChild",
  "father" : "",
  "desc" : "getElementFromChild",
  "name" : "SimpleListViewMore.prototype.getElementFromChild=function(childEl,parentClass)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeRow",
  "father" : "",
  "desc" : "removeRow",
  "name" : "SimpleListViewMore.prototype.removeRow=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeSpecialRow",
  "father" : "",
  "desc" : "removeSpecialRow",
  "name" : "SimpleListViewMore.prototype.removeSpecialRow=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeAllItemsButDefault",
  "father" : "",
  "desc" : "removeAllItemsButDefault",
  "name" : "SimpleListViewMore.prototype.removeAllItemsButDefault=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeAllRows",
  "father" : "",
  "desc" : "removeAllRows",
  "name" : "SimpleListViewMore.prototype.removeAllRows=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeMoreItem",
  "father" : "",
  "desc" : "removeMoreItem",
  "name" : "SimpleListViewMore.prototype.removeMoreItem=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeAllRowsWithPurview",
  "father" : "",
  "desc" : "removeAllRowsWithPurview",
  "name" : "SimpleListViewMore.prototype.removeAllRowsWithPurview=function(purviewType)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getBodyTable",
  "father" : "",
  "desc" : "getBodyTable",
  "name" : "SimpleListViewMore.prototype.getBodyTable=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "appendToParent",
  "father" : "",
  "desc" : "appendToParent",
  "name" : "SimpleListViewMore.prototype.appendToParent=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "SimpleListViewMore.prototype.init=function(parentEl)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doKeyup",
  "father" : "",
  "desc" : "doKeyup",
  "name" : "SimpleListViewMore.prototype.doKeyup=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "SimpleListViewMore.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshRows",
  "father" : "",
  "desc" : "refreshRows",
  "name" : "SimpleListViewMore.prototype.refreshRows=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showSearchBar",
  "father" : "",
  "desc" : "showSearchBar",
  "name" : "SimpleListViewMore.prototype.showSearchBar=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideSearchBar",
  "father" : "",
  "desc" : "hideSearchBar",
  "name" : "SimpleListViewMore.prototype.hideSearchBar=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSearch",
  "father" : "",
  "desc" : "doSearch",
  "name" : "SimpleListViewMore.prototype.doSearch=function(condition)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClearSearch",
  "father" : "",
  "desc" : "doClearSearch",
  "name" : "SimpleListViewMore.prototype.doClearSearch=function()",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "autoSetRowWidth",
  "father" : "",
  "desc" : "autoSetRowWidth",
  "name" : "SimpleListViewMore.prototype.autoSetRowWidth=function(isAuto)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setMultSelect",
  "father" : "",
  "desc" : "setMultSelect",
  "name" : "SimpleListViewMore.prototype.setMultSelect=function(isMult)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getLastSelectedRow",
  "father" : "",
  "desc" : "getLastSelectedRow",
  "name" : "SimpleListViewMore.prototype.getLastSelectedRow=function(e)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getRowIndex",
  "father" : "",
  "desc" : "getRowIndex",
  "name" : "SimpleListViewMore.prototype.getRowIndex=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getRowByIndex",
  "father" : "",
  "desc" : "getRowByIndex",
  "name" : "SimpleListViewMore.prototype.getRowByIndex=function(rowIndex)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSelectedRows",
  "father" : "",
  "desc" : "setSelectedRows",
  "name" : "SimpleListViewMore.prototype.setSelectedRows=function(startIndex,endIndex)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "unselectRow",
  "father" : "",
  "desc" : "unselectRow",
  "name" : "SimpleListViewMore.prototype.unselectRow=function(row)",
  "version" : "0",
  "path" : "freequery.control.SimpleListViewMore",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]