[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "CatalogTreeOptions.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.CatalogTreeOptions",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "render",
  "father" : "",
  "desc" : "render",
  "name" : "CatalogTreeOptions.prototype.render=function(dataItems)",
  "version" : "0",
  "path" : "freequery.control.CatalogTreeOptions",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "unrender",
  "father" : "",
  "desc" : "unrender",
  "name" : "CatalogTreeOptions.prototype.unrender=function()",
  "version" : "0",
  "path" : "freequery.control.CatalogTreeOptions",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedItems",
  "father" : "",
  "desc" : "getSelectedItems",
  "name" : "CatalogTreeOptions.prototype.getSelectedItems=function()",
  "version" : "0",
  "path" : "freequery.control.CatalogTreeOptions",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getAllItems",
  "father" : "",
  "desc" : "getAllItems",
  "name" : "CatalogTreeOptions.prototype.getAllItems=function(callbackFunc,that)",
  "version" : "0",
  "path" : "freequery.control.CatalogTreeOptions",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "searchItems",
  "father" : "",
  "desc" : "searchItems",
  "name" : "CatalogTreeOptions.prototype.searchItems=function(text,searchCallbackFunc,that)",
  "version" : "0",
  "path" : "freequery.control.CatalogTreeOptions",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedNodes",
  "father" : "",
  "desc" : "getSelectedNodes",
  "name" : "CatalogTreeOptions.prototype.getSelectedNodes=function()",
  "version" : "0",
  "path" : "freequery.control.CatalogTreeOptions",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "treeNodesToListviewNodes",
  "father" : "",
  "desc" : "treeNodesToListviewNodes",
  "name" : "CatalogTreeOptions.prototype.treeNodesToListviewNodes=function(treeNodes)",
  "version" : "0",
  "path" : "freequery.control.CatalogTreeOptions",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]