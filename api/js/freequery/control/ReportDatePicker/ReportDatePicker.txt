[ {
  "type" : 0,
  "keyword" : "doEditBlur",
  "father" : "",
  "desc" : "doEditBlur",
  "name" : "ReportDatePicker.prototype.doEditBlur=function()",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setWidth",
  "father" : "",
  "desc" : "setWidth",
  "name" : "ReportDatePicker.prototype.setWidth=function(width)",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setType",
  "father" : "",
  "desc" : "setType",
  "name" : "ReportDatePicker.prototype.setType=function(type)",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doTypeChange",
  "father" : "",
  "desc" : "doTypeChange",
  "name" : "ReportDatePicker.prototype.doTypeChange=function(obj,type)",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "btnClick",
  "father" : "",
  "desc" : "btnClick",
  "name" : "ReportDatePicker.prototype.btnClick=function(e)",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "ReportDatePicker.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroyCalendar",
  "father" : "",
  "desc" : "destroyCalendar",
  "name" : "ReportDatePicker.prototype.destroyCalendar=function()",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setAttribute",
  "father" : "",
  "desc" : "setAttribute",
  "name" : "ReportDatePicker.prototype.setAttribute=function(name,value)",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getAttribute",
  "father" : "",
  "desc" : "getAttribute",
  "name" : "ReportDatePicker.prototype.getAttribute=function(name)",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setValue",
  "father" : "",
  "desc" : "setValue",
  "name" : "ReportDatePicker.prototype.setValue=function(value)",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "ReportDatePicker.prototype.getValue=function()",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setReadOnly",
  "father" : "",
  "desc" : "setReadOnly",
  "name" : "ReportDatePicker.prototype.setReadOnly=function(readonly)",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getReadOnly",
  "father" : "",
  "desc" : "getReadOnly",
  "name" : "ReportDatePicker.prototype.getReadOnly=function()",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setEnabled",
  "father" : "",
  "desc" : "setEnabled",
  "name" : "ReportDatePicker.prototype.setEnabled=function(enable)",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedId",
  "father" : "",
  "desc" : "getSelectedId",
  "name" : "ReportDatePicker.prototype.getSelectedId=function()",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedText",
  "father" : "",
  "desc" : "getSelectedText",
  "name" : "ReportDatePicker.prototype.getSelectedText=function()",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clear",
  "father" : "",
  "desc" : "clear",
  "name" : "ReportDatePicker.prototype.clear=function()",
  "version" : "0",
  "path" : "freequery.control.ReportDatePicker",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]