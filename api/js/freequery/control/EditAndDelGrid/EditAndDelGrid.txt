[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "EditAndDelGrid.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "newDelHeader",
  "father" : "",
  "desc" : "newDelHeader",
  "name" : "EditAndDelGrid.prototype.newDelHeader=function()",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fillData",
  "father" : "",
  "desc" : "fillData",
  "name" : "EditAndDelGrid.prototype.fillData=function(data)",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "appendLastRows",
  "father" : "",
  "desc" : "appendLastRows",
  "name" : "EditAndDelGrid.prototype.appendLastRows=function(rows)",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showEditBox",
  "father" : "",
  "desc" : "showEditBox",
  "name" : "EditAndDelGrid.prototype.showEditBox=function(show,boxObj,top,left,width,height,text,cell)",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setDelToCell",
  "father" : "",
  "desc" : "setDelToCell",
  "name" : "EditAndDelGrid.prototype.setDelToCell=function(row,col)",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "delRow",
  "father" : "",
  "desc" : "delRow",
  "name" : "EditAndDelGrid.prototype.delRow=function(ev)",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getGridData",
  "father" : "",
  "desc" : "getGridData",
  "name" : "EditAndDelGrid.prototype.getGridData=function()",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clear",
  "father" : "",
  "desc" : "clear",
  "name" : "EditAndDelGrid.prototype.clear=function(clearHeader)",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doEditKeyDown",
  "father" : "",
  "desc" : "doEditKeyDown",
  "name" : "EditAndDelGrid.prototype.doEditKeyDown=function(ev)",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doEditKeyup",
  "father" : "",
  "desc" : "doEditKeyup",
  "name" : "EditAndDelGrid.prototype.doEditKeyup=function()",
  "version" : "0",
  "path" : "freequery.control.EditAndDelGrid",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]