[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "AutoCompleteBox.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "dropDown",
  "father" : "",
  "desc" : "dropDown",
  "name" : "AutoCompleteBox.prototype.dropDown=function(x,y,width,height)",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "itemsCount",
  "father" : "",
  "desc" : "itemsCount",
  "name" : "AutoCompleteBox.prototype.itemsCount=function()",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createItemHTML",
  "father" : "",
  "desc" : "createItemHTML",
  "name" : "AutoCompleteBox.prototype.createItemHTML=function(id,caption,data,imgs)",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertItem",
  "father" : "",
  "desc" : "insertItem",
  "name" : "AutoCompleteBox.prototype.insertItem=function(id,caption,data,imgs)",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertItems",
  "father" : "",
  "desc" : "insertItems",
  "name" : "AutoCompleteBox.prototype.insertItems=function(data)",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeItem",
  "father" : "",
  "desc" : "removeItem",
  "name" : "AutoCompleteBox.prototype.removeItem=function(id)",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshItems",
  "father" : "",
  "desc" : "refreshItems",
  "name" : "AutoCompleteBox.prototype.refreshItems=function()",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doMouseOver",
  "father" : "",
  "desc" : "doMouseOver",
  "name" : "AutoCompleteBox.prototype.doMouseOver=function(item)",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resetItemClass",
  "father" : "",
  "desc" : "resetItemClass",
  "name" : "AutoCompleteBox.prototype.resetItemClass=function(item)",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "selectNextItem",
  "father" : "",
  "desc" : "selectNextItem",
  "name" : "AutoCompleteBox.prototype.selectNextItem=function(isUp)",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doItemClick",
  "father" : "",
  "desc" : "doItemClick",
  "name" : "AutoCompleteBox.prototype.doItemClick=function(e)",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedId",
  "father" : "",
  "desc" : "getSelectedId",
  "name" : "AutoCompleteBox.prototype.getSelectedId=function()",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedValue",
  "father" : "",
  "desc" : "getSelectedValue",
  "name" : "AutoCompleteBox.prototype.getSelectedValue=function()",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clear",
  "father" : "",
  "desc" : "clear",
  "name" : "AutoCompleteBox.prototype.clear=function()",
  "version" : "0",
  "path" : "freequery.control.AutoCompleteBox",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]