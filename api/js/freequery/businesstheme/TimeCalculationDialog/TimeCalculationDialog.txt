[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "TimeCalculationDialog.prototype.init=function(parent,param,fn,obj)",
  "version" : "0",
  "path" : "freequery.businesstheme.TimeCalculationDialog",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onFunctionChange",
  "father" : "",
  "desc" : "onFunctionChange",
  "name" : "TimeCalculationDialog.prototype.onFunctionChange=function(e)",
  "version" : "0",
  "path" : "freequery.businesstheme.TimeCalculationDialog",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onLevelChange",
  "father" : "",
  "desc" : "onLevelChange",
  "name" : "TimeCalculationDialog.prototype.onLevelChange=function(e)",
  "version" : "0",
  "path" : "freequery.businesstheme.TimeCalculationDialog",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "TimeCalculationDialog.prototype.doOK=function(e)",
  "version" : "0",
  "path" : "freequery.businesstheme.TimeCalculationDialog",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_showTimeLevel",
  "father" : "",
  "desc" : "_showTimeLevel",
  "name" : "TimeCalculationDialog.prototype._showTimeLevel=function(val)",
  "version" : "0",
  "path" : "freequery.businesstheme.TimeCalculationDialog",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "_showFuncRelateTimeLevel",
  "father" : "",
  "desc" : "_showFuncRelateTimeLevel",
  "name" : "TimeCalculationDialog.prototype._showFuncRelateTimeLevel=function(funcName)",
  "version" : "0",
  "path" : "freequery.businesstheme.TimeCalculationDialog",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]