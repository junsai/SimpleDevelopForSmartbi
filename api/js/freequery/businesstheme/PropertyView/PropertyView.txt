[ {
  "type" : 0,
  "keyword" : "doCmd",
  "father" : "",
  "desc" : "doCmd",
  "name" : "PropertyView.prototype.doCmd=function(params)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "bindData",
  "father" : "",
  "desc" : "bindData",
  "name" : "PropertyView.prototype.bindData=function(params)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBtnOK_click_handler",
  "father" : "",
  "desc" : "elemBtnOK_click_handler",
  "name" : "PropertyView.prototype.elemBtnOK_click_handler=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBtnCancle_click_handler",
  "father" : "",
  "desc" : "elemBtnCancle_click_handler",
  "name" : "PropertyView.prototype.elemBtnCancle_click_handler=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemAssociationPropertyBtn_click_handler",
  "father" : "",
  "desc" : "elemAssociationPropertyBtn_click_handler",
  "name" : "PropertyView.prototype.elemAssociationPropertyBtn_click_handler=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemAssociationPropertyClearBtn_click_handler",
  "father" : "",
  "desc" : "elemAssociationPropertyClearBtn_click_handler",
  "name" : "PropertyView.prototype.elemAssociationPropertyClearBtn_click_handler=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOptionEnforce_click_handler",
  "father" : "",
  "desc" : "elemOptionEnforce_click_handler",
  "name" : "PropertyView.prototype.elemOptionEnforce_click_handler=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOptionHidden_click_handler",
  "father" : "",
  "desc" : "elemOptionHidden_click_handler",
  "name" : "PropertyView.prototype.elemOptionHidden_click_handler=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "selectAssociationProperty",
  "father" : "",
  "desc" : "selectAssociationProperty",
  "name" : "PropertyView.prototype.selectAssociationProperty=function(node)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemTimeCalcBtn_click_handler",
  "father" : "",
  "desc" : "elemTimeCalcBtn_click_handler",
  "name" : "PropertyView.prototype.elemTimeCalcBtn_click_handler=function(e)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemTimeCalcClearBtn_click_handler",
  "father" : "",
  "desc" : "elemTimeCalcClearBtn_click_handler",
  "name" : "PropertyView.prototype.elemTimeCalcClearBtn_click_handler=function(e)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "selectTimeCalc",
  "father" : "",
  "desc" : "selectTimeCalc",
  "name" : "PropertyView.prototype.selectTimeCalc=function(result)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemAsInsightCaclFieldTrue_click_handler",
  "father" : "",
  "desc" : "elemAsInsightCaclFieldTrue_click_handler",
  "name" : "PropertyView.prototype.elemAsInsightCaclFieldTrue_click_handler=function(e)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "propDatatype_change_handler",
  "father" : "",
  "desc" : "propDatatype_change_handler",
  "name" : "PropertyView.prototype.propDatatype_change_handler=function(box,oldId,id)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resetFormat",
  "father" : "",
  "desc" : "resetFormat",
  "name" : "PropertyView.prototype.resetFormat=function(type)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setEnabled",
  "father" : "",
  "desc" : "setEnabled",
  "name" : "PropertyView.prototype.setEnabled=function(value)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clear",
  "father" : "",
  "desc" : "clear",
  "name" : "PropertyView.prototype.clear=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "PropertyView.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onExpChanged",
  "father" : "",
  "desc" : "onExpChanged",
  "name" : "PropertyView.prototype.onExpChanged=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onExpFocusOut",
  "father" : "",
  "desc" : "onExpFocusOut",
  "name" : "PropertyView.prototype.onExpFocusOut=function(editor)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "optionCoditionChanged",
  "father" : "",
  "desc" : "optionCoditionChanged",
  "name" : "PropertyView.prototype.optionCoditionChanged=function()",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initTransformRule",
  "father" : "",
  "desc" : "initTransformRule",
  "name" : "PropertyView.prototype.initTransformRule=function(params)",
  "version" : "0",
  "path" : "freequery.businesstheme.PropertyView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]