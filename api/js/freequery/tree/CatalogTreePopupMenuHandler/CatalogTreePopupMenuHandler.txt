[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "CatalogTreePopupMenuHandler.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initMenu",
  "father" : "",
  "desc" : "initMenu",
  "name" : "CatalogTreePopupMenuHandler.prototype.initMenu=function()",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clearMenuState",
  "father" : "",
  "desc" : "clearMenuState",
  "name" : "CatalogTreePopupMenuHandler.prototype.clearMenuState=function()",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "canMove",
  "father" : "",
  "desc" : "canMove",
  "name" : "CatalogTreePopupMenuHandler.prototype.canMove=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resetMenuState",
  "father" : "",
  "desc" : "resetMenuState",
  "name" : "CatalogTreePopupMenuHandler.prototype.resetMenuState=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCmd",
  "father" : "",
  "desc" : "doCmd",
  "name" : "CatalogTreePopupMenuHandler.prototype.doCmd=function(node,func)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAfterMoveResource",
  "father" : "",
  "desc" : "doAfterMoveResource",
  "name" : "CatalogTreePopupMenuHandler.prototype.doAfterMoveResource=function(succeeded,srcNode,selectedNode)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRefreshAfterMoveRes",
  "father" : "",
  "desc" : "doRefreshAfterMoveRes",
  "name" : "CatalogTreePopupMenuHandler.prototype.doRefreshAfterMoveRes=function(srcNode,selectedNode)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAfterDuplicateResource",
  "father" : "",
  "desc" : "doAfterDuplicateResource",
  "name" : "CatalogTreePopupMenuHandler.prototype.doAfterDuplicateResource=function(toNode,srcId,name,alias,desc)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "newURL",
  "father" : "",
  "desc" : "newURL",
  "name" : "CatalogTreePopupMenuHandler.prototype.newURL=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "newFileResource",
  "father" : "",
  "desc" : "newFileResource",
  "name" : "CatalogTreePopupMenuHandler.prototype.newFileResource=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeOrder",
  "father" : "",
  "desc" : "changeOrder",
  "name" : "CatalogTreePopupMenuHandler.prototype.changeOrder=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createFolder",
  "father" : "",
  "desc" : "createFolder",
  "name" : "CatalogTreePopupMenuHandler.prototype.createFolder=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAfterNodeChange",
  "father" : "",
  "desc" : "doAfterNodeChange",
  "name" : "CatalogTreePopupMenuHandler.prototype.doAfterNodeChange=function(node,ok)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAfterNewNode",
  "father" : "",
  "desc" : "doAfterNewNode",
  "name" : "CatalogTreePopupMenuHandler.prototype.doAfterNewNode=function(node,ok)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshNode",
  "father" : "",
  "desc" : "refreshNode",
  "name" : "CatalogTreePopupMenuHandler.prototype.refreshNode=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeAlias",
  "father" : "",
  "desc" : "changeAlias",
  "name" : "CatalogTreePopupMenuHandler.prototype.changeAlias=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeNodeAlias",
  "father" : "",
  "desc" : "changeNodeAlias",
  "name" : "CatalogTreePopupMenuHandler.prototype.changeNodeAlias=function(succeed,ret)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "generateBusinessParam",
  "father" : "",
  "desc" : "generateBusinessParam",
  "name" : "CatalogTreePopupMenuHandler.prototype.generateBusinessParam=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showProperty",
  "father" : "",
  "desc" : "showProperty",
  "name" : "CatalogTreePopupMenuHandler.prototype.showProperty=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "shareReport",
  "father" : "",
  "desc" : "shareReport",
  "name" : "CatalogTreePopupMenuHandler.prototype.shareReport=function(node)",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getShareType",
  "father" : "",
  "desc" : "getShareType",
  "name" : "CatalogTreePopupMenuHandler.prototype.getShareType=function()",
  "version" : "0",
  "path" : "freequery.tree.CatalogTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]