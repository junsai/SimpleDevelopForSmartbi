[ {
  "type" : 0,
  "keyword" : "createTreeNode",
  "father" : "",
  "desc" : "createTreeNode",
  "name" : "SuperviseTree.prototype.createTreeNode=function(text,level,mayHasChild,parent,id)",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getMenuHandlers",
  "father" : "",
  "desc" : "getMenuHandlers",
  "name" : "SuperviseTree.prototype.getMenuHandlers=function()",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getCommandFactory",
  "father" : "",
  "desc" : "getCommandFactory",
  "name" : "SuperviseTree.prototype.getCommandFactory=function()",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doDeleteCallback",
  "father" : "",
  "desc" : "doDeleteCallback",
  "name" : "SuperviseTree.prototype.doDeleteCallback=function(flag,node)",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "selectNodeWithoutFire",
  "father" : "",
  "desc" : "selectNodeWithoutFire",
  "name" : "SuperviseTree.prototype.selectNodeWithoutFire=function(node,notFocus)",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resetNodeStyle",
  "father" : "",
  "desc" : "resetNodeStyle",
  "name" : "SuperviseTree.prototype.resetNodeStyle=function(node)",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "selectNodeEx",
  "father" : "",
  "desc" : "selectNodeEx",
  "name" : "SuperviseTree.prototype.selectNodeEx=function(node,notFocus)",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showRootNodeMark",
  "father" : "",
  "desc" : "showRootNodeMark",
  "name" : "SuperviseTree.prototype.showRootNodeMark=function(node,isShow)",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeNodeStyle",
  "father" : "",
  "desc" : "changeNodeStyle",
  "name" : "SuperviseTree.prototype.changeNodeStyle=function(node,isEmphasis)",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshNode",
  "father" : "",
  "desc" : "refreshNode",
  "name" : "SuperviseTree.prototype.refreshNode=function(id)",
  "version" : "0",
  "path" : "freequery.tree.SuperviseTree",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]