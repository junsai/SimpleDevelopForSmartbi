[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "QueryDesignerTreePopupMenuHandler.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.tree.QueryDesignerTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clearMenuState",
  "father" : "",
  "desc" : "clearMenuState",
  "name" : "QueryDesignerTreePopupMenuHandler.prototype.clearMenuState=function()",
  "version" : "0",
  "path" : "freequery.tree.QueryDesignerTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initMenu",
  "father" : "",
  "desc" : "initMenu",
  "name" : "QueryDesignerTreePopupMenuHandler.prototype.initMenu=function()",
  "version" : "0",
  "path" : "freequery.tree.QueryDesignerTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setEditable",
  "father" : "",
  "desc" : "setEditable",
  "name" : "QueryDesignerTreePopupMenuHandler.prototype.setEditable=function(node)",
  "version" : "0",
  "path" : "freequery.tree.QueryDesignerTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resetMenuState",
  "father" : "",
  "desc" : "resetMenuState",
  "name" : "QueryDesignerTreePopupMenuHandler.prototype.resetMenuState=function(node)",
  "version" : "0",
  "path" : "freequery.tree.QueryDesignerTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCmd",
  "father" : "",
  "desc" : "doCmd",
  "name" : "QueryDesignerTreePopupMenuHandler.prototype.doCmd=function(node,func)",
  "version" : "0",
  "path" : "freequery.tree.QueryDesignerTreePopupMenuHandler",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]