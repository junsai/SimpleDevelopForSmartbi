[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "DataSourceDialog.prototype.init=function(parent,data,fn,obj)",
  "version" : "0",
  "path" : "freequery.dialog.DataSourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addFilterTypes",
  "father" : "",
  "desc" : "addFilterTypes",
  "name" : "DataSourceDialog.prototype.addFilterTypes=function(isRawQuery)",
  "version" : "0",
  "path" : "freequery.dialog.DataSourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onClick",
  "father" : "",
  "desc" : "onClick",
  "name" : "DataSourceDialog.prototype.onClick=function(ev,node)",
  "version" : "0",
  "path" : "freequery.dialog.DataSourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onClickBusinessTheme",
  "father" : "",
  "desc" : "onClickBusinessTheme",
  "name" : "DataSourceDialog.prototype.onClickBusinessTheme=function(ev)",
  "version" : "0",
  "path" : "freequery.dialog.DataSourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnSelectNode",
  "father" : "",
  "desc" : "doOnSelectNode",
  "name" : "DataSourceDialog.prototype.doOnSelectNode=function(ev,node)",
  "version" : "0",
  "path" : "freequery.dialog.DataSourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "DataSourceDialog.prototype.doOK=function(ev)",
  "version" : "0",
  "path" : "freequery.dialog.DataSourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "DataSourceDialog.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.dialog.DataSourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]