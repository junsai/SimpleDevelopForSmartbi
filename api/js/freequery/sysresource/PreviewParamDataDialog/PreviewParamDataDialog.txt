[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "PreviewParamDataDialog.prototype.init=function(parent,params,fn,obj)",
  "version" : "0",
  "path" : "freequery.sysresource.PreviewParamDataDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshData",
  "father" : "",
  "desc" : "refreshData",
  "name" : "PreviewParamDataDialog.prototype.refreshData=function(queryBody,bvClientId)",
  "version" : "0",
  "path" : "freequery.sysresource.PreviewParamDataDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getBvId",
  "father" : "",
  "desc" : "getBvId",
  "name" : "PreviewParamDataDialog.prototype.getBvId=function(bvClientId)",
  "version" : "0",
  "path" : "freequery.sysresource.PreviewParamDataDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleObjectQuery",
  "father" : "",
  "desc" : "handleObjectQuery",
  "name" : "PreviewParamDataDialog.prototype.handleObjectQuery=function(bvClientId,queryId)",
  "version" : "0",
  "path" : "freequery.sysresource.PreviewParamDataDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleObjectQueryInfo",
  "father" : "",
  "desc" : "handleObjectQueryInfo",
  "name" : "PreviewParamDataDialog.prototype.handleObjectQueryInfo=function(bvClientId,queryId)",
  "version" : "0",
  "path" : "freequery.sysresource.PreviewParamDataDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSqlInfo",
  "father" : "",
  "desc" : "getSqlInfo",
  "name" : "PreviewParamDataDialog.prototype.getSqlInfo=function()",
  "version" : "0",
  "path" : "freequery.sysresource.PreviewParamDataDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleObjectFieldPart",
  "father" : "",
  "desc" : "handleObjectFieldPart",
  "name" : "PreviewParamDataDialog.prototype.handleObjectFieldPart=function(bvClientId,queryId,fieldText)",
  "version" : "0",
  "path" : "freequery.sysresource.PreviewParamDataDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleObjectWherePart",
  "father" : "",
  "desc" : "handleObjectWherePart",
  "name" : "PreviewParamDataDialog.prototype.handleObjectWherePart=function(bvClientId,queryId,whereText)",
  "version" : "0",
  "path" : "freequery.sysresource.PreviewParamDataDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "PreviewParamDataDialog.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.sysresource.PreviewParamDataDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]