[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "ImportChooserView.prototype.init=function()",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "ImportChooserView.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "close",
  "father" : "",
  "desc" : "close",
  "name" : "ImportChooserView.prototype.close=function(obj,done)",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "closeButtonClick",
  "father" : "",
  "desc" : "closeButtonClick",
  "name" : "ImportChooserView.prototype.closeButtonClick=function(e)",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "saveButtonClick",
  "father" : "",
  "desc" : "saveButtonClick",
  "name" : "ImportChooserView.prototype.saveButtonClick=function(e)",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSaveButtonEnabled",
  "father" : "",
  "desc" : "setSaveButtonEnabled",
  "name" : "ImportChooserView.prototype.setSaveButtonEnabled=function(enabled)",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSaveButtonEnabled",
  "father" : "",
  "desc" : "getSaveButtonEnabled",
  "name" : "ImportChooserView.prototype.getSaveButtonEnabled=function()",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshSaveButtonState",
  "father" : "",
  "desc" : "refreshSaveButtonState",
  "name" : "ImportChooserView.prototype.refreshSaveButtonState=function()",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resourcesSort",
  "father" : "",
  "desc" : "resourcesSort",
  "name" : "ImportChooserView.prototype.resourcesSort=function(items)",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "selectResources",
  "father" : "",
  "desc" : "selectResources",
  "name" : "ImportChooserView.prototype.selectResources=function(items)",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showConfilctDetail",
  "father" : "",
  "desc" : "showConfilctDetail",
  "name" : "ImportChooserView.prototype.showConfilctDetail=function()",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCheckboxClick",
  "father" : "",
  "desc" : "doCheckboxClick",
  "name" : "ImportChooserView.prototype.doCheckboxClick=function()",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAliasItemClick",
  "father" : "",
  "desc" : "doAliasItemClick",
  "name" : "ImportChooserView.prototype.doAliasItemClick=function()",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSetAliasOK",
  "father" : "",
  "desc" : "doSetAliasOK",
  "name" : "ImportChooserView.prototype.doSetAliasOK=function(succeeded,newAlias,remark)",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doPathItemClick",
  "father" : "",
  "desc" : "doPathItemClick",
  "name" : "ImportChooserView.prototype.doPathItemClick=function()",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doFolderChooseOK",
  "father" : "",
  "desc" : "doFolderChooseOK",
  "name" : "ImportChooserView.prototype.doFolderChooseOK=function(succeeded,selectedPath,selectedPathAlias,remark)",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doImportOK",
  "father" : "",
  "desc" : "doImportOK",
  "name" : "ImportChooserView.prototype.doImportOK=function()",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "callbackHandler",
  "father" : "",
  "desc" : "callbackHandler",
  "name" : "ImportChooserView.prototype.callbackHandler=function(msg,title)",
  "version" : "0",
  "path" : "freequery.toolkit.tkview.ImportChooserView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]