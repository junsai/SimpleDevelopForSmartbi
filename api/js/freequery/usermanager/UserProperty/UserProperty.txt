[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "UserProperty.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setReadOnly",
  "father" : "",
  "desc" : "setReadOnly",
  "name" : "UserProperty.prototype.setReadOnly=function(flag)",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "show",
  "father" : "",
  "desc" : "show",
  "name" : "UserProperty.prototype.show=function(type,pFolderId,propertyId,dsId,createType)",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hide",
  "father" : "",
  "desc" : "hide",
  "name" : "UserProperty.prototype.hide=function()",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onBtnOK",
  "father" : "",
  "desc" : "onBtnOK",
  "name" : "UserProperty.prototype.onBtnOK=function()",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "saveAsCallback",
  "father" : "",
  "desc" : "saveAsCallback",
  "name" : "UserProperty.prototype.saveAsCallback=function(result)",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isDirty",
  "father" : "",
  "desc" : "isDirty",
  "name" : "UserProperty.prototype.isDirty=function()",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "editChange",
  "father" : "",
  "desc" : "editChange",
  "name" : "UserProperty.prototype.editChange=function()",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "confirmClose",
  "father" : "",
  "desc" : "confirmClose",
  "name" : "UserProperty.prototype.confirmClose=function(callback)",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onBtnClose",
  "father" : "",
  "desc" : "onBtnClose",
  "name" : "UserProperty.prototype.onBtnClose=function(e)",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clearDsInfo",
  "father" : "",
  "desc" : "clearDsInfo",
  "name" : "UserProperty.prototype.clearDsInfo=function()",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initDataType",
  "father" : "",
  "desc" : "initDataType",
  "name" : "UserProperty.prototype.initDataType=function()",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onPreviewPropertyValueButtonClick",
  "father" : "",
  "desc" : "onPreviewPropertyValueButtonClick",
  "name" : "UserProperty.prototype.onPreviewPropertyValueButtonClick=function()",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "previewData",
  "father" : "",
  "desc" : "previewData",
  "name" : "UserProperty.prototype.previewData=function(dataSourceId,sql)",
  "version" : "0",
  "path" : "freequery.usermanager.UserProperty",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]