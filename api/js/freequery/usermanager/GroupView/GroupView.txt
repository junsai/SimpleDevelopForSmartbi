[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "GroupView.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnShow",
  "father" : "",
  "desc" : "doOnShow",
  "name" : "GroupView.prototype.doOnShow=function()",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "render",
  "father" : "",
  "desc" : "render",
  "name" : "GroupView.prototype.render=function(container,viewModel,parGroupId,curGroupId)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "unrender",
  "father" : "",
  "desc" : "unrender",
  "name" : "GroupView.prototype.unrender=function()",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initViewModel",
  "father" : "",
  "desc" : "initViewModel",
  "name" : "GroupView.prototype.initViewModel=function()",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initRoleList",
  "father" : "",
  "desc" : "initRoleList",
  "name" : "GroupView.prototype.initRoleList=function(parentDiv)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clear",
  "father" : "",
  "desc" : "clear",
  "name" : "GroupView.prototype.clear=function()",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "CheckGroupRole",
  "father" : "",
  "desc" : "CheckGroupRole",
  "name" : "GroupView.prototype.CheckGroupRole=function(groupId)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "disabledInheritedRole",
  "father" : "",
  "desc" : "disabledInheritedRole",
  "name" : "GroupView.prototype.disabledInheritedRole=function(groupId)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreate",
  "father" : "",
  "desc" : "doCreate",
  "name" : "GroupView.prototype.doCreate=function(e)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "GroupView.prototype.doClose=function(e)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setGroupInView",
  "father" : "",
  "desc" : "setGroupInView",
  "name" : "GroupView.prototype.setGroupInView=function(id,name,alias,desc,orgId)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createGroup",
  "father" : "",
  "desc" : "createGroup",
  "name" : "GroupView.prototype.createGroup=function()",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createGroupCallback",
  "father" : "",
  "desc" : "createGroupCallback",
  "name" : "GroupView.prototype.createGroupCallback=function(ret)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getGroup",
  "father" : "",
  "desc" : "getGroup",
  "name" : "GroupView.prototype.getGroup=function(groupId)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addRolesToGroup",
  "father" : "",
  "desc" : "addRolesToGroup",
  "name" : "GroupView.prototype.addRolesToGroup=function(groupId)",
  "version" : "0",
  "path" : "freequery.usermanager.GroupView",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]