[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "FileResourceDialog.prototype.init=function(parent,param,fn,obj)",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnFileNameChange",
  "father" : "",
  "desc" : "doOnFileNameChange",
  "name" : "FileResourceDialog.prototype.doOnFileNameChange=function(e)",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "FileResourceDialog.prototype.doClose=function(e)",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "FileResourceDialog.prototype.doOK=function(e)",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreateBefore1",
  "father" : "",
  "desc" : "doCreateBefore1",
  "name" : "FileResourceDialog.prototype.doCreateBefore1=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreateBefore2",
  "father" : "",
  "desc" : "doCreateBefore2",
  "name" : "FileResourceDialog.prototype.doCreateBefore2=function(info,force)",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreate",
  "father" : "",
  "desc" : "doCreate",
  "name" : "FileResourceDialog.prototype.doCreate=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "callbackHandler",
  "father" : "",
  "desc" : "callbackHandler",
  "name" : "FileResourceDialog.prototype.callbackHandler=function(msg,title)",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doUpdate",
  "father" : "",
  "desc" : "doUpdate",
  "name" : "FileResourceDialog.prototype.doUpdate=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doExcute",
  "father" : "",
  "desc" : "doExcute",
  "name" : "FileResourceDialog.prototype.doExcute=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.FileResourceDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]