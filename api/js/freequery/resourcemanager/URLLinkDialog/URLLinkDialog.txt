[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "URLLinkDialog.prototype.init=function(parent,param,fn,obj)",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setValue",
  "father" : "",
  "desc" : "setValue",
  "name" : "URLLinkDialog.prototype.setValue=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "URLLinkDialog.prototype.doClose=function(e)",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "URLLinkDialog.prototype.doOK=function(e)",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreateBefore1",
  "father" : "",
  "desc" : "doCreateBefore1",
  "name" : "URLLinkDialog.prototype.doCreateBefore1=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreateBefore2",
  "father" : "",
  "desc" : "doCreateBefore2",
  "name" : "URLLinkDialog.prototype.doCreateBefore2=function(info,force)",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSetting",
  "father" : "",
  "desc" : "getSetting",
  "name" : "URLLinkDialog.prototype.getSetting=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreate",
  "father" : "",
  "desc" : "doCreate",
  "name" : "URLLinkDialog.prototype.doCreate=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doUpdate",
  "father" : "",
  "desc" : "doUpdate",
  "name" : "URLLinkDialog.prototype.doUpdate=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSelect",
  "father" : "",
  "desc" : "doSelect",
  "name" : "URLLinkDialog.prototype.doSelect=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createURLLink",
  "father" : "",
  "desc" : "createURLLink",
  "name" : "URLLinkDialog.prototype.createURLLink=function(nodeId,type)",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAutoLoginClick",
  "father" : "",
  "desc" : "doAutoLoginClick",
  "name" : "URLLinkDialog.prototype.doAutoLoginClick=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.URLLinkDialog",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]