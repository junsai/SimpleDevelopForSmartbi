[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "TreeWrapper.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.TreeWrapper",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setTimeoutUpdateScrollator",
  "father" : "",
  "desc" : "setTimeoutUpdateScrollator",
  "name" : "TreeWrapper.prototype.setTimeoutUpdateScrollator=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.TreeWrapper",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateScrollator",
  "father" : "",
  "desc" : "updateScrollator",
  "name" : "TreeWrapper.prototype.updateScrollator=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.TreeWrapper",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateScrollatorAndScrollTop",
  "father" : "",
  "desc" : "updateScrollatorAndScrollTop",
  "name" : "TreeWrapper.prototype.updateScrollatorAndScrollTop=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.TreeWrapper",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showTree",
  "father" : "",
  "desc" : "showTree",
  "name" : "TreeWrapper.prototype.showTree=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.TreeWrapper",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideTree",
  "father" : "",
  "desc" : "hideTree",
  "name" : "TreeWrapper.prototype.hideTree=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.TreeWrapper",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showSearchbar",
  "father" : "",
  "desc" : "showSearchbar",
  "name" : "TreeWrapper.prototype.showSearchbar=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.TreeWrapper",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideSearchbar",
  "father" : "",
  "desc" : "hideSearchbar",
  "name" : "TreeWrapper.prototype.hideSearchbar=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.TreeWrapper",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroyTree",
  "father" : "",
  "desc" : "destroyTree",
  "name" : "TreeWrapper.prototype.destroyTree=function()",
  "version" : "0",
  "path" : "freequery.resourcemanager.TreeWrapper",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]