[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "TempTable.prototype.destroy=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "render",
  "father" : "",
  "desc" : "render",
  "name" : "TempTable.prototype.render=function(type,node,tempTableId)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "TempTable.prototype.init=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initSelectUnionDataSource",
  "father" : "",
  "desc" : "initSelectUnionDataSource",
  "name" : "TempTable.prototype.initSelectUnionDataSource=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initSelectSchema",
  "father" : "",
  "desc" : "initSelectSchema",
  "name" : "TempTable.prototype.initSelectSchema=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "unRender",
  "father" : "",
  "desc" : "unRender",
  "name" : "TempTable.prototype.unRender=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fillHeader",
  "father" : "",
  "desc" : "fillHeader",
  "name" : "TempTable.prototype.fillHeader=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doUnionDataSourceChange",
  "father" : "",
  "desc" : "doUnionDataSourceChange",
  "name" : "TempTable.prototype.doUnionDataSourceChange=function(combo,var1,var2,var3,var4)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "loadschemas",
  "father" : "",
  "desc" : "loadschemas",
  "name" : "TempTable.prototype.loadschemas=function(dsId)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSQLKeydown",
  "father" : "",
  "desc" : "doSQLKeydown",
  "name" : "TempTable.prototype.doSQLKeydown=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "chooseResource",
  "father" : "",
  "desc" : "chooseResource",
  "name" : "TempTable.prototype.chooseResource=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "chooseResourceAfter",
  "father" : "",
  "desc" : "chooseResourceAfter",
  "name" : "TempTable.prototype.chooseResourceAfter=function(node)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onResourceChange",
  "father" : "",
  "desc" : "onResourceChange",
  "name" : "TempTable.prototype.onResourceChange=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fillFields",
  "father" : "",
  "desc" : "fillFields",
  "name" : "TempTable.prototype.fillFields=function(fields)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSchema",
  "father" : "",
  "desc" : "getSchema",
  "name" : "TempTable.prototype.getSchema=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "generateSQL",
  "father" : "",
  "desc" : "generateSQL",
  "name" : "TempTable.prototype.generateSQL=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "includeChinese",
  "father" : "",
  "desc" : "includeChinese",
  "name" : "TempTable.prototype.includeChinese=function(tableName)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkTableNameValid",
  "father" : "",
  "desc" : "checkTableNameValid",
  "name" : "TempTable.prototype.checkTableNameValid=function(tableName,schemaName,sqlText)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkValid",
  "father" : "",
  "desc" : "checkValid",
  "name" : "TempTable.prototype.checkValid=function(nameStr)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getFields",
  "father" : "",
  "desc" : "getFields",
  "name" : "TempTable.prototype.getFields=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "TempTable.prototype.doClose=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "TempTable.prototype.doOK=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setReadOnly",
  "father" : "",
  "desc" : "setReadOnly",
  "name" : "TempTable.prototype.setReadOnly=function(flag)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initPlanInfo",
  "father" : "",
  "desc" : "initPlanInfo",
  "name" : "TempTable.prototype.initPlanInfo=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "generateSchedule",
  "father" : "",
  "desc" : "generateSchedule",
  "name" : "TempTable.prototype.generateSchedule=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "callBackCreateSchedule",
  "father" : "",
  "desc" : "callBackCreateSchedule",
  "name" : "TempTable.prototype.callBackCreateSchedule=function(ret)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createTask",
  "father" : "",
  "desc" : "createTask",
  "name" : "TempTable.prototype.createTask=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createPlan",
  "father" : "",
  "desc" : "createPlan",
  "name" : "TempTable.prototype.createPlan=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doExtractData",
  "father" : "",
  "desc" : "doExtractData",
  "name" : "TempTable.prototype.doExtractData=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "extractDataCallback",
  "father" : "",
  "desc" : "extractDataCallback",
  "name" : "TempTable.prototype.extractDataCallback=function(ret)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doTask",
  "father" : "",
  "desc" : "doTask",
  "name" : "TempTable.prototype.doTask=function()",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "afterChoosePlanDialog",
  "father" : "",
  "desc" : "afterChoosePlanDialog",
  "name" : "TempTable.prototype.afterChoosePlanDialog=function(planNode)",
  "version" : "0",
  "path" : "freequery.uniondata.TempTable",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]