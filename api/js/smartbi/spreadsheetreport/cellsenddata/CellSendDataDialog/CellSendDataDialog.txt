[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "CellSendDataDialog.prototype.init=function(parent)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initSelectActionCell",
  "father" : "",
  "desc" : "initSelectActionCell",
  "name" : "CellSendDataDialog.prototype.initSelectActionCell=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAddSetting",
  "father" : "",
  "desc" : "doAddSetting",
  "name" : "CellSendDataDialog.prototype.doAddSetting=function(ev)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createChooseArea",
  "father" : "",
  "desc" : "createChooseArea",
  "name" : "CellSendDataDialog.prototype.createChooseArea=function(inputBoxTd,actionscope,width)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createDeleteBtn",
  "father" : "",
  "desc" : "createDeleteBtn",
  "name" : "CellSendDataDialog.prototype.createDeleteBtn=function(deleteBoxTd)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doComboValueChange",
  "father" : "",
  "desc" : "doComboValueChange",
  "name" : "CellSendDataDialog.prototype.doComboValueChange=function(sender,selectedId,dropdownBoxSelectedId,selectedValue,\t\tdropdownBoxSelectedValue) ",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "delRow",
  "father" : "",
  "desc" : "delRow",
  "name" : "CellSendDataDialog.prototype.delRow=function(ev)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doChooseArea",
  "father" : "",
  "desc" : "doChooseArea",
  "name" : "CellSendDataDialog.prototype.doChooseArea=function(ev)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkValidate",
  "father" : "",
  "desc" : "checkValidate",
  "name" : "CellSendDataDialog.prototype.checkValidate=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "CellSendDataDialog.prototype.getValue=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setValue",
  "father" : "",
  "desc" : "setValue",
  "name" : "CellSendDataDialog.prototype.setValue=function(data)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "disalbedAddBtn",
  "father" : "",
  "desc" : "disalbedAddBtn",
  "name" : "CellSendDataDialog.prototype.disalbedAddBtn=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "enabledAddBtn",
  "father" : "",
  "desc" : "enabledAddBtn",
  "name" : "CellSendDataDialog.prototype.enabledAddBtn=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "CellSendDataDialog.prototype.destroy=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clear",
  "father" : "",
  "desc" : "clear",
  "name" : "CellSendDataDialog.prototype.clear=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setCellPosition",
  "father" : "",
  "desc" : "setCellPosition",
  "name" : "CellSendDataDialog.prototype.setCellPosition=function(cell)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSize",
  "father" : "",
  "desc" : "getSize",
  "name" : "CellSendDataDialog.prototype.getSize=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.cellsenddata.CellSendDataDialog",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]