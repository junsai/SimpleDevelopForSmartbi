[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "SpreadsheetReportRowsPerPage.prototype.init=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportRowsPerPage",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "SpreadsheetReportRowsPerPage.prototype.validate=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportRowsPerPage",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "SpreadsheetReportRowsPerPage.prototype.save=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportRowsPerPage",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doInitRowsPerPage",
  "father" : "",
  "desc" : "doInitRowsPerPage",
  "name" : "SpreadsheetReportRowsPerPage.prototype.doInitRowsPerPage=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportRowsPerPage",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "SpreadsheetReportRowsPerPage.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportRowsPerPage",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]