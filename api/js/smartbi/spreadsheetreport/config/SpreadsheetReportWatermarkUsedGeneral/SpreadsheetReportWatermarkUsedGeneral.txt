[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "SpreadsheetReportWatermarkUsedGeneral.prototype.init=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkUsedGeneral",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "SpreadsheetReportWatermarkUsedGeneral.prototype.validate=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkUsedGeneral",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "SpreadsheetReportWatermarkUsedGeneral.prototype.save=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkUsedGeneral",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doInitSpreadsheetReportWatermarkUsedGeneral",
  "father" : "",
  "desc" : "doInitSpreadsheetReportWatermarkUsedGeneral",
  "name" : "SpreadsheetReportWatermarkUsedGeneral.prototype.doInitSpreadsheetReportWatermarkUsedGeneral=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkUsedGeneral",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "SpreadsheetReportWatermarkUsedGeneral.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkUsedGeneral",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]