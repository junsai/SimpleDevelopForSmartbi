[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "SpreadsheetReportWritebackDataLimit.prototype.init=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWritebackDataLimit",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "SpreadsheetReportWritebackDataLimit.prototype.validate=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWritebackDataLimit",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "SpreadsheetReportWritebackDataLimit.prototype.save=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWritebackDataLimit",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "SpreadsheetReportWritebackDataLimit.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWritebackDataLimit",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getOptions",
  "father" : "",
  "desc" : "getOptions",
  "name" : "SpreadsheetReportWritebackDataLimit.prototype.getOptions=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWritebackDataLimit",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]