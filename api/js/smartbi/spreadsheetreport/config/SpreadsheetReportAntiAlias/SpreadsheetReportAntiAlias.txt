[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "SpreadsheetReportAntiAlias.prototype.init=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportAntiAlias",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "SpreadsheetReportAntiAlias.prototype.validate=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportAntiAlias",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "SpreadsheetReportAntiAlias.prototype.save=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportAntiAlias",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doInitSpreadsheetReportAntiAlias",
  "father" : "",
  "desc" : "doInitSpreadsheetReportAntiAlias",
  "name" : "SpreadsheetReportAntiAlias.prototype.doInitSpreadsheetReportAntiAlias=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportAntiAlias",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "SpreadsheetReportAntiAlias.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportAntiAlias",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]