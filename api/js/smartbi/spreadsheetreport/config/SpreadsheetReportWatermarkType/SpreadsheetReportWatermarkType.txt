[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "SpreadsheetReportWatermarkType.prototype.init=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkType",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "SpreadsheetReportWatermarkType.prototype.validate=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkType",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "SpreadsheetReportWatermarkType.prototype.save=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkType",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doInitSpreadsheetReportWatermarkType",
  "father" : "",
  "desc" : "doInitSpreadsheetReportWatermarkType",
  "name" : "SpreadsheetReportWatermarkType.prototype.doInitSpreadsheetReportWatermarkType=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkType",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doChangeSpreadsheetReportWatermarkType",
  "father" : "",
  "desc" : "doChangeSpreadsheetReportWatermarkType",
  "name" : "SpreadsheetReportWatermarkType.prototype.doChangeSpreadsheetReportWatermarkType=function()",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkType",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "SpreadsheetReportWatermarkType.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "smartbi.spreadsheetreport.config.SpreadsheetReportWatermarkType",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]