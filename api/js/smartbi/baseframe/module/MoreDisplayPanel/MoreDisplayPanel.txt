[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "MoreDisplayPanel.prototype.init=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getTools",
  "father" : "",
  "desc" : "getTools",
  "name" : "MoreDisplayPanel.prototype.getTools=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "reRenderItems",
  "father" : "",
  "desc" : "reRenderItems",
  "name" : "MoreDisplayPanel.prototype.reRenderItems=function(items)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addFoldFunction",
  "father" : "",
  "desc" : "addFoldFunction",
  "name" : "MoreDisplayPanel.prototype.addFoldFunction=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "toggle",
  "father" : "",
  "desc" : "toggle",
  "name" : "MoreDisplayPanel.prototype.toggle=function(isopen)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "bindDrag",
  "father" : "",
  "desc" : "bindDrag",
  "name" : "MoreDisplayPanel.prototype.bindDrag=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "putItem",
  "father" : "",
  "desc" : "putItem",
  "name" : "MoreDisplayPanel.prototype.putItem=function(evt)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "moveItem",
  "father" : "",
  "desc" : "moveItem",
  "name" : "MoreDisplayPanel.prototype.moveItem=function(evt)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addItem",
  "father" : "",
  "desc" : "addItem",
  "name" : "MoreDisplayPanel.prototype.addItem=function(item,$target)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "toggleNullTip",
  "father" : "",
  "desc" : "toggleNullTip",
  "name" : "MoreDisplayPanel.prototype.toggleNullTip=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.MoreDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]