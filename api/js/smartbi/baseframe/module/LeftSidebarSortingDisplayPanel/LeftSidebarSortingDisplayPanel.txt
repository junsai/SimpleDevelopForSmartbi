[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.init=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doItemClick",
  "father" : "",
  "desc" : "doItemClick",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.doItemClick=function(e)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getTpl",
  "father" : "",
  "desc" : "getTpl",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.getTpl=function(tools)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "reRenderItems",
  "father" : "",
  "desc" : "reRenderItems",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.reRenderItems=function(items)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderItems",
  "father" : "",
  "desc" : "renderItems",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.renderItems=function(container,items)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "bindDrag",
  "father" : "",
  "desc" : "bindDrag",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.bindDrag=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addSaveUserConfigDebounce",
  "father" : "",
  "desc" : "addSaveUserConfigDebounce",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.addSaveUserConfigDebounce=function(callback)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "moveItem",
  "father" : "",
  "desc" : "moveItem",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.moveItem=function(evt)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getTools",
  "father" : "",
  "desc" : "getTools",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.getTools=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initSetting",
  "father" : "",
  "desc" : "initSetting",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.initSetting=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSetting",
  "father" : "",
  "desc" : "getSetting",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.getSetting=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "loadUserSortingSetting",
  "father" : "",
  "desc" : "loadUserSortingSetting",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.loadUserSortingSetting=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initDefaultSortingSetting",
  "father" : "",
  "desc" : "initDefaultSortingSetting",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.initDefaultSortingSetting=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getConfigItems",
  "father" : "",
  "desc" : "getConfigItems",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.getConfigItems=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "saveConfig",
  "father" : "",
  "desc" : "saveConfig",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.saveConfig=function(configKey,value,longValue,callback)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doReset",
  "father" : "",
  "desc" : "doReset",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.doReset=function(isResetBtnClick)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "afterReset",
  "father" : "",
  "desc" : "afterReset",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.afterReset=function(ret,isResetBtnClick)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSave",
  "father" : "",
  "desc" : "doSave",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.doSave=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "afterSave",
  "father" : "",
  "desc" : "afterSave",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.afterSave=function(ret)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isSameOrder",
  "father" : "",
  "desc" : "isSameOrder",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.isSameOrder=function(array1,array2)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getCurrentSortingSetting",
  "father" : "",
  "desc" : "getCurrentSortingSetting",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.getCurrentSortingSetting=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getOldSortingSetting",
  "father" : "",
  "desc" : "getOldSortingSetting",
  "name" : "LeftSidebarSortingDisplayPanel.prototype.getOldSortingSetting=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.LeftSidebarSortingDisplayPanel",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]