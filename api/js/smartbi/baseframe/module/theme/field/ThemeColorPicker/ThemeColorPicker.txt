[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "ThemeColorPicker.prototype.init=function(container,config)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "HexToRGB",
  "father" : "",
  "desc" : "HexToRGB",
  "name" : "ThemeColorPicker.prototype.HexToRGB=function(hex)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "RGBToHex",
  "father" : "",
  "desc" : "RGBToHex",
  "name" : "ThemeColorPicker.prototype.RGBToHex=function(rgb)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "buildColorPicker",
  "father" : "",
  "desc" : "buildColorPicker",
  "name" : "ThemeColorPicker.prototype.buildColorPicker=function(config)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showPicker",
  "father" : "",
  "desc" : "showPicker",
  "name" : "ThemeColorPicker.prototype.showPicker=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hidePicker",
  "father" : "",
  "desc" : "hidePicker",
  "name" : "ThemeColorPicker.prototype.hidePicker=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "mousewheel",
  "father" : "",
  "desc" : "mousewheel",
  "name" : "ThemeColorPicker.prototype.mousewheel=function(e)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "buildColor",
  "father" : "",
  "desc" : "buildColor",
  "name" : "ThemeColorPicker.prototype.buildColor=function(color)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getColor",
  "father" : "",
  "desc" : "getColor",
  "name" : "ThemeColorPicker.prototype.getColor=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setColor",
  "father" : "",
  "desc" : "setColor",
  "name" : "ThemeColorPicker.prototype.setColor=function(color)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isShowReset",
  "father" : "",
  "desc" : "isShowReset",
  "name" : "ThemeColorPicker.prototype.isShowReset=function(flag)",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "ThemeColorPicker.prototype.destroy=function()",
  "version" : "0",
  "path" : "smartbi.baseframe.module.theme.field.ThemeColorPicker",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]