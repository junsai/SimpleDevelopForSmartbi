[ {
  "type" : 0,
  "keyword" : "render",
  "father" : "",
  "desc" : "render",
  "name" : "TreeDataSetSetting.prototype.render=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderTableData",
  "father" : "",
  "desc" : "renderTableData",
  "name" : "TreeDataSetSetting.prototype.renderTableData=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderRowTree",
  "father" : "",
  "desc" : "renderRowTree",
  "name" : "TreeDataSetSetting.prototype.renderRowTree=function(row,i,state)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemIsParentAndSon_change_handler",
  "father" : "",
  "desc" : "elemIsParentAndSon_change_handler",
  "name" : "TreeDataSetSetting.prototype.elemIsParentAndSon_change_handler=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createSubNode",
  "father" : "",
  "desc" : "createSubNode",
  "name" : "TreeDataSetSetting.prototype.createSubNode=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createParentNode",
  "father" : "",
  "desc" : "createParentNode",
  "name" : "TreeDataSetSetting.prototype.createParentNode=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createName",
  "father" : "",
  "desc" : "createName",
  "name" : "TreeDataSetSetting.prototype.createName=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createIndex",
  "father" : "",
  "desc" : "createIndex",
  "name" : "TreeDataSetSetting.prototype.createIndex=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doYaxis",
  "father" : "",
  "desc" : "doYaxis",
  "name" : "TreeDataSetSetting.prototype.doYaxis=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSubNode",
  "father" : "",
  "desc" : "doSubNode",
  "name" : "TreeDataSetSetting.prototype.doSubNode=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doParentNode",
  "father" : "",
  "desc" : "doParentNode",
  "name" : "TreeDataSetSetting.prototype.doParentNode=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doName",
  "father" : "",
  "desc" : "doName",
  "name" : "TreeDataSetSetting.prototype.doName=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doIndex",
  "father" : "",
  "desc" : "doIndex",
  "name" : "TreeDataSetSetting.prototype.doIndex=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setOtherColUnChecked",
  "father" : "",
  "desc" : "setOtherColUnChecked",
  "name" : "TreeDataSetSetting.prototype.setOtherColUnChecked=function(bofid,type)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setOnlyChecked",
  "father" : "",
  "desc" : "setOnlyChecked",
  "name" : "TreeDataSetSetting.prototype.setOnlyChecked=function(bofid,typeName)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setRootName",
  "father" : "",
  "desc" : "setRootName",
  "name" : "TreeDataSetSetting.prototype.setRootName=function(rootName)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getRootName",
  "father" : "",
  "desc" : "getRootName",
  "name" : "TreeDataSetSetting.prototype.getRootName=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "TreeDataSetSetting.prototype.getValue=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setValue",
  "father" : "",
  "desc" : "setValue",
  "name" : "TreeDataSetSetting.prototype.setValue=function(obj)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setCtrlValue",
  "father" : "",
  "desc" : "setCtrlValue",
  "name" : "TreeDataSetSetting.prototype.setCtrlValue=function(objList)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkValidate",
  "father" : "",
  "desc" : "checkValidate",
  "name" : "TreeDataSetSetting.prototype.checkValidate=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createOperation",
  "father" : "",
  "desc" : "createOperation",
  "name" : "TreeDataSetSetting.prototype.createOperation=function(row,i,obj)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "arrowUpField",
  "father" : "",
  "desc" : "arrowUpField",
  "name" : "TreeDataSetSetting.prototype.arrowUpField=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "arrowDownField",
  "father" : "",
  "desc" : "arrowDownField",
  "name" : "TreeDataSetSetting.prototype.arrowDownField=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changePosition",
  "father" : "",
  "desc" : "changePosition",
  "name" : "TreeDataSetSetting.prototype.changePosition=function(fiedList,i,j)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "reInitData",
  "father" : "",
  "desc" : "reInitData",
  "name" : "TreeDataSetSetting.prototype.reInitData=function(obj)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.TreeDataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]