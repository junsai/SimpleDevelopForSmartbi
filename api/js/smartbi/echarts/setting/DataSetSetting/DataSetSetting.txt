[ {
  "type" : 0,
  "keyword" : "setTemplateTypeState",
  "father" : "",
  "desc" : "setTemplateTypeState",
  "name" : "DataSetSetting.prototype.setTemplateTypeState=function(state)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getTemplateTypeState",
  "father" : "",
  "desc" : "getTemplateTypeState",
  "name" : "DataSetSetting.prototype.getTemplateTypeState=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "DataSetSetting.prototype.destroy=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "render",
  "father" : "",
  "desc" : "render",
  "name" : "DataSetSetting.prototype.render=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderRow5",
  "father" : "",
  "desc" : "renderRow5",
  "name" : "DataSetSetting.prototype.renderRow5=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderRowScatter",
  "father" : "",
  "desc" : "renderRowScatter",
  "name" : "DataSetSetting.prototype.renderRowScatter=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderRow1",
  "father" : "",
  "desc" : "renderRow1",
  "name" : "DataSetSetting.prototype.renderRow1=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderRow7",
  "father" : "",
  "desc" : "renderRow7",
  "name" : "DataSetSetting.prototype.renderRow7=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderRowGraph",
  "father" : "",
  "desc" : "renderRowGraph",
  "name" : "DataSetSetting.prototype.renderRowGraph=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderRowCombination",
  "father" : "",
  "desc" : "renderRowCombination",
  "name" : "DataSetSetting.prototype.renderRowCombination=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderRowHeatmap",
  "father" : "",
  "desc" : "renderRowHeatmap",
  "name" : "DataSetSetting.prototype.renderRowHeatmap=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderRowWordCloud",
  "father" : "",
  "desc" : "renderRowWordCloud",
  "name" : "DataSetSetting.prototype.renderRowWordCloud=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createName",
  "father" : "",
  "desc" : "createName",
  "name" : "DataSetSetting.prototype.createName=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createSY",
  "father" : "",
  "desc" : "createSY",
  "name" : "DataSetSetting.prototype.createSY=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createS",
  "father" : "",
  "desc" : "createS",
  "name" : "DataSetSetting.prototype.createS=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createZ",
  "father" : "",
  "desc" : "createZ",
  "name" : "DataSetSetting.prototype.createZ=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createR",
  "father" : "",
  "desc" : "createR",
  "name" : "DataSetSetting.prototype.createR=function(row,i)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doGraphClick",
  "father" : "",
  "desc" : "doGraphClick",
  "name" : "DataSetSetting.prototype.doGraphClick=function(e)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createGraphCell",
  "father" : "",
  "desc" : "createGraphCell",
  "name" : "DataSetSetting.prototype.createGraphCell=function(row,i,value)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createCell",
  "father" : "",
  "desc" : "createCell",
  "name" : "DataSetSetting.prototype.createCell=function(row,i,value)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "DataSetSetting.prototype.getValue=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setValue",
  "father" : "",
  "desc" : "setValue",
  "name" : "DataSetSetting.prototype.setValue=function(obj)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkValidate",
  "father" : "",
  "desc" : "checkValidate",
  "name" : "DataSetSetting.prototype.checkValidate=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCategories",
  "father" : "",
  "desc" : "doCategories",
  "name" : "DataSetSetting.prototype.doCategories=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doYaxis",
  "father" : "",
  "desc" : "doYaxis",
  "name" : "DataSetSetting.prototype.doYaxis=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSYaxis",
  "father" : "",
  "desc" : "doSYaxis",
  "name" : "DataSetSetting.prototype.doSYaxis=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createLinkItemSettingObj",
  "father" : "",
  "desc" : "createLinkItemSettingObj",
  "name" : "DataSetSetting.prototype.createLinkItemSettingObj=function(container,relativeElem)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createSeriesSettingObj",
  "father" : "",
  "desc" : "createSeriesSettingObj",
  "name" : "DataSetSetting.prototype.createSeriesSettingObj=function(container,relativeElem)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroySeriesSettingObj",
  "father" : "",
  "desc" : "destroySeriesSettingObj",
  "name" : "DataSetSetting.prototype.destroySeriesSettingObj=function(settingBtnObj)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleLinkItemSettingBtnClick",
  "father" : "",
  "desc" : "handleLinkItemSettingBtnClick",
  "name" : "DataSetSetting.prototype.handleLinkItemSettingBtnClick=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleSeriesSettingBtnClick",
  "father" : "",
  "desc" : "handleSeriesSettingBtnClick",
  "name" : "DataSetSetting.prototype.handleSeriesSettingBtnClick=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doDisplayNameClick",
  "father" : "",
  "desc" : "doDisplayNameClick",
  "name" : "DataSetSetting.prototype.doDisplayNameClick=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doZaxis",
  "father" : "",
  "desc" : "doZaxis",
  "name" : "DataSetSetting.prototype.doZaxis=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setTableOnlyChecked",
  "father" : "",
  "desc" : "setTableOnlyChecked",
  "name" : "DataSetSetting.prototype.setTableOnlyChecked=function(tableId,tableType)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSeries",
  "father" : "",
  "desc" : "doSeries",
  "name" : "DataSetSetting.prototype.doSeries=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRenderAsChanged",
  "father" : "",
  "desc" : "doRenderAsChanged",
  "name" : "DataSetSetting.prototype.doRenderAsChanged=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDisplayModeByTemplate",
  "father" : "",
  "desc" : "getDisplayModeByTemplate",
  "name" : "DataSetSetting.prototype.getDisplayModeByTemplate=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDisplayModeByControl",
  "father" : "",
  "desc" : "getDisplayModeByControl",
  "name" : "DataSetSetting.prototype.getDisplayModeByControl=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isWaterFall",
  "father" : "",
  "desc" : "isWaterFall",
  "name" : "DataSetSetting.prototype.isWaterFall=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeSeriesColumnState",
  "father" : "",
  "desc" : "changeSeriesColumnState",
  "name" : "DataSetSetting.prototype.changeSeriesColumnState=function(state)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeZColumnState",
  "father" : "",
  "desc" : "changeZColumnState",
  "name" : "DataSetSetting.prototype.changeZColumnState=function(state)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeColumnState",
  "father" : "",
  "desc" : "changeColumnState",
  "name" : "DataSetSetting.prototype.changeColumnState=function(columnName,objKey,state)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "synAxisName",
  "father" : "",
  "desc" : "synAxisName",
  "name" : "DataSetSetting.prototype.synAxisName=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getFieldOptions",
  "father" : "",
  "desc" : "getFieldOptions",
  "name" : "DataSetSetting.prototype.getFieldOptions=function(notDouble)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getCurrentFields",
  "father" : "",
  "desc" : "getCurrentFields",
  "name" : "DataSetSetting.prototype.getCurrentFields=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataSetSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]