[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "IntervalColorsSetting.prototype.init=function(parent,params,fn,obj)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemColorSettingControlBox_click_handler",
  "father" : "",
  "desc" : "elemColorSettingControlBox_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemColorSettingControlBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemColorSchemeBox_click_handler",
  "father" : "",
  "desc" : "elemColorSchemeBox_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemColorSchemeBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemFlagSizeBox_click_handler",
  "father" : "",
  "desc" : "elemFlagSizeBox_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemFlagSizeBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemTopNEffectSettingBox_click_handler",
  "father" : "",
  "desc" : "elemTopNEffectSettingBox_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemTopNEffectSettingBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemColorTextStyleBox_click_handler",
  "father" : "",
  "desc" : "elemColorTextStyleBox_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemColorTextStyleBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemLegendLocationBox_click_handler",
  "father" : "",
  "desc" : "elemLegendLocationBox_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemLegendLocationBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemColorTypeBox_click_handler",
  "father" : "",
  "desc" : "elemColorTypeBox_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemColorTypeBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemTopNNumber_click_handler",
  "father" : "",
  "desc" : "elemTopNNumber_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemTopNNumber_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemTopNSettingBox_click_handler",
  "father" : "",
  "desc" : "elemTopNSettingBox_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemTopNSettingBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemChartNameBox_click_handler",
  "father" : "",
  "desc" : "elemChartNameBox_click_handler",
  "name" : "IntervalColorsSetting.prototype.elemChartNameBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "colorTextStryleCheckFuc",
  "father" : "",
  "desc" : "colorTextStryleCheckFuc",
  "name" : "IntervalColorsSetting.prototype.colorTextStryleCheckFuc=function(obj,objTr,checked)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "IntervalColorsSetting.prototype.destroy=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initLegendsettings",
  "father" : "",
  "desc" : "initLegendsettings",
  "name" : "IntervalColorsSetting.prototype.initLegendsettings=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initGrid",
  "father" : "",
  "desc" : "initGrid",
  "name" : "IntervalColorsSetting.prototype.initGrid=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initGridOfContinuous",
  "father" : "",
  "desc" : "initGridOfContinuous",
  "name" : "IntervalColorsSetting.prototype.initGridOfContinuous=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initGridOfPiecewise",
  "father" : "",
  "desc" : "initGridOfPiecewise",
  "name" : "IntervalColorsSetting.prototype.initGridOfPiecewise=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initTable",
  "father" : "",
  "desc" : "initTable",
  "name" : "IntervalColorsSetting.prototype.initTable=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initTableOfContinuous",
  "father" : "",
  "desc" : "initTableOfContinuous",
  "name" : "IntervalColorsSetting.prototype.initTableOfContinuous=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initTableOfPiecewise",
  "father" : "",
  "desc" : "initTableOfPiecewise",
  "name" : "IntervalColorsSetting.prototype.initTableOfPiecewise=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initFlagType",
  "father" : "",
  "desc" : "initFlagType",
  "name" : "IntervalColorsSetting.prototype.initFlagType=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initSystemColors",
  "father" : "",
  "desc" : "initSystemColors",
  "name" : "IntervalColorsSetting.prototype.initSystemColors=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertSystemColorsItems",
  "father" : "",
  "desc" : "insertSystemColorsItems",
  "name" : "IntervalColorsSetting.prototype.insertSystemColorsItems=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showSystemColors",
  "father" : "",
  "desc" : "showSystemColors",
  "name" : "IntervalColorsSetting.prototype.showSystemColors=function(obj,oldId,selectedId)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "restoreDefaultColors",
  "father" : "",
  "desc" : "restoreDefaultColors",
  "name" : "IntervalColorsSetting.prototype.restoreDefaultColors=function(colors,colorsList)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkColors",
  "father" : "",
  "desc" : "checkColors",
  "name" : "IntervalColorsSetting.prototype.checkColors=function(colors)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isField",
  "father" : "",
  "desc" : "isField",
  "name" : "IntervalColorsSetting.prototype.isField=function(fieldId)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkInvalidCharacter",
  "father" : "",
  "desc" : "checkInvalidCharacter",
  "name" : "IntervalColorsSetting.prototype.checkInvalidCharacter=function(value,elementName,allowEmpty)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getGridData",
  "father" : "",
  "desc" : "getGridData",
  "name" : "IntervalColorsSetting.prototype.getGridData=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkValidate",
  "father" : "",
  "desc" : "checkValidate",
  "name" : "IntervalColorsSetting.prototype.checkValidate=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doGridValueChange",
  "father" : "",
  "desc" : "doGridValueChange",
  "name" : "IntervalColorsSetting.prototype.doGridValueChange=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "IntervalColorsSetting.prototype.doOK=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "IntervalColorsSetting.prototype.doClose=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onColorTypeToggleChanged",
  "father" : "",
  "desc" : "onColorTypeToggleChanged",
  "name" : "IntervalColorsSetting.prototype.onColorTypeToggleChanged=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onToggleChanged",
  "father" : "",
  "desc" : "onToggleChanged",
  "name" : "IntervalColorsSetting.prototype.onToggleChanged=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeOtherSpinner",
  "father" : "",
  "desc" : "changeOtherSpinner",
  "name" : "IntervalColorsSetting.prototype.changeOtherSpinner=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "chooseBrushType",
  "father" : "",
  "desc" : "chooseBrushType",
  "name" : "IntervalColorsSetting.prototype.chooseBrushType=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemLeftOfCanvas_blur_handler",
  "father" : "",
  "desc" : "elemLeftOfCanvas_blur_handler",
  "name" : "IntervalColorsSetting.prototype.elemLeftOfCanvas_blur_handler=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemTopOfCanvas_blur_handler",
  "father" : "",
  "desc" : "elemTopOfCanvas_blur_handler",
  "name" : "IntervalColorsSetting.prototype.elemTopOfCanvas_blur_handler=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemTopNNumber_blur_handler",
  "father" : "",
  "desc" : "elemTopNNumber_blur_handler",
  "name" : "IntervalColorsSetting.prototype.elemTopNNumber_blur_handler=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkNumValidate",
  "father" : "",
  "desc" : "checkNumValidate",
  "name" : "IntervalColorsSetting.prototype.checkNumValidate=function(obj)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkSymbolSize",
  "father" : "",
  "desc" : "checkSymbolSize",
  "name" : "IntervalColorsSetting.prototype.checkSymbolSize=function(obj1,obj2)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.IntervalColorsSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]