[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "DataProcessSetting.prototype.destroy=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initComponent",
  "father" : "",
  "desc" : "initComponent",
  "name" : "DataProcessSetting.prototype.initComponent=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initSort",
  "father" : "",
  "desc" : "initSort",
  "name" : "DataProcessSetting.prototype.initSort=function(currentFields)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDefaultChartName",
  "father" : "",
  "desc" : "getDefaultChartName",
  "name" : "DataProcessSetting.prototype.getDefaultChartName=function(chartType)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addUsedCheckboxClickHandler",
  "father" : "",
  "desc" : "addUsedCheckboxClickHandler",
  "name" : "DataProcessSetting.prototype.addUsedCheckboxClickHandler=function(cbox,cboxTd)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "show",
  "father" : "",
  "desc" : "show",
  "name" : "DataProcessSetting.prototype.show=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOutputRowsSystem_click_handler",
  "father" : "",
  "desc" : "elemOutputRowsSystem_click_handler",
  "name" : "DataProcessSetting.prototype.elemOutputRowsSystem_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOutputRowsAll_click_handler",
  "father" : "",
  "desc" : "elemOutputRowsAll_click_handler",
  "name" : "DataProcessSetting.prototype.elemOutputRowsAll_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOutputRowsCustom_click_handler",
  "father" : "",
  "desc" : "elemOutputRowsCustom_click_handler",
  "name" : "DataProcessSetting.prototype.elemOutputRowsCustom_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemChartName_click_handler",
  "father" : "",
  "desc" : "elemChartName_click_handler",
  "name" : "DataProcessSetting.prototype.elemChartName_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOutputDataDynamicBox_click_handler",
  "father" : "",
  "desc" : "elemOutputDataDynamicBox_click_handler",
  "name" : "DataProcessSetting.prototype.elemOutputDataDynamicBox_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOutputRowDiv_click_handler",
  "father" : "",
  "desc" : "elemOutputRowDiv_click_handler",
  "name" : "DataProcessSetting.prototype.elemOutputRowDiv_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOutputDataDynamicDiv_click_handler",
  "father" : "",
  "desc" : "elemOutputDataDynamicDiv_click_handler",
  "name" : "DataProcessSetting.prototype.elemOutputDataDynamicDiv_click_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemActivatedClickHandler",
  "father" : "",
  "desc" : "elemActivatedClickHandler",
  "name" : "DataProcessSetting.prototype.elemActivatedClickHandler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemThemePreviewLabel_mouseleave_handler",
  "father" : "",
  "desc" : "elemThemePreviewLabel_mouseleave_handler",
  "name" : "DataProcessSetting.prototype.elemThemePreviewLabel_mouseleave_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemThemePreviewLabel_mouseenter_handler",
  "father" : "",
  "desc" : "elemThemePreviewLabel_mouseenter_handler",
  "name" : "DataProcessSetting.prototype.elemThemePreviewLabel_mouseenter_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOutputRowsText_keypress_handler",
  "father" : "",
  "desc" : "elemOutputRowsText_keypress_handler",
  "name" : "DataProcessSetting.prototype.elemOutputRowsText_keypress_handler=function(ev)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setOutputRowsAllVisible",
  "father" : "",
  "desc" : "setOutputRowsAllVisible",
  "name" : "DataProcessSetting.prototype.setOutputRowsAllVisible=function(visible)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateUsedCheckbox",
  "father" : "",
  "desc" : "updateUsedCheckbox",
  "name" : "DataProcessSetting.prototype.updateUsedCheckbox=function(cbox,checked)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setValue",
  "father" : "",
  "desc" : "setValue",
  "name" : "DataProcessSetting.prototype.setValue=function(obj)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "DataProcessSetting.prototype.getValue=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOutputRowsText_blur_handler",
  "father" : "",
  "desc" : "elemOutputRowsText_blur_handler",
  "name" : "DataProcessSetting.prototype.elemOutputRowsText_blur_handler=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkValidate",
  "father" : "",
  "desc" : "checkValidate",
  "name" : "DataProcessSetting.prototype.checkValidate=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getObjValueByKey",
  "father" : "",
  "desc" : "getObjValueByKey",
  "name" : "DataProcessSetting.prototype.getObjValueByKey=function(obj,key)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setObjValue",
  "father" : "",
  "desc" : "setObjValue",
  "name" : "DataProcessSetting.prototype.setObjValue=function(obj,key,value)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemSortControlBox_click_handler",
  "father" : "",
  "desc" : "elemSortControlBox_click_handler",
  "name" : "DataProcessSetting.prototype.elemSortControlBox_click_handler=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemSortTypeRadio1_click_handler",
  "father" : "",
  "desc" : "elemSortTypeRadio1_click_handler",
  "name" : "DataProcessSetting.prototype.elemSortTypeRadio1_click_handler=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemSortTypeRadio2_click_handler",
  "father" : "",
  "desc" : "elemSortTypeRadio2_click_handler",
  "name" : "DataProcessSetting.prototype.elemSortTypeRadio2_click_handler=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "themeChange",
  "father" : "",
  "desc" : "themeChange",
  "name" : "DataProcessSetting.prototype.themeChange=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemChartThemeBox_click_handler",
  "father" : "",
  "desc" : "elemChartThemeBox_click_handler",
  "name" : "DataProcessSetting.prototype.elemChartThemeBox_click_handler=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isShowChartOutputDataDynamic",
  "father" : "",
  "desc" : "isShowChartOutputDataDynamic",
  "name" : "DataProcessSetting.prototype.isShowChartOutputDataDynamic=function()",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "synAxisNamebyTheme",
  "father" : "",
  "desc" : "synAxisNamebyTheme",
  "name" : "DataProcessSetting.prototype.synAxisNamebyTheme=function(that)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getThemeJSON",
  "father" : "",
  "desc" : "getThemeJSON",
  "name" : "DataProcessSetting.prototype.getThemeJSON=function(themeName)",
  "version" : "0",
  "path" : "smartbi.echarts.setting.DataProcessSetting",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]