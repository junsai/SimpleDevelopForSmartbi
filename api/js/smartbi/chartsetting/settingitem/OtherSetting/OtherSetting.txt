[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "OtherSetting.prototype.destroy=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doLostTabActive",
  "father" : "",
  "desc" : "doLostTabActive",
  "name" : "OtherSetting.prototype.doLostTabActive=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnTabActive",
  "father" : "",
  "desc" : "doOnTabActive",
  "name" : "OtherSetting.prototype.doOnTabActive=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemFormatCode_click_handler",
  "father" : "",
  "desc" : "elemFormatCode_click_handler",
  "name" : "OtherSetting.prototype.elemFormatCode_click_handler=function(e)",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setValue",
  "father" : "",
  "desc" : "setValue",
  "name" : "OtherSetting.prototype.setValue=function(obj)",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "OtherSetting.prototype.getValue=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkValidate",
  "father" : "",
  "desc" : "checkValidate",
  "name" : "OtherSetting.prototype.checkValidate=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemExampleCode_click_handler",
  "father" : "",
  "desc" : "elemExampleCode_click_handler",
  "name" : "OtherSetting.prototype.elemExampleCode_click_handler=function(e)",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemEChartsOptions_click_handler",
  "father" : "",
  "desc" : "elemEChartsOptions_click_handler",
  "name" : "OtherSetting.prototype.elemEChartsOptions_click_handler=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setArrayPrototype",
  "father" : "",
  "desc" : "setArrayPrototype",
  "name" : "OtherSetting.prototype.setArrayPrototype=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.settingitem.OtherSetting",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]