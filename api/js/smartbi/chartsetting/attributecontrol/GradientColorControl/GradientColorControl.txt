[ {
  "type" : 0,
  "keyword" : "initLayout",
  "father" : "",
  "desc" : "initLayout",
  "name" : "GradientColorControl.prototype.initLayout=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "GradientColorControl.prototype.destroy=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "GradientColorControl.prototype.getValue=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setValue",
  "father" : "",
  "desc" : "setValue",
  "name" : "GradientColorControl.prototype.setValue=function(obj)",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setDisabled",
  "father" : "",
  "desc" : "setDisabled",
  "name" : "GradientColorControl.prototype.setDisabled=function(disabled)",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkValidate",
  "father" : "",
  "desc" : "checkValidate",
  "name" : "GradientColorControl.prototype.checkValidate=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "drawGradient",
  "father" : "",
  "desc" : "drawGradient",
  "name" : "GradientColorControl.prototype.drawGradient=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doBeginColorValueChange",
  "father" : "",
  "desc" : "doBeginColorValueChange",
  "name" : "GradientColorControl.prototype.doBeginColorValueChange=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doEndColorValueChange",
  "father" : "",
  "desc" : "doEndColorValueChange",
  "name" : "GradientColorControl.prototype.doEndColorValueChange=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAngleChange",
  "father" : "",
  "desc" : "doAngleChange",
  "name" : "GradientColorControl.prototype.doAngleChange=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doValueChange",
  "father" : "",
  "desc" : "doValueChange",
  "name" : "GradientColorControl.prototype.doValueChange=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSecondColorDisabled",
  "father" : "",
  "desc" : "setSecondColorDisabled",
  "name" : "GradientColorControl.prototype.setSecondColorDisabled=function(flag)",
  "version" : "0",
  "path" : "smartbi.chartsetting.attributecontrol.GradientColorControl",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]