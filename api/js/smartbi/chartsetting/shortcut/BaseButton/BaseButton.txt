[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "BaseButton.prototype.destroy=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "unrender",
  "father" : "",
  "desc" : "unrender",
  "name" : "BaseButton.prototype.unrender=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "render",
  "father" : "",
  "desc" : "render",
  "name" : "BaseButton.prototype.render=function(defObj,chartType)",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hidden",
  "father" : "",
  "desc" : "hidden",
  "name" : "BaseButton.prototype.hidden=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "show",
  "father" : "",
  "desc" : "show",
  "name" : "BaseButton.prototype.show=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getTitle",
  "father" : "",
  "desc" : "getTitle",
  "name" : "BaseButton.prototype.getTitle=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getClassName",
  "father" : "",
  "desc" : "getClassName",
  "name" : "BaseButton.prototype.getClassName=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getOffset",
  "father" : "",
  "desc" : "getOffset",
  "name" : "BaseButton.prototype.getOffset=function(obj,parent)",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elementInPanel",
  "father" : "",
  "desc" : "elementInPanel",
  "name" : "BaseButton.prototype.elementInPanel=function(el,panel)",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnClick",
  "father" : "",
  "desc" : "doOnClick",
  "name" : "BaseButton.prototype.doOnClick=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMouseEnter",
  "father" : "",
  "desc" : "onMouseEnter",
  "name" : "BaseButton.prototype.onMouseEnter=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onMouseLeave",
  "father" : "",
  "desc" : "onMouseLeave",
  "name" : "BaseButton.prototype.onMouseLeave=function()",
  "version" : "0",
  "path" : "smartbi.chartsetting.shortcut.BaseButton",
  "updateTime" : "2021-09-22 01:18:32",
  "isDelete" : 0
} ]