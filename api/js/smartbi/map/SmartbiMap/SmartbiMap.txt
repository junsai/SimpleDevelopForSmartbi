[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "init:function(viewOptions,chartType,chartClassName)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "destroy:function()",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getChart",
  "father" : "",
  "desc" : "getChart",
  "name" : "getChart:function()",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setChart",
  "father" : "",
  "desc" : "setChart",
  "name" : "setChart:function(hcChart)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getOptions",
  "father" : "",
  "desc" : "getOptions",
  "name" : "getOptions:function()",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSVG",
  "father" : "",
  "desc" : "getSVG",
  "name" : "getSVG:function()",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showLoading",
  "father" : "",
  "desc" : "showLoading",
  "name" : "showLoading:function(message,loadingDiv,conWidth,conHeight)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideLoading",
  "father" : "",
  "desc" : "hideLoading",
  "name" : "hideLoading:function(loadingContainer)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refresh",
  "father" : "",
  "desc" : "refresh",
  "name" : "refresh:function(container,chartOptions)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resizeTo",
  "father" : "",
  "desc" : "resizeTo",
  "name" : "resizeTo:function(w,h)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "mergerDefaultOptions",
  "father" : "",
  "desc" : "mergerDefaultOptions",
  "name" : "mergerDefaultOptions:function(chartOptions,chartType)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "preprocessServerChartOptions",
  "father" : "",
  "desc" : "preprocessServerChartOptions",
  "name" : "preprocessServerChartOptions:function(chartOptions)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "preprocessChartOptions",
  "father" : "",
  "desc" : "preprocessChartOptions",
  "name" : "preprocessChartOptions:function(options)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "process",
  "father" : "",
  "desc" : "process",
  "name" : "process:function(options)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "attachEvents",
  "father" : "",
  "desc" : "attachEvents",
  "name" : "attachEvents:function(options)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "processOptions",
  "father" : "",
  "desc" : "processOptions",
  "name" : "processOptions:function(chart)",
  "version" : "0",
  "path" : "smartbi.map.SmartbiMap",
  "updateTime" : "2021-09-22 01:18:33",
  "isDelete" : 0
} ]