[ {
  "type" : 0,
  "keyword" : "$",
  "father" : "",
  "desc" : "$",
  "name" : "$:function(id)",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "create",
  "father" : "",
  "desc" : "create",
  "name" : "create:function(s,b)",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getPageOffsetTop",
  "father" : "",
  "desc" : "getPageOffsetTop",
  "name" : "getPageOffsetTop:function(el,container)",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getWindowWidth",
  "father" : "",
  "desc" : "getWindowWidth",
  "name" : "getWindowWidth:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getWindowHeight",
  "father" : "",
  "desc" : "getWindowHeight",
  "name" : "getWindowHeight:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addEvent",
  "father" : "",
  "desc" : "addEvent",
  "name" : "addEvent:function(obj,type,fn)",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeEvent",
  "father" : "",
  "desc" : "removeEvent",
  "name" : "removeEvent:function(obj,type,fn)",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "print",
  "father" : "",
  "desc" : "print",
  "name" : "print:function(obj,_level)",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "run",
  "father" : "",
  "desc" : "run",
  "name" : "run:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "log",
  "father" : "",
  "desc" : "log",
  "name" : "log:function(txts)",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clear",
  "father" : "",
  "desc" : "clear",
  "name" : "clear:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "close",
  "father" : "",
  "desc" : "close",
  "name" : "close:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setControlSize",
  "father" : "",
  "desc" : "setControlSize",
  "name" : "setControlSize:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resize",
  "father" : "",
  "desc" : "resize",
  "name" : "resize:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "scrollIntoView",
  "father" : "",
  "desc" : "scrollIntoView",
  "name" : "scrollIntoView:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "onKeyDown",
  "father" : "",
  "desc" : "onKeyDown",
  "name" : "onKeyDown:function(evt)",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "init:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setup",
  "father" : "",
  "desc" : "setup",
  "name" : "setup:function()",
  "version" : "0",
  "path" : "jsconsole",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
} ]