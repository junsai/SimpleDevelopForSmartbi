[ {
  "type" : 0,
  "keyword" : "setHidden",
  "father" : "",
  "desc" : "setHidden",
  "name" : "PageToolbar.prototype.setHidden=function(isHidden)",
  "version" : "0",
  "path" : "bof.macro.sdk.smartbix.implements.PageToolbar",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isHidden",
  "father" : "",
  "desc" : "isHidden",
  "name" : "PageToolbar.prototype.isHidden=function()",
  "version" : "0",
  "path" : "bof.macro.sdk.smartbix.implements.PageToolbar",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "unfold",
  "father" : "",
  "desc" : "unfold",
  "name" : "PageToolbar.prototype.unfold=function()",
  "version" : "0",
  "path" : "bof.macro.sdk.smartbix.implements.PageToolbar",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fold",
  "father" : "",
  "desc" : "fold",
  "name" : "PageToolbar.prototype.fold=function()",
  "version" : "0",
  "path" : "bof.macro.sdk.smartbix.implements.PageToolbar",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getButtons",
  "father" : "",
  "desc" : "getButtons",
  "name" : "PageToolbar.prototype.getButtons=function()",
  "version" : "0",
  "path" : "bof.macro.sdk.smartbix.implements.PageToolbar",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addButton",
  "father" : "",
  "desc" : "addButton",
  "name" : "PageToolbar.prototype.addButton=function(menu,index)",
  "version" : "0",
  "path" : "bof.macro.sdk.smartbix.implements.PageToolbar",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "removeButton",
  "father" : "",
  "desc" : "removeButton",
  "name" : "PageToolbar.prototype.removeButton=function(index)",
  "version" : "0",
  "path" : "bof.macro.sdk.smartbix.implements.PageToolbar",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]