[ {
  "type" : 0,
  "keyword" : "initRuleListBox",
  "father" : "",
  "desc" : "initRuleListBox",
  "name" : "SimpleReportPackDef.prototype.initRuleListBox=function()",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRuleDataSource",
  "father" : "",
  "desc" : "doRuleDataSource",
  "name" : "SimpleReportPackDef.prototype.doRuleDataSource=function(e)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRuleBusinessView",
  "father" : "",
  "desc" : "doRuleBusinessView",
  "name" : "SimpleReportPackDef.prototype.doRuleBusinessView=function(e)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRuleSimpleReport",
  "father" : "",
  "desc" : "doRuleSimpleReport",
  "name" : "SimpleReportPackDef.prototype.doRuleSimpleReport=function(e)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "afterRuleDataSource",
  "father" : "",
  "desc" : "afterRuleDataSource",
  "name" : "SimpleReportPackDef.prototype.afterRuleDataSource=function(openType,list)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "afterRuleBusinessView",
  "father" : "",
  "desc" : "afterRuleBusinessView",
  "name" : "SimpleReportPackDef.prototype.afterRuleBusinessView=function(openType,list)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "afterRuleSimpleReport",
  "father" : "",
  "desc" : "afterRuleSimpleReport",
  "name" : "SimpleReportPackDef.prototype.afterRuleSimpleReport=function(openType,list)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setDirty",
  "father" : "",
  "desc" : "setDirty",
  "name" : "SimpleReportPackDef.prototype.setDirty=function(dirty)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createXML",
  "father" : "",
  "desc" : "createXML",
  "name" : "SimpleReportPackDef.prototype.createXML=function()",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initFromXml",
  "father" : "",
  "desc" : "initFromXml",
  "name" : "SimpleReportPackDef.prototype.initFromXml=function(xml)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createMatch",
  "father" : "",
  "desc" : "createMatch",
  "name" : "SimpleReportPackDef.prototype.createMatch=function(dom,type)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRuleNoteDataSource",
  "father" : "",
  "desc" : "doRuleNoteDataSource",
  "name" : "SimpleReportPackDef.prototype.doRuleNoteDataSource=function(e)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRuleNoteBusinessView",
  "father" : "",
  "desc" : "doRuleNoteBusinessView",
  "name" : "SimpleReportPackDef.prototype.doRuleNoteBusinessView=function(e)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRuleNoteSimpleReport",
  "father" : "",
  "desc" : "doRuleNoteSimpleReport",
  "name" : "SimpleReportPackDef.prototype.doRuleNoteSimpleReport=function(e)",
  "version" : "0",
  "path" : "bof.macro.views.SimpleReportPackDef",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]