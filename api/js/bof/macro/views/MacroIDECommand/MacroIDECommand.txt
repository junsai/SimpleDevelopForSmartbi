[ {
  "type" : 0,
  "keyword" : "execute",
  "father" : "",
  "desc" : "execute",
  "name" : "MacroIDECommand.prototype.execute=function(action,node)",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "close",
  "father" : "",
  "desc" : "close",
  "name" : "MacroIDECommand.prototype.close=function()",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "MacroIDECommand.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnClose",
  "father" : "",
  "desc" : "doOnClose",
  "name" : "MacroIDECommand.prototype.doOnClose=function()",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnNeedRefresh",
  "father" : "",
  "desc" : "doOnNeedRefresh",
  "name" : "MacroIDECommand.prototype.doOnNeedRefresh=function(view,id)",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "openMacroIDE",
  "father" : "",
  "desc" : "openMacroIDE",
  "name" : "MacroIDECommand.prototype.openMacroIDE=function(node)",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createMacroGuide",
  "father" : "",
  "desc" : "createMacroGuide",
  "name" : "MacroIDECommand.prototype.createMacroGuide=function(node)",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createMacroIDE",
  "father" : "",
  "desc" : "createMacroIDE",
  "name" : "MacroIDECommand.prototype.createMacroIDE=function(node)",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "macroIDEEnabled",
  "father" : "",
  "desc" : "macroIDEEnabled",
  "name" : "MacroIDECommand.prototype.macroIDEEnabled=function(node)",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkWritePermission",
  "father" : "",
  "desc" : "checkWritePermission",
  "name" : "MacroIDECommand.prototype.checkWritePermission=function(node)",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkStaticResource",
  "father" : "",
  "desc" : "checkStaticResource",
  "name" : "MacroIDECommand.prototype.checkStaticResource=function(node)",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "confirmClose",
  "father" : "",
  "desc" : "confirmClose",
  "name" : "MacroIDECommand.prototype.confirmClose=function(callback)",
  "version" : "0",
  "path" : "bof.macro.views.MacroIDECommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]