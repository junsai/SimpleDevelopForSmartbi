[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "AlterPasswordDialog.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.config.dialog.AlterPasswordDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "AlterPasswordDialog.prototype.init=function(parent,data,fn,obj)",
  "version" : "0",
  "path" : "bof.config.dialog.AlterPasswordDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "AlterPasswordDialog.prototype.doOK=function()",
  "version" : "0",
  "path" : "bof.config.dialog.AlterPasswordDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "AlterPasswordDialog.prototype.doClose=function()",
  "version" : "0",
  "path" : "bof.config.dialog.AlterPasswordDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemOldPassword_focus_handler",
  "father" : "",
  "desc" : "elemOldPassword_focus_handler",
  "name" : "AlterPasswordDialog.prototype.elemOldPassword_focus_handler=function()",
  "version" : "0",
  "path" : "bof.config.dialog.AlterPasswordDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemNewPassword_focus_handler",
  "father" : "",
  "desc" : "elemNewPassword_focus_handler",
  "name" : "AlterPasswordDialog.prototype.elemNewPassword_focus_handler=function()",
  "version" : "0",
  "path" : "bof.config.dialog.AlterPasswordDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemConfirmPassword_focus_handler",
  "father" : "",
  "desc" : "elemConfirmPassword_focus_handler",
  "name" : "AlterPasswordDialog.prototype.elemConfirmPassword_focus_handler=function()",
  "version" : "0",
  "path" : "bof.config.dialog.AlterPasswordDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
} ]