[ {
  "type" : 0,
  "keyword" : "hasManageOp",
  "father" : "",
  "desc" : "hasManageOp",
  "name" : "hasManageOp:function()",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getBaseManager",
  "father" : "",
  "desc" : "getBaseManager",
  "name" : "getBaseManager:function()",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getHandlerTree",
  "father" : "",
  "desc" : "getHandlerTree",
  "name" : "getHandlerTree:function(id,type,onlyForOpen)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isLocateType",
  "father" : "",
  "desc" : "isLocateType",
  "name" : "isLocateType:function(type)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getMacroModuleResource",
  "father" : "",
  "desc" : "getMacroModuleResource",
  "name" : "getMacroModuleResource:function(modId)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getMyFavoriteModuleResource",
  "father" : "",
  "desc" : "getMyFavoriteModuleResource",
  "name" : "getMyFavoriteModuleResource:function(modId)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "locate",
  "father" : "",
  "desc" : "locate",
  "name" : "locate:function(id,type)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "openResource",
  "father" : "",
  "desc" : "openResource",
  "name" : "openResource:function(id,alias,type,cmd,useShadowTree)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "'getNodePath'",
  "father" : "",
  "desc" : "'getNodePath'",
  "name" : "'getNodePath':function()",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isDataQueryFolder",
  "father" : "",
  "desc" : "isDataQueryFolder",
  "name" : "isDataQueryFolder:function(id)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isDataset",
  "father" : "",
  "desc" : "isDataset",
  "name" : "isDataset:function(id)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "openAction",
  "father" : "",
  "desc" : "openAction",
  "name" : "openAction:function(id,alias,type,command,node,moduleid)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "open",
  "father" : "",
  "desc" : "open",
  "name" : "open:function(id,alias,type,cmd,useShadowTree,node)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getShadowTree",
  "father" : "",
  "desc" : "getShadowTree",
  "name" : "getShadowTree:function()",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "openModule",
  "father" : "",
  "desc" : "openModule",
  "name" : "openModule:function(moduleId,params)",
  "version" : "0",
  "path" : "bof.baseajax.common.searchHelper",
  "updateTime" : "2021-09-22 01:18:28",
  "isDelete" : 0
} ]