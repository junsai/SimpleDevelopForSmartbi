[ {
  "type" : 0,
  "keyword" : "initHandlers",
  "father" : "",
  "desc" : "initHandlers",
  "name" : "ReportExportPanel.prototype.initHandlers=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getAcceptHandler",
  "father" : "",
  "desc" : "getAcceptHandler",
  "name" : "ReportExportPanel.prototype.getAcceptHandler=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initReportExportDatas",
  "father" : "",
  "desc" : "initReportExportDatas",
  "name" : "ReportExportPanel.prototype.initReportExportDatas=function(taskInfo)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "translateInsightParams",
  "father" : "",
  "desc" : "translateInsightParams",
  "name" : "ReportExportPanel.prototype.translateInsightParams=function(reportParamSetting)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setParameterValue",
  "father" : "",
  "desc" : "setParameterValue",
  "name" : "ReportExportPanel.prototype.setParameterValue=function(reportParamSetting,byAlias)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getParamByAlias",
  "father" : "",
  "desc" : "getParamByAlias",
  "name" : "ReportExportPanel.prototype.getParamByAlias=function(alias)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initMailList",
  "father" : "",
  "desc" : "initMailList",
  "name" : "ReportExportPanel.prototype.initMailList=function(sendSetting)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initEnumParamComboBoxByTaskInfo",
  "father" : "",
  "desc" : "initEnumParamComboBoxByTaskInfo",
  "name" : "ReportExportPanel.prototype.initEnumParamComboBoxByTaskInfo=function(taskInfo)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initEnumParamComboBoxByParamInfo",
  "father" : "",
  "desc" : "initEnumParamComboBoxByParamInfo",
  "name" : "ReportExportPanel.prototype.initEnumParamComboBoxByParamInfo=function(paramInfo)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initDefaultParamComboBoxByTaskInfo",
  "father" : "",
  "desc" : "initDefaultParamComboBoxByTaskInfo",
  "name" : "ReportExportPanel.prototype.initDefaultParamComboBoxByTaskInfo=function(taskInfo)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initDefaultParamComboBoxByParamInfo",
  "father" : "",
  "desc" : "initDefaultParamComboBoxByParamInfo",
  "name" : "ReportExportPanel.prototype.initDefaultParamComboBoxByParamInfo=function(paramInfo)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getLinkageDefaultParamInfo",
  "father" : "",
  "desc" : "getLinkageDefaultParamInfo",
  "name" : "ReportExportPanel.prototype.getLinkageDefaultParamInfo=function(paramInfo)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getLinkageParamInfo",
  "father" : "",
  "desc" : "getLinkageParamInfo",
  "name" : "ReportExportPanel.prototype.getLinkageParamInfo=function(paramInfo)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateParameterPanel",
  "father" : "",
  "desc" : "updateParameterPanel",
  "name" : "ReportExportPanel.prototype.updateParameterPanel=function(enumIds,defaultIds,isEnumValues)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doDefaultComboBoxChanged",
  "father" : "",
  "desc" : "doDefaultComboBoxChanged",
  "name" : "ReportExportPanel.prototype.doDefaultComboBoxChanged=function(box,oldIds,ids)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doEnumComboBoxChanged",
  "father" : "",
  "desc" : "doEnumComboBoxChanged",
  "name" : "ReportExportPanel.prototype.doEnumComboBoxChanged=function(box,oldIds,ids)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hide",
  "father" : "",
  "desc" : "hide",
  "name" : "ReportExportPanel.prototype.hide=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "show",
  "father" : "",
  "desc" : "show",
  "name" : "ReportExportPanel.prototype.show=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "exportReportTypeChange",
  "father" : "",
  "desc" : "exportReportTypeChange",
  "name" : "ReportExportPanel.prototype.exportReportTypeChange=function(obj,selectedId,selectValue,dropdownBoxSelectedId,dropdownBoxSelectedValue)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "paramSetTypeChange",
  "father" : "",
  "desc" : "paramSetTypeChange",
  "name" : "ReportExportPanel.prototype.paramSetTypeChange=function(obj,selectedId,selectValue,dropdownBoxSelectedId,dropdownBoxSelectedValue)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showEnumTr",
  "father" : "",
  "desc" : "showEnumTr",
  "name" : "ReportExportPanel.prototype.showEnumTr=function(flag)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showParamSettingTr",
  "father" : "",
  "desc" : "showParamSettingTr",
  "name" : "ReportExportPanel.prototype.showParamSettingTr=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showExportSettingTr",
  "father" : "",
  "desc" : "showExportSettingTr",
  "name" : "ReportExportPanel.prototype.showExportSettingTr=function(exportType,sendType)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initBox",
  "father" : "",
  "desc" : "initBox",
  "name" : "ReportExportPanel.prototype.initBox=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateExportFormula",
  "father" : "",
  "desc" : "updateExportFormula",
  "name" : "ReportExportPanel.prototype.updateExportFormula=function(combo,selectedId,selectValue)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkAddSelected",
  "father" : "",
  "desc" : "checkAddSelected",
  "name" : "ReportExportPanel.prototype.checkAddSelected=function(combo)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkAlert",
  "father" : "",
  "desc" : "checkAlert",
  "name" : "ReportExportPanel.prototype.checkAlert=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemSendType_change_handler",
  "father" : "",
  "desc" : "elemSendType_change_handler",
  "name" : "ReportExportPanel.prototype.elemSendType_change_handler=function(obj,selectedId,selectValue,dropdownBoxSelectedId,dropdownBoxSelectedValue)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "ReportExportPanel.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBnSelectReport_click_handler",
  "father" : "",
  "desc" : "elemBnSelectReport_click_handler",
  "name" : "ReportExportPanel.prototype.elemBnSelectReport_click_handler=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemSelectDir_click_handler",
  "father" : "",
  "desc" : "elemSelectDir_click_handler",
  "name" : "ReportExportPanel.prototype.elemSelectDir_click_handler=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "replaceDirPath",
  "father" : "",
  "desc" : "replaceDirPath",
  "name" : "ReportExportPanel.prototype.replaceDirPath=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "callBackReportDirSelected",
  "father" : "",
  "desc" : "callBackReportDirSelected",
  "name" : "ReportExportPanel.prototype.callBackReportDirSelected=function(openType,list)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "callBackReportSelected",
  "father" : "",
  "desc" : "callBackReportSelected",
  "name" : "ReportExportPanel.prototype.callBackReportSelected=function(openType,list)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setControlByReportType",
  "father" : "",
  "desc" : "setControlByReportType",
  "name" : "ReportExportPanel.prototype.setControlByReportType=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroyParameterPanel",
  "father" : "",
  "desc" : "destroyParameterPanel",
  "name" : "ReportExportPanel.prototype.destroyParameterPanel=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initParameterPanel",
  "father" : "",
  "desc" : "initParameterPanel",
  "name" : "ReportExportPanel.prototype.initParameterPanel=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "confirmParamTitleVisible",
  "father" : "",
  "desc" : "confirmParamTitleVisible",
  "name" : "ReportExportPanel.prototype.confirmParamTitleVisible=function(params)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setTimeIntervalType",
  "father" : "",
  "desc" : "setTimeIntervalType",
  "name" : "ReportExportPanel.prototype.setTimeIntervalType=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doEditChange",
  "father" : "",
  "desc" : "doEditChange",
  "name" : "ReportExportPanel.prototype.doEditChange=function(paramPanel,combo,param,value)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doSelectChange",
  "father" : "",
  "desc" : "doSelectChange",
  "name" : "ReportExportPanel.prototype.doSelectChange=function(paramPanel,param,oldId,newId,oldValue,newValue)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setMetricReportDefaultValue",
  "father" : "",
  "desc" : "setMetricReportDefaultValue",
  "name" : "ReportExportPanel.prototype.setMetricReportDefaultValue=function(outputParam,param,controller)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkedValue",
  "father" : "",
  "desc" : "checkedValue",
  "name" : "ReportExportPanel.prototype.checkedValue=function(combo,value,display)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "valueChecked",
  "father" : "",
  "desc" : "valueChecked",
  "name" : "ReportExportPanel.prototype.valueChecked=function(comBoxData,value,display)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setParamValue",
  "father" : "",
  "desc" : "setParamValue",
  "name" : "ReportExportPanel.prototype.setParamValue=function(paramId,paramValue,paramDisplayValue)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getParamDefaultValue",
  "father" : "",
  "desc" : "getParamDefaultValue",
  "name" : "ReportExportPanel.prototype.getParamDefaultValue=function(paramId)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getParamStandbyValue",
  "father" : "",
  "desc" : "getParamStandbyValue",
  "name" : "ReportExportPanel.prototype.getParamStandbyValue=function(paramId)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setSimpleReportDefaultValue",
  "father" : "",
  "desc" : "setSimpleReportDefaultValue",
  "name" : "ReportExportPanel.prototype.setSimpleReportDefaultValue=function(parameters)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setDashboardDefaultValue",
  "father" : "",
  "desc" : "setDashboardDefaultValue",
  "name" : "ReportExportPanel.prototype.setDashboardDefaultValue=function(parameters)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setParamDefaultValue",
  "father" : "",
  "desc" : "setParamDefaultValue",
  "name" : "ReportExportPanel.prototype.setParamDefaultValue=function(param,onlyClient)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initExportReportType",
  "father" : "",
  "desc" : "initExportReportType",
  "name" : "ReportExportPanel.prototype.initExportReportType=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initExportSheetIndexes",
  "father" : "",
  "desc" : "initExportSheetIndexes",
  "name" : "ReportExportPanel.prototype.initExportSheetIndexes=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initSendType",
  "father" : "",
  "desc" : "initSendType",
  "name" : "ReportExportPanel.prototype.initSendType=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initParamSetType",
  "father" : "",
  "desc" : "initParamSetType",
  "name" : "ReportExportPanel.prototype.initParamSetType=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemAddressee_blur_handler",
  "father" : "",
  "desc" : "elemAddressee_blur_handler",
  "name" : "ReportExportPanel.prototype.elemAddressee_blur_handler=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemCc_blur_handler",
  "father" : "",
  "desc" : "elemCc_blur_handler",
  "name" : "ReportExportPanel.prototype.elemCc_blur_handler=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBcc_blur_handler",
  "father" : "",
  "desc" : "elemBcc_blur_handler",
  "name" : "ReportExportPanel.prototype.elemBcc_blur_handler=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkEmailFormat",
  "father" : "",
  "desc" : "checkEmailFormat",
  "name" : "ReportExportPanel.prototype.checkEmailFormat=function(str)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemFilePath_blur_handler",
  "father" : "",
  "desc" : "elemFilePath_blur_handler",
  "name" : "ReportExportPanel.prototype.elemFilePath_blur_handler=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemFileName_blur_handler",
  "father" : "",
  "desc" : "elemFileName_blur_handler",
  "name" : "ReportExportPanel.prototype.elemFileName_blur_handler=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkFileName",
  "father" : "",
  "desc" : "checkFileName",
  "name" : "ReportExportPanel.prototype.checkFileName=function(doAlert,selectValue,resetSuffix)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "trimValue",
  "father" : "",
  "desc" : "trimValue",
  "name" : "ReportExportPanel.prototype.trimValue=function(value)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkFilePath",
  "father" : "",
  "desc" : "checkFilePath",
  "name" : "ReportExportPanel.prototype.checkFilePath=function(str)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getParameters",
  "father" : "",
  "desc" : "getParameters",
  "name" : "ReportExportPanel.prototype.getParameters=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fillFreeReportParam",
  "father" : "",
  "desc" : "fillFreeReportParam",
  "name" : "ReportExportPanel.prototype.fillFreeReportParam=function(paramArray,param)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setFilePathSetting",
  "father" : "",
  "desc" : "setFilePathSetting",
  "name" : "ReportExportPanel.prototype.setFilePathSetting=function(sendSetting)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getFilePathSetting",
  "father" : "",
  "desc" : "getFilePathSetting",
  "name" : "ReportExportPanel.prototype.getFilePathSetting=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSendSetting",
  "father" : "",
  "desc" : "getSendSetting",
  "name" : "ReportExportPanel.prototype.getSendSetting=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getExportSetting",
  "father" : "",
  "desc" : "getExportSetting",
  "name" : "ReportExportPanel.prototype.getExportSetting=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getEveryParam",
  "father" : "",
  "desc" : "getEveryParam",
  "name" : "ReportExportPanel.prototype.getEveryParam=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "closeReport",
  "father" : "",
  "desc" : "closeReport",
  "name" : "ReportExportPanel.prototype.closeReport=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initOtherInfo",
  "father" : "",
  "desc" : "initOtherInfo",
  "name" : "ReportExportPanel.prototype.initOtherInfo=function(sendType,sendSetting)",
  "version" : "0",
  "path" : "bof.schedule.panel.ReportExportPanel",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]