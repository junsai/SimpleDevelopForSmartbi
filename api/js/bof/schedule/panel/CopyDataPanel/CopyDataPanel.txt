[ {
  "type" : 0,
  "keyword" : "initCopyDatas",
  "father" : "",
  "desc" : "initCopyDatas",
  "name" : "CopyDataPanel.prototype.initCopyDatas=function(taskInfo)",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initBox",
  "father" : "",
  "desc" : "initBox",
  "name" : "CopyDataPanel.prototype.initBox=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "schemas_change_handler",
  "father" : "",
  "desc" : "schemas_change_handler",
  "name" : "CopyDataPanel.prototype.schemas_change_handler=function(obj,selectedId,selectValue,dropdownBoxSelectedId,dropdownBoxSelectedValue)",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hide",
  "father" : "",
  "desc" : "hide",
  "name" : "CopyDataPanel.prototype.hide=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "show",
  "father" : "",
  "desc" : "show",
  "name" : "CopyDataPanel.prototype.show=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBnDataSource_click_handler",
  "father" : "",
  "desc" : "elemBnDataSource_click_handler",
  "name" : "CopyDataPanel.prototype.elemBnDataSource_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "callBackDataSourceValue",
  "father" : "",
  "desc" : "callBackDataSourceValue",
  "name" : "CopyDataPanel.prototype.callBackDataSourceValue=function(info)",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDataSourceName",
  "father" : "",
  "desc" : "getDataSourceName",
  "name" : "CopyDataPanel.prototype.getDataSourceName=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "loadschemas",
  "father" : "",
  "desc" : "loadschemas",
  "name" : "CopyDataPanel.prototype.loadschemas=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initTableList",
  "father" : "",
  "desc" : "initTableList",
  "name" : "CopyDataPanel.prototype.initTableList=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "CopyDataPanel.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.schedule.panel.CopyDataPanel",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]