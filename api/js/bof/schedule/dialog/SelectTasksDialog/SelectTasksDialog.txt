[ {
  "type" : 0,
  "keyword" : "initTaskTree",
  "father" : "",
  "desc" : "initTaskTree",
  "name" : "SelectTasksDialog.prototype.initTaskTree=function(container)",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnSelectNode",
  "father" : "",
  "desc" : "doOnSelectNode",
  "name" : "SelectTasksDialog.prototype.doOnSelectNode=function(tree,newNode,oldNode)",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnRowDblClick",
  "father" : "",
  "desc" : "doOnRowDblClick",
  "name" : "SelectTasksDialog.prototype.doOnRowDblClick=function(tr)",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnSelectRow",
  "father" : "",
  "desc" : "doOnSelectRow",
  "name" : "SelectTasksDialog.prototype.doOnSelectRow=function(trList)",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doTreeDblClick",
  "father" : "",
  "desc" : "doTreeDblClick",
  "name" : "SelectTasksDialog.prototype.doTreeDblClick=function(tree,node)",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "SelectTasksDialog.prototype.init=function(parent,data,fn,obj)",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getTreeContainer",
  "father" : "",
  "desc" : "getTreeContainer",
  "name" : "SelectTasksDialog.prototype.getTreeContainer=function(contentBanner)",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertSearchBar",
  "father" : "",
  "desc" : "insertSearchBar",
  "name" : "SelectTasksDialog.prototype.insertSearchBar=function(contentBanner)",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleTreeDblClick",
  "father" : "",
  "desc" : "handleTreeDblClick",
  "name" : "SelectTasksDialog.prototype.handleTreeDblClick=function(tree,node)",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAdd",
  "father" : "",
  "desc" : "doAdd",
  "name" : "SelectTasksDialog.prototype.doAdd=function()",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRemove",
  "father" : "",
  "desc" : "doRemove",
  "name" : "SelectTasksDialog.prototype.doRemove=function()",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "SelectTasksDialog.prototype.doClose=function()",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "SelectTasksDialog.prototype.doOK=function()",
  "version" : "0",
  "path" : "bof.schedule.dialog.SelectTasksDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]