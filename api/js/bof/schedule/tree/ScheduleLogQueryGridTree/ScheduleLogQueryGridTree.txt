[ {
  "type" : 0,
  "keyword" : "createGridTreeNode",
  "father" : "",
  "desc" : "createGridTreeNode",
  "name" : "ScheduleLogQueryGridTree.prototype.createGridTreeNode=function(text,level,mayHasChild,id)",
  "version" : "0",
  "path" : "bof.schedule.tree.ScheduleLogQueryGridTree",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "render",
  "father" : "",
  "desc" : "render",
  "name" : "ScheduleLogQueryGridTree.prototype.render=function(planType,scheduleType,startTime,endTime,\t\tscheduleName, taskName, pageRows) ",
  "version" : "0",
  "path" : "bof.schedule.tree.ScheduleLogQueryGridTree",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refresh",
  "father" : "",
  "desc" : "refresh",
  "name" : "ScheduleLogQueryGridTree.prototype.refresh=function(pageNum,pageRows)",
  "version" : "0",
  "path" : "bof.schedule.tree.ScheduleLogQueryGridTree",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addScheduleNode",
  "father" : "",
  "desc" : "addScheduleNode",
  "name" : "ScheduleLogQueryGridTree.prototype.addScheduleNode=function(scheduleNodeInfo)",
  "version" : "0",
  "path" : "bof.schedule.tree.ScheduleLogQueryGridTree",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "renderColumnFunc",
  "father" : "",
  "desc" : "renderColumnFunc",
  "name" : "ScheduleLogQueryGridTree.prototype.renderColumnFunc=function(tdEl,colIndex,treeNode)",
  "version" : "0",
  "path" : "bof.schedule.tree.ScheduleLogQueryGridTree",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]