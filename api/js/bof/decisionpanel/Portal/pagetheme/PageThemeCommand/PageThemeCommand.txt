[ {
  "type" : 0,
  "keyword" : "execute",
  "father" : "",
  "desc" : "execute",
  "name" : "PageThemeCommand.prototype.execute=function(action,node)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagetheme.PageThemeCommand",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "close",
  "father" : "",
  "desc" : "close",
  "name" : "PageThemeCommand.prototype.close=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagetheme.PageThemeCommand",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "confirmClose",
  "father" : "",
  "desc" : "confirmClose",
  "name" : "PageThemeCommand.prototype.confirmClose=function(func)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagetheme.PageThemeCommand",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "PageThemeCommand.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagetheme.PageThemeCommand",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnClose",
  "father" : "",
  "desc" : "doOnClose",
  "name" : "PageThemeCommand.prototype.doOnClose=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagetheme.PageThemeCommand",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnNeedRefresh",
  "father" : "",
  "desc" : "doOnNeedRefresh",
  "name" : "PageThemeCommand.prototype.doOnNeedRefresh=function(view,id)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagetheme.PageThemeCommand",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createPageTheme",
  "father" : "",
  "desc" : "createPageTheme",
  "name" : "PageThemeCommand.prototype.createPageTheme=function(node)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagetheme.PageThemeCommand",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "editPageTheme",
  "father" : "",
  "desc" : "editPageTheme",
  "name" : "PageThemeCommand.prototype.editPageTheme=function(node)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagetheme.PageThemeCommand",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
} ]