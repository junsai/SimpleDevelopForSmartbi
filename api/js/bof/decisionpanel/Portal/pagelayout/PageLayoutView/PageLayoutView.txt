[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "PageLayoutView.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "PageLayoutView.prototype.getValue=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refresh",
  "father" : "",
  "desc" : "refresh",
  "name" : "PageLayoutView.prototype.refresh=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDefaultLayout",
  "father" : "",
  "desc" : "getDefaultLayout",
  "name" : "PageLayoutView.prototype.getDefaultLayout=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDefaultValue",
  "father" : "",
  "desc" : "getDefaultValue",
  "name" : "PageLayoutView.prototype.getDefaultValue=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fill",
  "father" : "",
  "desc" : "fill",
  "name" : "PageLayoutView.prototype.fill=function(bean)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setNameDisabled",
  "father" : "",
  "desc" : "setNameDisabled",
  "name" : "PageLayoutView.prototype.setNameDisabled=function(flag)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isDirty",
  "father" : "",
  "desc" : "isDirty",
  "name" : "PageLayoutView.prototype.isDirty=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshButtonStatus",
  "father" : "",
  "desc" : "refreshButtonStatus",
  "name" : "PageLayoutView.prototype.refreshButtonStatus=function(ev)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "reset",
  "father" : "",
  "desc" : "reset",
  "name" : "PageLayoutView.prototype.reset=function(bean)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkElemValid",
  "father" : "",
  "desc" : "checkElemValid",
  "name" : "PageLayoutView.prototype.checkElemValid=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setCurrentLocation",
  "father" : "",
  "desc" : "setCurrentLocation",
  "name" : "PageLayoutView.prototype.setCurrentLocation=function(currentLocation)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRefresh",
  "father" : "",
  "desc" : "doRefresh",
  "name" : "PageLayoutView.prototype.doRefresh=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBtnHtmlLayoutTip_click_handler",
  "father" : "",
  "desc" : "elemBtnHtmlLayoutTip_click_handler",
  "name" : "PageLayoutView.prototype.elemBtnHtmlLayoutTip_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBtnDownload_click_handler",
  "father" : "",
  "desc" : "elemBtnDownload_click_handler",
  "name" : "PageLayoutView.prototype.elemBtnDownload_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemUploadFile_change_handler",
  "father" : "",
  "desc" : "elemUploadFile_change_handler",
  "name" : "PageLayoutView.prototype.elemUploadFile_change_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.pagelayout.PageLayoutView",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
} ]