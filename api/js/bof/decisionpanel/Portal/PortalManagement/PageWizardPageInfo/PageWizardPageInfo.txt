[ {
  "type" : 0,
  "keyword" : "createFormatEditor",
  "father" : "",
  "desc" : "createFormatEditor",
  "name" : "PageWizardPageInfo.prototype.createFormatEditor=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "PageWizardPageInfo.prototype.getValue=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDefaultValue",
  "father" : "",
  "desc" : "getDefaultValue",
  "name" : "PageWizardPageInfo.prototype.getDefaultValue=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fill",
  "father" : "",
  "desc" : "fill",
  "name" : "PageWizardPageInfo.prototype.fill=function(bean)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isDirty",
  "father" : "",
  "desc" : "isDirty",
  "name" : "PageWizardPageInfo.prototype.isDirty=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkValid",
  "father" : "",
  "desc" : "checkValid",
  "name" : "PageWizardPageInfo.prototype.checkValid=function(parentId)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkNameANDAlias",
  "father" : "",
  "desc" : "checkNameANDAlias",
  "name" : "PageWizardPageInfo.prototype.checkNameANDAlias=function(parentId)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "trim",
  "father" : "",
  "desc" : "trim",
  "name" : "PageWizardPageInfo.prototype.trim=function(inputString)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshButtonStatus",
  "father" : "",
  "desc" : "refreshButtonStatus",
  "name" : "PageWizardPageInfo.prototype.refreshButtonStatus=function(ev)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemRadio1_click_handler",
  "father" : "",
  "desc" : "elemRadio1_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemRadio1_click_handler=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemRadio2_click_handler",
  "father" : "",
  "desc" : "elemRadio2_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemRadio2_click_handler=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemRadio3_click_handler",
  "father" : "",
  "desc" : "elemRadio3_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemRadio3_click_handler=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemSetGroupId_click_handler",
  "father" : "",
  "desc" : "elemSetGroupId_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemSetGroupId_click_handler=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setGroupId",
  "father" : "",
  "desc" : "setGroupId",
  "name" : "PageWizardPageInfo.prototype.setGroupId=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "PageWizardPageInfo.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setGroupInfo",
  "father" : "",
  "desc" : "setGroupInfo",
  "name" : "PageWizardPageInfo.prototype.setGroupInfo=function(groupInfo)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setGroupConfigDisplay",
  "father" : "",
  "desc" : "setGroupConfigDisplay",
  "name" : "PageWizardPageInfo.prototype.setGroupConfigDisplay=function(display)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemScreenShotUpload_click_handler",
  "father" : "",
  "desc" : "elemScreenShotUpload_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemScreenShotUpload_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validScreenShotFile",
  "father" : "",
  "desc" : "validScreenShotFile",
  "name" : "PageWizardPageInfo.prototype.validScreenShotFile=function(filePath)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setScreenShot",
  "father" : "",
  "desc" : "setScreenShot",
  "name" : "PageWizardPageInfo.prototype.setScreenShot=function(screenShot)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemScreenshotName_click_handler",
  "father" : "",
  "desc" : "elemScreenshotName_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemScreenshotName_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBgImageUpload_click_handler",
  "father" : "",
  "desc" : "elemBgImageUpload_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemBgImageUpload_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setUploadImage",
  "father" : "",
  "desc" : "setUploadImage",
  "name" : "PageWizardPageInfo.prototype.setUploadImage=function(bean)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initPageTheme",
  "father" : "",
  "desc" : "initPageTheme",
  "name" : "PageWizardPageInfo.prototype.initPageTheme=function(bean)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initIPadPageTheme",
  "father" : "",
  "desc" : "initIPadPageTheme",
  "name" : "PageWizardPageInfo.prototype.initIPadPageTheme=function(bean)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initPageThemeComboBox",
  "father" : "",
  "desc" : "initPageThemeComboBox",
  "name" : "PageWizardPageInfo.prototype.initPageThemeComboBox=function(pDiv,selectPageThemeId)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemNoChild_click_handler",
  "father" : "",
  "desc" : "elemNoChild_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemNoChild_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemHasChild_click_handler",
  "father" : "",
  "desc" : "elemHasChild_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemHasChild_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fillPageEx",
  "father" : "",
  "desc" : "fillPageEx",
  "name" : "PageWizardPageInfo.prototype.fillPageEx=function(bean,menuLevel,parentMenuStyle)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getPageEx",
  "father" : "",
  "desc" : "getPageEx",
  "name" : "PageWizardPageInfo.prototype.getPageEx=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemDeleteScreenshot_click_handler",
  "father" : "",
  "desc" : "elemDeleteScreenshot_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemDeleteScreenshot_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemDeleteBgImage_click_handler",
  "father" : "",
  "desc" : "elemDeleteBgImage_click_handler",
  "name" : "PageWizardPageInfo.prototype.elemDeleteBgImage_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.decisionpanel.Portal.PortalManagement.PageWizardPageInfo",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
} ]