[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "DashboardSettingDialog.prototype.init=function(parent,data,fn,obj)",
  "version" : "0",
  "path" : "bof.decisionpanel.dashboard.DashboardSettingDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initReportHeader",
  "father" : "",
  "desc" : "initReportHeader",
  "name" : "DashboardSettingDialog.prototype.initReportHeader=function(reportHeader)",
  "version" : "0",
  "path" : "bof.decisionpanel.dashboard.DashboardSettingDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initReportTail",
  "father" : "",
  "desc" : "initReportTail",
  "name" : "DashboardSettingDialog.prototype.initReportTail=function(reportTail)",
  "version" : "0",
  "path" : "bof.decisionpanel.dashboard.DashboardSettingDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "DashboardSettingDialog.prototype.doClose=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.dashboard.DashboardSettingDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "DashboardSettingDialog.prototype.doOK=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.dashboard.DashboardSettingDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroyReportHeader",
  "father" : "",
  "desc" : "destroyReportHeader",
  "name" : "DashboardSettingDialog.prototype.destroyReportHeader=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.dashboard.DashboardSettingDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroyReportTail",
  "father" : "",
  "desc" : "destroyReportTail",
  "name" : "DashboardSettingDialog.prototype.destroyReportTail=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.dashboard.DashboardSettingDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "DashboardSettingDialog.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.decisionpanel.dashboard.DashboardSettingDialog",
  "updateTime" : "2021-09-22 01:18:29",
  "isDelete" : 0
} ]