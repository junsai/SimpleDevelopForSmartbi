[ {
  "type" : 0,
  "keyword" : "initFirstLevelMembers",
  "father" : "",
  "desc" : "initFirstLevelMembers",
  "name" : "HierarchyMemberWithCustomTreeNode.prototype.initFirstLevelMembers=function()",
  "version" : "0",
  "path" : "bof.olapquery.tree.HierarchyMemberWithCustomTreeNode",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initGlobalNamedSet",
  "father" : "",
  "desc" : "initGlobalNamedSet",
  "name" : "HierarchyMemberWithCustomTreeNode.prototype.initGlobalNamedSet=function()",
  "version" : "0",
  "path" : "bof.olapquery.tree.HierarchyMemberWithCustomTreeNode",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initLocalNamedSets",
  "father" : "",
  "desc" : "initLocalNamedSets",
  "name" : "HierarchyMemberWithCustomTreeNode.prototype.initLocalNamedSets=function()",
  "version" : "0",
  "path" : "bof.olapquery.tree.HierarchyMemberWithCustomTreeNode",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initMemberChildrenRemoteCallback",
  "father" : "",
  "desc" : "initMemberChildrenRemoteCallback",
  "name" : "HierarchyMemberWithCustomTreeNode.prototype.initMemberChildrenRemoteCallback=function(result)",
  "version" : "0",
  "path" : "bof.olapquery.tree.HierarchyMemberWithCustomTreeNode",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initHierarchyRootMembersCallback",
  "father" : "",
  "desc" : "initHierarchyRootMembersCallback",
  "name" : "HierarchyMemberWithCustomTreeNode.prototype.initHierarchyRootMembersCallback=function(result)",
  "version" : "0",
  "path" : "bof.olapquery.tree.HierarchyMemberWithCustomTreeNode",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initMemberChildren",
  "father" : "",
  "desc" : "initMemberChildren",
  "name" : "HierarchyMemberWithCustomTreeNode.prototype.initMemberChildren=function()",
  "version" : "0",
  "path" : "bof.olapquery.tree.HierarchyMemberWithCustomTreeNode",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initRemoteCallback",
  "father" : "",
  "desc" : "initRemoteCallback",
  "name" : "HierarchyMemberWithCustomTreeNode.prototype.initRemoteCallback=function(ret,type,isMemberHasChildren)",
  "version" : "0",
  "path" : "bof.olapquery.tree.HierarchyMemberWithCustomTreeNode",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]