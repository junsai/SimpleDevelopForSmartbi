[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "ParamAdvancedSearchDialog.prototype.init=function(parent,param,fn,obj)",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initLevelInfo",
  "father" : "",
  "desc" : "initLevelInfo",
  "name" : "ParamAdvancedSearchDialog.prototype.initLevelInfo=function()",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemMEMBER_click_handler",
  "father" : "",
  "desc" : "elemMEMBER_click_handler",
  "name" : "ParamAdvancedSearchDialog.prototype.elemMEMBER_click_handler=function(ev)",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemATTRIBUTE_click_handler",
  "father" : "",
  "desc" : "elemATTRIBUTE_click_handler",
  "name" : "ParamAdvancedSearchDialog.prototype.elemATTRIBUTE_click_handler=function(ev)",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getFilter",
  "father" : "",
  "desc" : "getFilter",
  "name" : "ParamAdvancedSearchDialog.prototype.getFilter=function()",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refresh",
  "father" : "",
  "desc" : "refresh",
  "name" : "ParamAdvancedSearchDialog.prototype.refresh=function()",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeEditor",
  "father" : "",
  "desc" : "changeEditor",
  "name" : "ParamAdvancedSearchDialog.prototype.changeEditor=function(selectedOperatorType)",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getFilterType",
  "father" : "",
  "desc" : "getFilterType",
  "name" : "ParamAdvancedSearchDialog.prototype.getFilterType=function()",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initLevelComboBox",
  "father" : "",
  "desc" : "initLevelComboBox",
  "name" : "ParamAdvancedSearchDialog.prototype.initLevelComboBox=function()",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "selectLevel",
  "father" : "",
  "desc" : "selectLevel",
  "name" : "ParamAdvancedSearchDialog.prototype.selectLevel=function(obj,oldId,newId,        oldName, newName)",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initLevelProperties",
  "father" : "",
  "desc" : "initLevelProperties",
  "name" : "ParamAdvancedSearchDialog.prototype.initLevelProperties=function()",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "selectLevelProperty",
  "father" : "",
  "desc" : "selectLevelProperty",
  "name" : "ParamAdvancedSearchDialog.prototype.selectLevelProperty=function(obj,oldId,newId,        oldName, newName)",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "selectOperatorType",
  "father" : "",
  "desc" : "selectOperatorType",
  "name" : "ParamAdvancedSearchDialog.prototype.selectOperatorType=function(obj,oldId,newId,        oldName, newName)",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initOperatorTypes",
  "father" : "",
  "desc" : "initOperatorTypes",
  "name" : "ParamAdvancedSearchDialog.prototype.initOperatorTypes=function(operatorTypeComboBox,selectArray)",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "ParamAdvancedSearchDialog.prototype.doOK=function(e)",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "ParamAdvancedSearchDialog.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.olapquery.param.ParamAdvancedSearchDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]