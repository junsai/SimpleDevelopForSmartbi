[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "SubtotalSettingDialog.prototype.init=function(parent,data,fn,obj)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "SubtotalSettingDialog.prototype.doClose=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOK",
  "father" : "",
  "desc" : "doOK",
  "name" : "SubtotalSettingDialog.prototype.doOK=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "SubtotalSettingDialog.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnTypeChange",
  "father" : "",
  "desc" : "doOnTypeChange",
  "name" : "SubtotalSettingDialog.prototype.doOnTypeChange=function(combox,oldId,newId)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertTableRow",
  "father" : "",
  "desc" : "insertTableRow",
  "name" : "SubtotalSettingDialog.prototype.insertTableRow=function(params)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemAddBtn_click_handler",
  "father" : "",
  "desc" : "elemAddBtn_click_handler",
  "name" : "SubtotalSettingDialog.prototype.elemAddBtn_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "delClickHandler",
  "father" : "",
  "desc" : "delClickHandler",
  "name" : "SubtotalSettingDialog.prototype.delClickHandler=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "insertBlankRow",
  "father" : "",
  "desc" : "insertBlankRow",
  "name" : "SubtotalSettingDialog.prototype.insertBlankRow=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hierarchyChange",
  "father" : "",
  "desc" : "hierarchyChange",
  "name" : "SubtotalSettingDialog.prototype.hierarchyChange=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "deleteSubTotalByRowId",
  "father" : "",
  "desc" : "deleteSubTotalByRowId",
  "name" : "SubtotalSettingDialog.prototype.deleteSubTotalByRowId=function(rowId)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroySubtotalInfo",
  "father" : "",
  "desc" : "destroySubtotalInfo",
  "name" : "SubtotalSettingDialog.prototype.destroySubtotalInfo=function(obj)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClearBtnClick",
  "father" : "",
  "desc" : "doClearBtnClick",
  "name" : "SubtotalSettingDialog.prototype.doClearBtnClick=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValueTypeList",
  "father" : "",
  "desc" : "getValueTypeList",
  "name" : "SubtotalSettingDialog.prototype.getValueTypeList=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getFormatList",
  "father" : "",
  "desc" : "getFormatList",
  "name" : "SubtotalSettingDialog.prototype.getFormatList=function(type)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doValueTypeChange",
  "father" : "",
  "desc" : "doValueTypeChange",
  "name" : "SubtotalSettingDialog.prototype.doValueTypeChange=function(sender,oldId,newId,oldValue,newValue)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.SubtotalSettingDialog",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]