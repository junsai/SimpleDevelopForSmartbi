[ {
  "type" : 0,
  "keyword" : "render",
  "father" : "",
  "desc" : "render",
  "name" : "AdvancedSettingTree.prototype.render=function(cubeId,olapClientId)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "AdvancedSettingTree.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createTreeNode",
  "father" : "",
  "desc" : "createTreeNode",
  "name" : "AdvancedSettingTree.prototype.createTreeNode=function(text,level,mayHasChild,parent)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getMenuHandlers",
  "father" : "",
  "desc" : "getMenuHandlers",
  "name" : "AdvancedSettingTree.prototype.getMenuHandlers=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshHierarchyFolder",
  "father" : "",
  "desc" : "refreshHierarchyFolder",
  "name" : "AdvancedSettingTree.prototype.refreshHierarchyFolder=function(ret)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshCustomMemberFolder",
  "father" : "",
  "desc" : "refreshCustomMemberFolder",
  "name" : "AdvancedSettingTree.prototype.refreshCustomMemberFolder=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshParamFolder",
  "father" : "",
  "desc" : "refreshParamFolder",
  "name" : "AdvancedSettingTree.prototype.refreshParamFolder=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "canDrag",
  "father" : "",
  "desc" : "canDrag",
  "name" : "AdvancedSettingTree.prototype.canDrag=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setEnabled",
  "father" : "",
  "desc" : "setEnabled",
  "name" : "AdvancedSettingTree.prototype.setEnabled=function(enabled)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getEnabled",
  "father" : "",
  "desc" : "getEnabled",
  "name" : "AdvancedSettingTree.prototype.getEnabled=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnClick",
  "father" : "",
  "desc" : "doOnClick",
  "name" : "AdvancedSettingTree.prototype.doOnClick=function(e)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setEditType",
  "father" : "",
  "desc" : "setEditType",
  "name" : "AdvancedSettingTree.prototype.setEditType=function(type)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doRefreshParam",
  "father" : "",
  "desc" : "doRefreshParam",
  "name" : "AdvancedSettingTree.prototype.doRefreshParam=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "synHierarchy",
  "father" : "",
  "desc" : "synHierarchy",
  "name" : "AdvancedSettingTree.prototype.synHierarchy=function(axis,hierarchyInt)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.advancedsetting.AdvancedSettingTree",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]