[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "OlapQueryViewAdapter.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewAdapter",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getParamValues",
  "father" : "",
  "desc" : "getParamValues",
  "name" : "OlapQueryViewAdapter.prototype.getParamValues=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewAdapter",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "open",
  "father" : "",
  "desc" : "open",
  "name" : "OlapQueryViewAdapter.prototype.open=function(action,dataSourceId,resId,resType,showToolbar,paramsInfoStr,\t\t\trefresh, showShortToolbar,shortToolbarAlign, hideToolbarItems, openByParamValueSetting, selectedNodeName, selectedNodeId)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewAdapter",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "afterOpenOlapQueryView",
  "father" : "",
  "desc" : "afterOpenOlapQueryView",
  "name" : "OlapQueryViewAdapter.prototype.afterOpenOlapQueryView=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewAdapter",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hideToolbarButton",
  "father" : "",
  "desc" : "hideToolbarButton",
  "name" : "OlapQueryViewAdapter.prototype.hideToolbarButton=function(hideToolbarItems)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewAdapter",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]