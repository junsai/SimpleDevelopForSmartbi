[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "OlapQueryViewHandler.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initMenu",
  "father" : "",
  "desc" : "initMenu",
  "name" : "OlapQueryViewHandler.prototype.initMenu=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getFunctionName",
  "father" : "",
  "desc" : "getFunctionName",
  "name" : "OlapQueryViewHandler.prototype.getFunctionName=function(funName)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setEditable",
  "father" : "",
  "desc" : "setEditable",
  "name" : "OlapQueryViewHandler.prototype.setEditable=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clearMenuState",
  "father" : "",
  "desc" : "clearMenuState",
  "name" : "OlapQueryViewHandler.prototype.clearMenuState=function()",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "resetMenuState",
  "father" : "",
  "desc" : "resetMenuState",
  "name" : "OlapQueryViewHandler.prototype.resetMenuState=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCmd",
  "father" : "",
  "desc" : "doCmd",
  "name" : "OlapQueryViewHandler.prototype.doCmd=function(node,func)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAfterNodeChange",
  "father" : "",
  "desc" : "doAfterNodeChange",
  "name" : "OlapQueryViewHandler.prototype.doAfterNodeChange=function(node,ok)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createCustomMemberFunction",
  "father" : "",
  "desc" : "createCustomMemberFunction",
  "name" : "OlapQueryViewHandler.prototype.createCustomMemberFunction=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateCustomMember",
  "father" : "",
  "desc" : "updateCustomMember",
  "name" : "OlapQueryViewHandler.prototype.updateCustomMember=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "deleteCustomMember",
  "father" : "",
  "desc" : "deleteCustomMember",
  "name" : "OlapQueryViewHandler.prototype.deleteCustomMember=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createNamedSetFunction",
  "father" : "",
  "desc" : "createNamedSetFunction",
  "name" : "OlapQueryViewHandler.prototype.createNamedSetFunction=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateNamedSet",
  "father" : "",
  "desc" : "updateNamedSet",
  "name" : "OlapQueryViewHandler.prototype.updateNamedSet=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "deleteNamedSet",
  "father" : "",
  "desc" : "deleteNamedSet",
  "name" : "OlapQueryViewHandler.prototype.deleteNamedSet=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshNode",
  "father" : "",
  "desc" : "refreshNode",
  "name" : "OlapQueryViewHandler.prototype.refreshNode=function(node,needRefresh)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "searchNode",
  "father" : "",
  "desc" : "searchNode",
  "name" : "OlapQueryViewHandler.prototype.searchNode=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "processSearchResult",
  "father" : "",
  "desc" : "processSearchResult",
  "name" : "OlapQueryViewHandler.prototype.processSearchResult=function(node,processType,selectMembersString)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeAlias",
  "father" : "",
  "desc" : "changeAlias",
  "name" : "OlapQueryViewHandler.prototype.changeAlias=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeGlobalCustomMemberAlias",
  "father" : "",
  "desc" : "changeGlobalCustomMemberAlias",
  "name" : "OlapQueryViewHandler.prototype.changeGlobalCustomMemberAlias=function(succeed,node,alias)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refreshNodeWithRebuildParam",
  "father" : "",
  "desc" : "refreshNodeWithRebuildParam",
  "name" : "OlapQueryViewHandler.prototype.refreshNodeWithRebuildParam=function(node,needRefresh)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "showProperty",
  "father" : "",
  "desc" : "showProperty",
  "name" : "OlapQueryViewHandler.prototype.showProperty=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.olapquery.OlapQueryViewHandler",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]