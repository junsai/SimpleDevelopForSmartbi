[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "OlapDataFontSizeSetting.prototype.init=function()",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontSizeSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "OlapDataFontSizeSetting.prototype.validate=function()",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontSizeSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "OlapDataFontSizeSetting.prototype.save=function()",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontSizeSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doInitOlapDataFontSizeSetting",
  "father" : "",
  "desc" : "doInitOlapDataFontSizeSetting",
  "name" : "OlapDataFontSizeSetting.prototype.doInitOlapDataFontSizeSetting=function()",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontSizeSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "OlapDataFontSizeSetting.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontSizeSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]