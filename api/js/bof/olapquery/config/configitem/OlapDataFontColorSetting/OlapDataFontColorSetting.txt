[ {
  "type" : 0,
  "keyword" : "init",
  "father" : "",
  "desc" : "init",
  "name" : "OlapDataFontColorSetting.prototype.init=function()",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontColorSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "OlapDataFontColorSetting.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontColorSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "validate",
  "father" : "",
  "desc" : "validate",
  "name" : "OlapDataFontColorSetting.prototype.validate=function()",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontColorSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "OlapDataFontColorSetting.prototype.save=function()",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontColorSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doInitOlapDataFontColorSetting",
  "father" : "",
  "desc" : "doInitOlapDataFontColorSetting",
  "name" : "OlapDataFontColorSetting.prototype.doInitOlapDataFontColorSetting=function()",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontColorSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "handleConfig",
  "father" : "",
  "desc" : "handleConfig",
  "name" : "OlapDataFontColorSetting.prototype.handleConfig=function(systemConfig)",
  "version" : "0",
  "path" : "bof.olapquery.config.configitem.OlapDataFontColorSetting",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]