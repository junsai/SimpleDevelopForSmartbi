[ {
  "type" : 0,
  "keyword" : "execute",
  "father" : "",
  "desc" : "execute",
  "name" : "MdxTemplateCommand.prototype.execute=function(action,node)",
  "version" : "0",
  "path" : "bof.olapquery.mdxtemplate.MdxTemplateCommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "close",
  "father" : "",
  "desc" : "close",
  "name" : "MdxTemplateCommand.prototype.close=function()",
  "version" : "0",
  "path" : "bof.olapquery.mdxtemplate.MdxTemplateCommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "confirmClose",
  "father" : "",
  "desc" : "confirmClose",
  "name" : "MdxTemplateCommand.prototype.confirmClose=function(func)",
  "version" : "0",
  "path" : "bof.olapquery.mdxtemplate.MdxTemplateCommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "MdxTemplateCommand.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.olapquery.mdxtemplate.MdxTemplateCommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnClose",
  "father" : "",
  "desc" : "doOnClose",
  "name" : "MdxTemplateCommand.prototype.doOnClose=function()",
  "version" : "0",
  "path" : "bof.olapquery.mdxtemplate.MdxTemplateCommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnNeedRefresh",
  "father" : "",
  "desc" : "doOnNeedRefresh",
  "name" : "MdxTemplateCommand.prototype.doOnNeedRefresh=function(view,id)",
  "version" : "0",
  "path" : "bof.olapquery.mdxtemplate.MdxTemplateCommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createMdxTemplate",
  "father" : "",
  "desc" : "createMdxTemplate",
  "name" : "MdxTemplateCommand.prototype.createMdxTemplate=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.mdxtemplate.MdxTemplateCommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "editMdxTemplate",
  "father" : "",
  "desc" : "editMdxTemplate",
  "name" : "MdxTemplateCommand.prototype.editMdxTemplate=function(node)",
  "version" : "0",
  "path" : "bof.olapquery.mdxtemplate.MdxTemplateCommand",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]