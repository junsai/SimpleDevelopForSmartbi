[ {
  "type" : 0,
  "keyword" : "open",
  "father" : "",
  "desc" : "open",
  "name" : "DataSourceCreateAction.prototype.open=function()",
  "version" : "0",
  "path" : "bof.olapquery.datasource.actions.DataSourceCreateAction",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "createView",
  "father" : "",
  "desc" : "createView",
  "name" : "DataSourceCreateAction.prototype.createView=function()",
  "version" : "0",
  "path" : "bof.olapquery.datasource.actions.DataSourceCreateAction",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "DataSourceCreateAction.prototype.save=function()",
  "version" : "0",
  "path" : "bof.olapquery.datasource.actions.DataSourceCreateAction",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreate",
  "father" : "",
  "desc" : "doCreate",
  "name" : "DataSourceCreateAction.prototype.doCreate=function(parentId)",
  "version" : "0",
  "path" : "bof.olapquery.datasource.actions.DataSourceCreateAction",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreateBefore1",
  "father" : "",
  "desc" : "doCreateBefore1",
  "name" : "DataSourceCreateAction.prototype.doCreateBefore1=function()",
  "version" : "0",
  "path" : "bof.olapquery.datasource.actions.DataSourceCreateAction",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCreateBefore2",
  "father" : "",
  "desc" : "doCreateBefore2",
  "name" : "DataSourceCreateAction.prototype.doCreateBefore2=function(info,force)",
  "version" : "0",
  "path" : "bof.olapquery.datasource.actions.DataSourceCreateAction",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "close",
  "father" : "",
  "desc" : "close",
  "name" : "DataSourceCreateAction.prototype.close=function()",
  "version" : "0",
  "path" : "bof.olapquery.datasource.actions.DataSourceCreateAction",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "discard",
  "father" : "",
  "desc" : "discard",
  "name" : "DataSourceCreateAction.prototype.discard=function()",
  "version" : "0",
  "path" : "bof.olapquery.datasource.actions.DataSourceCreateAction",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]