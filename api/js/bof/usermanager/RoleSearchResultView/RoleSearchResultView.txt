[ {
  "type" : 0,
  "keyword" : "elemTbody_dblclick_handler",
  "father" : "",
  "desc" : "elemTbody_dblclick_handler",
  "name" : "RoleSearchResultView.prototype.elemTbody_dblclick_handler=function()",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "refresh",
  "father" : "",
  "desc" : "refresh",
  "name" : "RoleSearchResultView.prototype.refresh=function()",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemList",
  "father" : "",
  "desc" : "getItemList",
  "name" : "RoleSearchResultView.prototype.getItemList=function()",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fillList",
  "father" : "",
  "desc" : "fillList",
  "name" : "RoleSearchResultView.prototype.fillList=function()",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getItemListLength",
  "father" : "",
  "desc" : "getItemListLength",
  "name" : "RoleSearchResultView.prototype.getItemListLength=function()",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getCurrentItemId",
  "father" : "",
  "desc" : "getCurrentItemId",
  "name" : "RoleSearchResultView.prototype.getCurrentItemId=function()",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setCurrentItemId",
  "father" : "",
  "desc" : "setCurrentItemId",
  "name" : "RoleSearchResultView.prototype.setCurrentItemId=function(itemId)",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemTbody_contextmenu_handler",
  "father" : "",
  "desc" : "elemTbody_contextmenu_handler",
  "name" : "RoleSearchResultView.prototype.elemTbody_contextmenu_handler=function(ev)",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getMenuHandlers",
  "father" : "",
  "desc" : "getMenuHandlers",
  "name" : "RoleSearchResultView.prototype.getMenuHandlers=function()",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initMenuHandlers",
  "father" : "",
  "desc" : "initMenuHandlers",
  "name" : "RoleSearchResultView.prototype.initMenuHandlers=function(type)",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doOnItemMenu",
  "father" : "",
  "desc" : "doOnItemMenu",
  "name" : "RoleSearchResultView.prototype.doOnItemMenu=function(e,type)",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doCmd",
  "father" : "",
  "desc" : "doCmd",
  "name" : "RoleSearchResultView.prototype.doCmd=function(cmd)",
  "version" : "0",
  "path" : "bof.usermanager.RoleSearchResultView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]