[ {
  "type" : 0,
  "keyword" : "createView",
  "father" : "",
  "desc" : "createView",
  "name" : "AddUsersToRoleAction.prototype.createView=function()",
  "version" : "0",
  "path" : "bof.usermanager.actions.AddUsersToRoleAction",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "close",
  "father" : "",
  "desc" : "close",
  "name" : "AddUsersToRoleAction.prototype.close=function()",
  "version" : "0",
  "path" : "bof.usermanager.actions.AddUsersToRoleAction",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "AddUsersToRoleAction.prototype.getValue=function()",
  "version" : "0",
  "path" : "bof.usermanager.actions.AddUsersToRoleAction",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "save",
  "father" : "",
  "desc" : "save",
  "name" : "AddUsersToRoleAction.prototype.save=function(roleId)",
  "version" : "0",
  "path" : "bof.usermanager.actions.AddUsersToRoleAction",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "saveBean",
  "father" : "",
  "desc" : "saveBean",
  "name" : "AddUsersToRoleAction.prototype.saveBean=function(roleId,userList)",
  "version" : "0",
  "path" : "bof.usermanager.actions.AddUsersToRoleAction",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isOpenEnabled",
  "father" : "",
  "desc" : "isOpenEnabled",
  "name" : "AddUsersToRoleAction.prototype.isOpenEnabled=function(currentNode)",
  "version" : "0",
  "path" : "bof.usermanager.actions.AddUsersToRoleAction",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "confirm",
  "father" : "",
  "desc" : "confirm",
  "name" : "AddUsersToRoleAction.prototype.confirm=function()",
  "version" : "0",
  "path" : "bof.usermanager.actions.AddUsersToRoleAction",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]