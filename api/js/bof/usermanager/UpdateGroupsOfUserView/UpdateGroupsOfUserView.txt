[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "UpdateGroupsOfUserView.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isDirty",
  "father" : "",
  "desc" : "isDirty",
  "name" : "UpdateGroupsOfUserView.prototype.isDirty=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getValue",
  "father" : "",
  "desc" : "getValue",
  "name" : "UpdateGroupsOfUserView.prototype.getValue=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getDefaultValue",
  "father" : "",
  "desc" : "getDefaultValue",
  "name" : "UpdateGroupsOfUserView.prototype.getDefaultValue=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initUserName",
  "father" : "",
  "desc" : "initUserName",
  "name" : "UpdateGroupsOfUserView.prototype.initUserName=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fill",
  "father" : "",
  "desc" : "fill",
  "name" : "UpdateGroupsOfUserView.prototype.fill=function(bean)",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getSelectedList",
  "father" : "",
  "desc" : "getSelectedList",
  "name" : "UpdateGroupsOfUserView.prototype.getSelectedList=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "addGroup",
  "father" : "",
  "desc" : "addGroup",
  "name" : "UpdateGroupsOfUserView.prototype.addGroup=function(groupInfo)",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAdd",
  "father" : "",
  "desc" : "doAdd",
  "name" : "UpdateGroupsOfUserView.prototype.doAdd=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAllAdd",
  "father" : "",
  "desc" : "doAllAdd",
  "name" : "UpdateGroupsOfUserView.prototype.doAllAdd=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doDelete",
  "father" : "",
  "desc" : "doDelete",
  "name" : "UpdateGroupsOfUserView.prototype.doDelete=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doAllDelete",
  "father" : "",
  "desc" : "doAllDelete",
  "name" : "UpdateGroupsOfUserView.prototype.doAllDelete=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemDoAdd_click_handler",
  "father" : "",
  "desc" : "elemDoAdd_click_handler",
  "name" : "UpdateGroupsOfUserView.prototype.elemDoAdd_click_handler=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemDoDelete_click_handler",
  "father" : "",
  "desc" : "elemDoDelete_click_handler",
  "name" : "UpdateGroupsOfUserView.prototype.elemDoDelete_click_handler=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemDoAllDelete_click_handler",
  "father" : "",
  "desc" : "elemDoAllDelete_click_handler",
  "name" : "UpdateGroupsOfUserView.prototype.elemDoAllDelete_click_handler=function()",
  "version" : "0",
  "path" : "bof.usermanager.UpdateGroupsOfUserView",
  "updateTime" : "2021-09-22 01:18:31",
  "isDelete" : 0
} ]