[ {
  "type" : 0,
  "keyword" : "destroy",
  "father" : "",
  "desc" : "destroy",
  "name" : "MetadataSearchView.prototype.destroy=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hasManageOp",
  "father" : "",
  "desc" : "hasManageOp",
  "name" : "MetadataSearchView.prototype.hasManageOp=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "hide",
  "father" : "",
  "desc" : "hide",
  "name" : "MetadataSearchView.prototype.hide=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "show",
  "father" : "",
  "desc" : "show",
  "name" : "MetadataSearchView.prototype.show=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getPageSize",
  "father" : "",
  "desc" : "getPageSize",
  "name" : "MetadataSearchView.prototype.getPageSize=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemKeywords_keypress_handler",
  "father" : "",
  "desc" : "elemKeywords_keypress_handler",
  "name" : "MetadataSearchView.prototype.elemKeywords_keypress_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemClearBtnCell_click_handler",
  "father" : "",
  "desc" : "elemClearBtnCell_click_handler",
  "name" : "MetadataSearchView.prototype.elemClearBtnCell_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemKeywords_input_handler",
  "father" : "",
  "desc" : "elemKeywords_input_handler",
  "name" : "MetadataSearchView.prototype.elemKeywords_input_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clearSearchResult",
  "father" : "",
  "desc" : "clearSearchResult",
  "name" : "MetadataSearchView.prototype.clearSearchResult=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "checkIndexDir",
  "father" : "",
  "desc" : "checkIndexDir",
  "name" : "MetadataSearchView.prototype.checkIndexDir=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBtnHelp_click_handler",
  "father" : "",
  "desc" : "elemBtnHelp_click_handler",
  "name" : "MetadataSearchView.prototype.elemBtnHelp_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getReportFilterTypes",
  "father" : "",
  "desc" : "getReportFilterTypes",
  "name" : "MetadataSearchView.prototype.getReportFilterTypes=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getBizviewFilterTypes",
  "father" : "",
  "desc" : "getBizviewFilterTypes",
  "name" : "MetadataSearchView.prototype.getBizviewFilterTypes=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getOtherFilterTypes",
  "father" : "",
  "desc" : "getOtherFilterTypes",
  "name" : "MetadataSearchView.prototype.getOtherFilterTypes=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "initStandbyFilters",
  "father" : "",
  "desc" : "initStandbyFilters",
  "name" : "MetadataSearchView.prototype.initStandbyFilters=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "changeSelectMethod",
  "father" : "",
  "desc" : "changeSelectMethod",
  "name" : "MetadataSearchView.prototype.changeSelectMethod=function(obj)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getExtTypes",
  "father" : "",
  "desc" : "getExtTypes",
  "name" : "MetadataSearchView.prototype.getExtTypes=function(types)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBtnSearch_click_handler",
  "father" : "",
  "desc" : "elemBtnSearch_click_handler",
  "name" : "MetadataSearchView.prototype.elemBtnSearch_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemFirstPage_click_handler",
  "father" : "",
  "desc" : "elemFirstPage_click_handler",
  "name" : "MetadataSearchView.prototype.elemFirstPage_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemPrevPage_click_handler",
  "father" : "",
  "desc" : "elemPrevPage_click_handler",
  "name" : "MetadataSearchView.prototype.elemPrevPage_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemNextPage_click_handler",
  "father" : "",
  "desc" : "elemNextPage_click_handler",
  "name" : "MetadataSearchView.prototype.elemNextPage_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemLastPage_click_handler",
  "father" : "",
  "desc" : "elemLastPage_click_handler",
  "name" : "MetadataSearchView.prototype.elemLastPage_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemBtnClose_click_handler",
  "father" : "",
  "desc" : "elemBtnClose_click_handler",
  "name" : "MetadataSearchView.prototype.elemBtnClose_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "setButtonVisible",
  "father" : "",
  "desc" : "setButtonVisible",
  "name" : "MetadataSearchView.prototype.setButtonVisible=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "getResult",
  "father" : "",
  "desc" : "getResult",
  "name" : "MetadataSearchView.prototype.getResult=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "clearList",
  "father" : "",
  "desc" : "clearList",
  "name" : "MetadataSearchView.prototype.clearList=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "isFolderType",
  "father" : "",
  "desc" : "isFolderType",
  "name" : "MetadataSearchView.prototype.isFolderType=function(type)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "fillList",
  "father" : "",
  "desc" : "fillList",
  "name" : "MetadataSearchView.prototype.fillList=function(result)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "customElementType",
  "father" : "",
  "desc" : "customElementType",
  "name" : "MetadataSearchView.prototype.customElementType=function(node)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemResoureFrom_click_handler",
  "father" : "",
  "desc" : "elemResoureFrom_click_handler",
  "name" : "MetadataSearchView.prototype.elemResoureFrom_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "openDialog",
  "father" : "",
  "desc" : "openDialog",
  "name" : "MetadataSearchView.prototype.openDialog=function(func,analysisObject)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemImpactAnalysis_click_handler",
  "father" : "",
  "desc" : "elemImpactAnalysis_click_handler",
  "name" : "MetadataSearchView.prototype.elemImpactAnalysis_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "elemPedigreeAnalysis_click_handler",
  "father" : "",
  "desc" : "elemPedigreeAnalysis_click_handler",
  "name" : "MetadataSearchView.prototype.elemPedigreeAnalysis_click_handler=function(e)",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "doClose",
  "father" : "",
  "desc" : "doClose",
  "name" : "MetadataSearchView.prototype.doClose=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
}, {
  "type" : 0,
  "keyword" : "updateScrollbar",
  "father" : "",
  "desc" : "updateScrollbar",
  "name" : "MetadataSearchView.prototype.updateScrollbar=function()",
  "version" : "0",
  "path" : "bof.metadata.MetadataSearchView",
  "updateTime" : "2021-09-22 01:18:30",
  "isDelete" : 0
} ]