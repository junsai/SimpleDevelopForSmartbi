[ {
  "type" : 1,
  "keyword" : "getCatalogTreeModule",
  "father" : null,
  "desc" : "getCatalogTreeModule",
  "name" : "public ICatalogTreeModule getCatalogTreeModule ()",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getCatalogElementPath",
  "father" : null,
  "desc" : "getCatalogElementPath",
  "name" : "public List<MdpPageElement> getCatalogElementPath (String)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "hasChild",
  "father" : null,
  "desc" : "hasChild",
  "name" : "public boolean hasChild (String)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getSubPagesByClientType",
  "father" : null,
  "desc" : "getSubPagesByClientType",
  "name" : "public List<MdpPageElement> getSubPagesByClientType (String,String)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getUserPreference",
  "father" : null,
  "desc" : "getUserPreference",
  "name" : "public IUserStyleConfig getUserPreference (String)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getShowSubMenuStyleOption",
  "father" : null,
  "desc" : "getShowSubMenuStyleOption",
  "name" : "public boolean getShowSubMenuStyleOption ()",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getMyFavoriteElements",
  "father" : null,
  "desc" : "getMyFavoriteElements",
  "name" : "public List<MyFavoriteElement> getMyFavoriteElements ()",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getMyFavoriteRootId",
  "father" : null,
  "desc" : "getMyFavoriteRootId",
  "name" : "public String getMyFavoriteRootId ()",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getChildStyleByPageId",
  "father" : null,
  "desc" : "getChildStyleByPageId",
  "name" : "public String getChildStyleByPageId (String)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getPageExByPageId",
  "father" : null,
  "desc" : "getPageExByPageId",
  "name" : "public Page getPageExByPageId (String)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getChildStyleByPageEx",
  "father" : null,
  "desc" : "getChildStyleByPageEx",
  "name" : "public String getChildStyleByPageEx (Page)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getUserConfig",
  "father" : null,
  "desc" : "getUserConfig",
  "name" : "public List<IUserStyleConfig> getUserConfig (List<String>)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getMyPages",
  "father" : null,
  "desc" : "getMyPages",
  "name" : "public List<MdpPageElement> getMyPages ()",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getMyFirstPageId",
  "father" : null,
  "desc" : "getMyFirstPageId",
  "name" : "public String getMyFirstPageId ()",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "hasChildPages",
  "father" : null,
  "desc" : "hasChildPages",
  "name" : "public boolean hasChildPages (String)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getSubPages",
  "father" : null,
  "desc" : "getSubPages",
  "name" : "public List<MdpPageElement> getSubPages (String)",
  "version" : "0",
  "path" : "smartbi\\mdp\\MDPService",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
} ]