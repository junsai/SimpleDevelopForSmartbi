[ {
  "type" : 1,
  "keyword" : "getTempId",
  "father" : null,
  "desc" : "getTempId",
  "name" : "public String getTempId ()",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "removeRef",
  "father" : null,
  "desc" : "removeRef",
  "name" : "public void removeRef (String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "saveCommonLayout",
  "father" : null,
  "desc" : "saveCommonLayout",
  "name" : "public void saveCommonLayout (String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "createPortalPage",
  "father" : null,
  "desc" : "createPortalPage",
  "name" : "public ICatalogElement createPortalPage (String,String,String,String,CatalogElementType,String,int)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "deleteNodes",
  "father" : null,
  "desc" : "deleteNodes",
  "name" : "public void deleteNodes (List<String>)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getLayout",
  "father" : null,
  "desc" : "getLayout",
  "name" : "public String getLayout (String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "saveLayout",
  "father" : null,
  "desc" : "saveLayout",
  "name" : "public void saveLayout (String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "updateLayout",
  "father" : null,
  "desc" : "updateLayout",
  "name" : "public void updateLayout (String,String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "deleteUserLayout",
  "father" : null,
  "desc" : "deleteUserLayout",
  "name" : "public void deleteUserLayout ()",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getCommonLayout",
  "father" : null,
  "desc" : "getCommonLayout",
  "name" : "public String getCommonLayout ()",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setPageToRef",
  "father" : null,
  "desc" : "setPageToRef",
  "name" : "public void setPageToRef (String,String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getPortalViewRootElements",
  "father" : null,
  "desc" : "getPortalViewRootElements",
  "name" : "public List<ICatalogElement> getPortalViewRootElements ()",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "addReportToLayout",
  "father" : null,
  "desc" : "addReportToLayout",
  "name" : "public void addReportToLayout (List<String>String,String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getPortalViewChildElements",
  "father" : null,
  "desc" : "getPortalViewChildElements",
  "name" : "public List<ICatalogElement> getPortalViewChildElements (String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setSelfPageRootPageToRef",
  "father" : null,
  "desc" : "setSelfPageRootPageToRef",
  "name" : "public void setSelfPageRootPageToRef (String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getEditRootElements",
  "father" : null,
  "desc" : "getEditRootElements",
  "name" : "public List<ICatalogElement> getEditRootElements ()",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getViewPortalElement",
  "father" : null,
  "desc" : "getViewPortalElement",
  "name" : "public ICatalogElement getViewPortalElement (String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getViewPortalChildElements",
  "father" : null,
  "desc" : "getViewPortalChildElements",
  "name" : "public List<ICatalogElement> getViewPortalChildElements (String)",
  "version" : "0",
  "path" : "smartbi\\freequery\\client\\portal\\PortalService",
  "updateTime" : "2021-09-19 10:19:49",
  "isDelete" : 0
} ]