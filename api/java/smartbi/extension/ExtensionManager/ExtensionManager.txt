[ {
  "type" : 1,
  "keyword" : "shutdown",
  "father" : null,
  "desc" : "shutdown",
  "name" : "public void shutdown ()",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getExtensions",
  "father" : null,
  "desc" : "getExtensions",
  "name" : "public List<Extension> getExtensions ()",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getConfig",
  "father" : null,
  "desc" : "getConfig",
  "name" : "public ExtensionConfiguration getConfig ()",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getCatalogTreeModule",
  "father" : null,
  "desc" : "getCatalogTreeModule",
  "name" : "public ICatalogTreeModule getCatalogTreeModule ()",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setCatalogTreeModule",
  "father" : null,
  "desc" : "setCatalogTreeModule",
  "name" : "public void setCatalogTreeModule (ICatalogTreeModule)",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getVersionTxt",
  "father" : null,
  "desc" : "getVersionTxt",
  "name" : "String getVersionTxt (String)",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "closeExtensions",
  "father" : null,
  "desc" : "closeExtensions",
  "name" : "public void closeExtensions ()",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "initClassLoader",
  "father" : null,
  "desc" : "initClassLoader",
  "name" : "public void initClassLoader ()",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getVersionList",
  "father" : null,
  "desc" : "getVersionList",
  "name" : "JSONArray getVersionList (String)",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getVersionObject",
  "father" : null,
  "desc" : "getVersionObject",
  "name" : "JSONObject getVersionObject (String)",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getConfigurationAsJSONObject",
  "father" : null,
  "desc" : "getConfigurationAsJSONObject",
  "name" : "JSONObject getConfigurationAsJSONObject ()",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getExtensionByName",
  "father" : null,
  "desc" : "getExtensionByName",
  "name" : "public Extension getExtensionByName (String)",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getVersionTxtFromExtName",
  "father" : null,
  "desc" : "getVersionTxtFromExtName",
  "name" : "String getVersionTxtFromExtName (String)",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getConfiguration",
  "father" : null,
  "desc" : "getConfiguration",
  "name" : "InputStream getConfiguration ()",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "reactivate",
  "father" : null,
  "desc" : "reactivate",
  "name" : "public void reactivate ()",
  "version" : "0",
  "path" : "smartbi\\extension\\ExtensionManager",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
} ]