[ {
  "type" : 1,
  "keyword" : "isClusterExceed",
  "father" : null,
  "desc" : "isClusterExceed",
  "name" : "public boolean isClusterExceed ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isNeedPreUpgrade",
  "father" : null,
  "desc" : "isNeedPreUpgrade",
  "name" : "public boolean isNeedPreUpgrade ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getShutdownHook",
  "father" : null,
  "desc" : "getShutdownHook",
  "name" : "public IShutdownHook getShutdownHook (String)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getVersionInfo",
  "father" : null,
  "desc" : "getVersionInfo",
  "name" : "public StringBuilder getVersionInfo ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setShutdownHooks",
  "father" : null,
  "desc" : "setShutdownHooks",
  "name" : "public void setShutdownHooks (Map<String,IShutdownHook>)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getShutdownHooks",
  "father" : null,
  "desc" : "getShutdownHooks",
  "name" : "public Map<String,IShutdownHook> getShutdownHooks ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isConfigFileOK",
  "father" : null,
  "desc" : "isConfigFileOK",
  "name" : "public boolean isConfigFileOK ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setActived",
  "father" : null,
  "desc" : "setActived",
  "name" : "public void setActived (boolean)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isNeedUpgrade",
  "father" : null,
  "desc" : "isNeedUpgrade",
  "name" : "public boolean isNeedUpgrade ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setClusterExceed",
  "father" : null,
  "desc" : "setClusterExceed",
  "name" : "public void setClusterExceed (boolean)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isActived",
  "father" : null,
  "desc" : "isActived",
  "name" : "public boolean isActived ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isFirstUpgrade",
  "father" : null,
  "desc" : "isFirstUpgrade",
  "name" : "public boolean isFirstUpgrade ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setFirstUpgrade",
  "father" : null,
  "desc" : "setFirstUpgrade",
  "name" : "public void setFirstUpgrade (boolean)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setNeedUpgrade",
  "father" : null,
  "desc" : "setNeedUpgrade",
  "name" : "public void setNeedUpgrade (boolean)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setDoPostUpgrade",
  "father" : null,
  "desc" : "setDoPostUpgrade",
  "name" : "public void setDoPostUpgrade (boolean)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isDoPostUpgrade",
  "father" : null,
  "desc" : "isDoPostUpgrade",
  "name" : "public boolean isDoPostUpgrade ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getServletContext",
  "father" : null,
  "desc" : "getServletContext",
  "name" : "public Object getServletContext ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isServerStartupSucceed",
  "father" : null,
  "desc" : "isServerStartupSucceed",
  "name" : "public boolean isServerStartupSucceed ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getFrameworkConfiguraion",
  "father" : null,
  "desc" : "getFrameworkConfiguraion",
  "name" : "public IFrameworkConfiguration getFrameworkConfiguraion ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isSpreadsheetEdition",
  "father" : null,
  "desc" : "isSpreadsheetEdition",
  "name" : "public boolean isSpreadsheetEdition ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getProductEdition",
  "father" : null,
  "desc" : "getProductEdition",
  "name" : "public ProductEdition getProductEdition ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getExceptionHandler",
  "father" : null,
  "desc" : "getExceptionHandler",
  "name" : "public IServerExceptionHandler getExceptionHandler ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setExceptionHandler",
  "father" : null,
  "desc" : "setExceptionHandler",
  "name" : "public void setExceptionHandler (IServerExceptionHandler)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setNeedPreUpgrade",
  "father" : null,
  "desc" : "setNeedPreUpgrade",
  "name" : "public void setNeedPreUpgrade (boolean)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setModules",
  "father" : null,
  "desc" : "setModules",
  "name" : "public void setModules (Map<String,IModule>)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isInsightEdition",
  "father" : null,
  "desc" : "isInsightEdition",
  "name" : "public boolean isInsightEdition ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isMiningEdition",
  "father" : null,
  "desc" : "isMiningEdition",
  "name" : "public boolean isMiningEdition ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getModule",
  "father" : null,
  "desc" : "getModule",
  "name" : "public IModule getModule (String)",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "reactivate",
  "father" : null,
  "desc" : "reactivate",
  "name" : "public void reactivate ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getModules",
  "father" : null,
  "desc" : "getModules",
  "name" : "public Map<String,IModule> getModules ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isEagleEdition",
  "father" : null,
  "desc" : "isEagleEdition",
  "name" : "public boolean isEagleEdition ()",
  "version" : "0",
  "path" : "smartbi\\framework\\Framework",
  "updateTime" : "2021-09-19 10:19:51",
  "isDelete" : 0
} ]