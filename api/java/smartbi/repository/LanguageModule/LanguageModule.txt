[ {
  "type" : 1,
  "keyword" : "getValue",
  "father" : null,
  "desc" : "getValue",
  "name" : "public String getValue (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getValue",
  "father" : null,
  "desc" : "getValue",
  "name" : "public String getValue (String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getState",
  "father" : null,
  "desc" : "getState",
  "name" : "public IStateModule getState ()",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setState",
  "father" : null,
  "desc" : "setState",
  "name" : "public void setState (IStateModule)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "addSupportType",
  "father" : null,
  "desc" : "addSupportType",
  "name" : "public void addSupportType (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getLanguageById",
  "father" : null,
  "desc" : "getLanguageById",
  "name" : "public ILanguage getLanguageById (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getAllKeys",
  "father" : null,
  "desc" : "getAllKeys",
  "name" : "public List<String> getAllKeys ()",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getAllLanguages",
  "father" : null,
  "desc" : "getAllLanguages",
  "name" : "public List<ILanguage> getAllLanguages ()",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "deleteLanguage",
  "father" : null,
  "desc" : "deleteLanguage",
  "name" : "public void deleteLanguage (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "deleteLanguage",
  "father" : null,
  "desc" : "deleteLanguage",
  "name" : "public void deleteLanguage (String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setCatalogTree",
  "father" : null,
  "desc" : "setCatalogTree",
  "name" : "public void setCatalogTree (ICatalogTreeModule)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getCatalogTree",
  "father" : null,
  "desc" : "getCatalogTree",
  "name" : "public ICatalogTreeModule getCatalogTree ()",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "createLanguage",
  "father" : null,
  "desc" : "createLanguage",
  "name" : "public boolean createLanguage (String,String,String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "clearAllCache",
  "father" : null,
  "desc" : "clearAllCache",
  "name" : "public void clearAllCache ()",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getExtend",
  "father" : null,
  "desc" : "getExtend",
  "name" : "public String getExtend (String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getExtend",
  "father" : null,
  "desc" : "getExtend",
  "name" : "public String getExtend (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "setMetadata",
  "father" : null,
  "desc" : "setMetadata",
  "name" : "public void setMetadata (IMetadataModule)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getMetadata",
  "father" : null,
  "desc" : "getMetadata",
  "name" : "public IMetadataModule getMetadata ()",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getLanguagesByKey",
  "father" : null,
  "desc" : "getLanguagesByKey",
  "name" : "public List<ILanguage> getLanguagesByKey (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "updateLanguageByKeyBatch",
  "father" : null,
  "desc" : "updateLanguageByKeyBatch",
  "name" : "public boolean updateLanguageByKeyBatch (List<LanguageRowData>boolean)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getLanguagesAsEditList",
  "father" : null,
  "desc" : "getLanguagesAsEditList",
  "name" : "public List<Language>> getLanguagesAsEditList (int,int)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getAllSupportTypes",
  "father" : null,
  "desc" : "getAllSupportTypes",
  "name" : "public List<String> getAllSupportTypes ()",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getLanguageByKeyAndLocale",
  "father" : null,
  "desc" : "getLanguageByKeyAndLocale",
  "name" : "public ILanguage getLanguageByKeyAndLocale (String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "updateLanguageByKeyAndLocale",
  "father" : null,
  "desc" : "updateLanguageByKeyAndLocale",
  "name" : "public boolean updateLanguageByKeyAndLocale (String,String,String,String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "updateLanguagesByKey",
  "father" : null,
  "desc" : "updateLanguagesByKey",
  "name" : "public boolean updateLanguagesByKey (String,String,String,String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getResTreeLanguages",
  "father" : null,
  "desc" : "getResTreeLanguages",
  "name" : "public List<CatalogLanguage> getResTreeLanguages (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "updateOrCreateLanguage",
  "father" : null,
  "desc" : "updateOrCreateLanguage",
  "name" : "public boolean updateOrCreateLanguage (String,String,String,String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "updateOrCreateLanguageWithId",
  "father" : null,
  "desc" : "updateOrCreateLanguageWithId",
  "name" : "public boolean updateOrCreateLanguageWithId (String,String,String,String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "createLanguageWithId",
  "father" : null,
  "desc" : "createLanguageWithId",
  "name" : "public boolean createLanguageWithId (String,String,String,String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getKeyLocaleToLanguageMap",
  "father" : null,
  "desc" : "getKeyLocaleToLanguageMap",
  "name" : "public Map<String,Language> getKeyLocaleToLanguageMap ()",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "deleteLanguagesByKey",
  "father" : null,
  "desc" : "deleteLanguagesByKey",
  "name" : "public void deleteLanguagesByKey (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "updateLanguage",
  "father" : null,
  "desc" : "updateLanguage",
  "name" : "public boolean updateLanguage (String,String,String,String,String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "isSupportType",
  "father" : null,
  "desc" : "isSupportType",
  "name" : "public boolean isSupportType (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "updateOrCreateLanguageBatch",
  "father" : null,
  "desc" : "updateOrCreateLanguageBatch",
  "name" : "public boolean updateOrCreateLanguageBatch (List<String>>)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "searchLanguagesWithKeyWord",
  "father" : null,
  "desc" : "searchLanguagesWithKeyWord",
  "name" : "public Collection<CatalogLanguage> searchLanguagesWithKeyWord (String,int)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
}, {
  "type" : 1,
  "keyword" : "getCurrentLocaleValue",
  "father" : null,
  "desc" : "getCurrentLocaleValue",
  "name" : "public String getCurrentLocaleValue (String)",
  "version" : "0",
  "path" : "smartbi\\repository\\LanguageModule",
  "updateTime" : "2021-09-19 10:19:50",
  "isDelete" : 0
} ]